<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://ays-pro.com/
 * @since      1.0.0
 *
 * @package    Survey_Maker
 * @subpackage Survey_Maker/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Survey_Maker
 * @subpackage Survey_Maker/admin
 * @author     Survey Maker team <info@ays-pro.com>
 */
class Survey_Maker_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * The surveys list table object.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      object    $surveys_obj    The surveys list table object.
	 */
    private $surveys_obj;

	/**
	 * The surveys categories list table object.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      object    $surveys_categories_obj    The surveys categories list table object.
	 */
    private $surveys_categories_obj;

	/**
	 * The survey questions list table object.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      object    $questions_obj    The survey questions list table object.
	 */
    private $questions_obj;

	/**
	 * The survey questions categories list table object.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      object    $question_categories_obj    The survey questions categories list table object.
	 */
    private $question_categories_obj;

	/**
	 * The survey submissions list table object.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      object    $results_obj    The survey submissions list table object.
	 */
    private $submissions_obj;

	/**
	 * The survey questions categories list table object for each survey.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      object    $each_result_obj    The survey submissions list table object for each survey.
	 */
    private $each_submission_obj;

	/**
	 * The settings object of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      object    $settings_obj    The settings object of this plugin.
	 */
    private $settings_obj;

	/**
	 * The capability of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $capability    The capability for users access to this plugin.
	 */
    private $capability;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

        add_filter('set-screen-option', array(__CLASS__, 'set_screen'), 10, 3);
        // $per_page_array = array(
        //     'quizes_per_page',
        //     'questions_per_page',
        //     'quiz_categories_per_page',
        //     'question_categories_per_page',
        //     'attributes_per_page',
        //     'quiz_results_per_page',
        //     'quiz_each_results_per_page',
        //     'quiz_orders_per_page',
        // );
        // foreach($per_page_array as $option_name){
        //     add_filter('set_screen_option_'.$option_name, array(__CLASS__, 'set_screen'), 10, 3);
        // }

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles( $hook_suffix ) {

        wp_enqueue_style( $this->plugin_name . '-admin', plugin_dir_url(__FILE__) . 'css/admin.css', array(), $this->version, 'all');
        
        if (false !== strpos($hook_suffix, "plugins.php")){
            wp_enqueue_style( $this->plugin_name . '-sweetalert-css', SURVEY_MAKER_PUBLIC_URL . '/css/survey-maker-sweetalert2.min.css', array(), $this->version, 'all');
        }

        if (false === strpos($hook_suffix, $this->plugin_name))
            return;
            
        // You need styling for the datepicker. For simplicity I've linked to the jQuery UI CSS on a CDN.
        wp_register_style( 'jquery-ui', 'https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css' );
        wp_enqueue_style( 'jquery-ui' );

        wp_enqueue_style('wp-color-picker');
        wp_enqueue_style( $this->plugin_name . '-banner.css', plugin_dir_url(__FILE__) . 'css/banner.css', array(), $this->version, 'all');
        wp_enqueue_style( $this->plugin_name . '-animate.css', plugin_dir_url(__FILE__) . 'css/animate.css', array(), $this->version, 'all');
        wp_enqueue_style( $this->plugin_name . '-animations.css', plugin_dir_url(__FILE__) . 'css/animations.css', array(), $this->version, 'all');
        wp_enqueue_style( $this->plugin_name . '-font-awesome', '//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css', array(), $this->version, 'all');
        wp_enqueue_style( $this->plugin_name . '-font-awesome-icons', plugin_dir_url(__FILE__) . 'css/ays-font-awesome.css', array(), $this->version, 'all');
        wp_enqueue_style( $this->plugin_name . '-select2', SURVEY_MAKER_PUBLIC_URL .  '/css/survey-maker-select2.min.css', array(), $this->version, 'all');
        wp_enqueue_style( $this->plugin_name . '-transition', SURVEY_MAKER_PUBLIC_URL .  '/css/transition.min.css', array(), $this->version, 'all');
        wp_enqueue_style( $this->plugin_name . '-dropdown', SURVEY_MAKER_PUBLIC_URL .  '/css/dropdown.min.css', array(), $this->version, 'all');
        wp_enqueue_style( $this->plugin_name . '-popup', plugin_dir_url(__FILE__) . 'css/popup.min.css', array(), $this->version, 'all');
        wp_enqueue_style( $this->plugin_name . '-bootstrap', plugin_dir_url(__FILE__) . 'css/bootstrap.min.css', array(), $this->version, 'all');
        wp_enqueue_style( $this->plugin_name . '-data-bootstrap', plugin_dir_url(__FILE__) . 'css/dataTables.bootstrap4.min.css', array(), $this->version, 'all');
        wp_enqueue_style( $this->plugin_name . '-datetimepicker', plugin_dir_url(__FILE__) . 'css/jquery-ui-timepicker-addon.css', array(), $this->version, 'all');

        wp_enqueue_style( $this->plugin_name . "-general", plugin_dir_url( __FILE__ ) . 'css/survey-maker-general.css', array(), time(), 'all' );
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/survey-maker-admin.css', array(), time(), 'all' );
        wp_enqueue_style( $this->plugin_name . "-loaders", plugin_dir_url(__FILE__) . 'css/loaders.css', array(), $this->version, 'all');

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts( $hook_suffix ) {
        global $wp_version;

        $version1 = $wp_version;
        $operator = '>=';
        $version2 = '5.5';
        $versionCompare = $this->aysSurveyMakerVersionCompare($version1, $operator, $version2);

        if ($versionCompare) {
            wp_enqueue_script( $this->plugin_name.'-wp-load-scripts', plugin_dir_url(__FILE__) . 'js/survey-maker-wp-load-scripts.js', array(), $this->version, true);
        }

        if (false !== strpos($hook_suffix, "plugins.php")){
            wp_enqueue_script( $this->plugin_name . '-sweetalert-js', SURVEY_MAKER_PUBLIC_URL . '/js/survey-maker-sweetalert2.all.min.js', array('jquery'), $this->version, true );
            wp_enqueue_script( $this->plugin_name . '-admin', plugin_dir_url(__FILE__) . 'js/admin.js', array( 'jquery' ), $this->version, true );
            wp_localize_script( $this->plugin_name . '-admin', 'SurveyMakerAdmin', array( 
            	'ajaxUrl' => admin_url( 'admin-ajax.php' )
            ) );
        }
        
        if (false === strpos($hook_suffix, $this->plugin_name))
            return;

        wp_enqueue_script( 'jquery' );
        wp_enqueue_script( 'jquery-effects-core' );
        wp_enqueue_script( 'jquery-ui-sortable' );
        wp_enqueue_script( 'jquery-ui-datepicker' );
        wp_enqueue_media();
        wp_enqueue_script( $this->plugin_name . '-color-picker-alpha', plugin_dir_url(__FILE__) . 'js/wp-color-picker-alpha.min.js', array( 'wp-color-picker' ), $this->version, true );
        $color_picker_strings = array(
            'clear'            => __( 'Clear', $this->plugin_name ),
            'clearAriaLabel'   => __( 'Clear color', $this->plugin_name ),
            'defaultString'    => __( 'Default', $this->plugin_name ),
            'defaultAriaLabel' => __( 'Select default color', $this->plugin_name ),
            'pick'             => __( 'Select Color', $this->plugin_name ),
            'defaultLabel'     => __( 'Color value', $this->plugin_name ),
        );
        wp_localize_script( $this->plugin_name . '-color-picker-alpha', 'wpColorPickerL10n', $color_picker_strings );


		/* 
        ========================================== 
           * Bootstrap
           * select2
           * jQuery DataTables
        ========================================== 
        */
        wp_enqueue_script( $this->plugin_name . "-popper", plugin_dir_url(__FILE__) . 'js/popper.min.js', array( 'jquery' ), $this->version, true );
        wp_enqueue_script( $this->plugin_name . "-bootstrap", plugin_dir_url(__FILE__) . 'js/bootstrap.min.js', array( 'jquery' ), $this->version, true );
        wp_enqueue_script( $this->plugin_name . '-select2js', SURVEY_MAKER_PUBLIC_URL . '/js/survey-maker-select2.min.js', array('jquery'), $this->version, true);
        wp_enqueue_script( $this->plugin_name . '-sweetalert-js', SURVEY_MAKER_PUBLIC_URL . '/js/survey-maker-sweetalert2.all.min.js', array('jquery'), $this->version, true );
        wp_enqueue_script( $this->plugin_name . '-datatable-min', SURVEY_MAKER_PUBLIC_URL . '/js/survey-maker-datatable.min.js', array('jquery'), $this->version, true);
        wp_enqueue_script( $this->plugin_name . '-transition-min', SURVEY_MAKER_PUBLIC_URL . '/js/transition.min.js', array('jquery'), $this->version, true);
        wp_enqueue_script( $this->plugin_name . '-dropdown-min', SURVEY_MAKER_PUBLIC_URL . '/js/dropdown.min.js', array('jquery'), $this->version, true);
        wp_enqueue_script( $this->plugin_name . "-db4.min.js", plugin_dir_url( __FILE__ ) . 'js/dataTables.bootstrap4.min.js', array( 'jquery' ), $this->version, true );
        wp_enqueue_script( $this->plugin_name . "-datetimepicker", plugin_dir_url( __FILE__ ) . 'js/jquery-ui-timepicker-addon.js', array( 'jquery' ), $this->version, true );
        wp_enqueue_script( $this->plugin_name . '-autosize', SURVEY_MAKER_PUBLIC_URL . '/js/survey-maker-autosize.js', array( 'jquery' ), $this->version, false );

        /* 
        ================================================
           Survey admin dashboard scripts (Google charts)
        ================================================
        */
        if ( strpos($hook_suffix, 'each-submission') !== false ) {
            wp_enqueue_script( $this->plugin_name . '-charts-google', plugin_dir_url(__FILE__) . 'js/google-chart.js', array('jquery'), $this->version, true);
            wp_enqueue_script( $this->plugin_name . '-charts', plugin_dir_url(__FILE__) . 'js/partials/survey-maker-admin-submissions-charts.js', array('jquery'), $this->version, true);
        }

        /* 
        ================================================
           Quiz admin dashboard scripts (and for AJAX)
        ================================================
        */
        wp_enqueue_script( $this->plugin_name . "-survey-styles", plugin_dir_url(__FILE__) . 'js/partials/survey-maker-admin-survey-styles.js', array('jquery', 'wp-color-picker'), $this->version, true);
        wp_enqueue_script( $this->plugin_name . "-functions", plugin_dir_url(__FILE__) . 'js/functions.js', array( 'jquery', 'wp-color-picker' ), $this->version, true );
        wp_enqueue_script( $this->plugin_name . '-ajax', plugin_dir_url(__FILE__) . 'js/survey-maker-admin-ajax.js', array('jquery'), $this->version, true);
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/survey-maker-admin.js', array( 'jquery' ), $this->version, true );
        wp_localize_script( $this->plugin_name, 'SurveyMakerAdmin', array( 
        	'ajaxUrl' => admin_url( 'admin-ajax.php' ),
            'longAnswerText'                    => __( 'Long answer text', $this->plugin_name ),
            'shortAnswerText'                   => __( 'Short answer text', $this->plugin_name ),
            'numberAnswerText'                  => __( 'Number answer text', $this->plugin_name ),
            'emailField'                        => __( 'Email field', $this->plugin_name ),
            'nameField'                         => __( 'Name field', $this->plugin_name ),
            'selectUserRoles'                   => __( 'Select user roles', $this->plugin_name ),
            'addQuestion'                       => __( 'Add question', $this->plugin_name ),
            'addSection'                        => __( 'Add section', $this->plugin_name ),
            'duplicate'                         => __( 'Duplicate', $this->plugin_name ),
            'delete'                            => __( 'Delete', $this->plugin_name ),
            'addImage'                          => __( 'Add Image', $this->plugin_name ),
            'editImage'                         => __( 'Edit Image', $this->plugin_name ),
            'removeImage'                       => __( 'Remove Image', $this->plugin_name ),
            'collapseSectionQuestions'          => __( 'Collapse section questions', $this->plugin_name ),
            'expandSectionQuestions'            => __( 'Expand section questions', $this->plugin_name ),
            'selectQuestionDefaultType'         => __( 'Select question default type', $this->plugin_name ),
            'chooseAnswer'                      => __( 'Choose answer', $this->plugin_name ),
            'yes'                               => __( 'Yes', $this->plugin_name ),
            'cancel'                            => __( 'Cancel', $this->plugin_name ),
            'questionDeleteConfirmation'        => __( 'Are you sure you want to delete this question?', $this->plugin_name ),
            'sectionDeleteConfirmation'         => __( 'Are you sure you want to delete this section?', $this->plugin_name ),
            'loadResource'                      => __( "Can't load resource.", $this->plugin_name ),
            'somethingWentWrong'                => __( "Maybe something went wrong.", $this->plugin_name ),
            'dataDeleted'                       => __( "Maybe the data has been deleted.", $this->plugin_name ),
            'minimumCountOfQuestions'           => __( 'Sorry minimum count of questions should be 1', $this->plugin_name ),
            'enableMaxSelectionCount'           => __( 'Enable selection count', $this->plugin_name ),
            'disableMaxSelectionCount'          => __( 'Disable max selection count', $this->plugin_name ),
            'enableWordLimitation'              => __( 'Enable word limitation', $this->plugin_name ),
            'disableWordLimitation'             => __( 'Disable word limitation', $this->plugin_name ),
            'enableNumberLimitation'            => __( 'Enable limitation', $this->plugin_name ),
            'disableNumberLimitation'           => __( 'Disable limitation', $this->plugin_name ),
            'successfullySent'                  => __( 'Successfully sent', $this->plugin_name ),
            'failed'                            => __( 'Failed', $this->plugin_name ),
            'selectPage'                        => __( 'Select page', $this->plugin_name ),
            'selectPostType'                    => __( 'Select post type', $this->plugin_name ),
            'copied'                            => __( 'Copied!', $this->plugin_name),
            'clickForCopy'                      => __( 'Click for copy', $this->plugin_name),
            'icons' => array(
                'radioButtonUnchecked' => SURVEY_MAKER_ADMIN_URL . '/images/icons/radio-button-unchecked.svg',
                'checkboxUnchecked' => SURVEY_MAKER_ADMIN_URL . '/images/icons/checkbox-unchecked.svg',
            ),
            'nextSurveyPage' => __( 'Are you sure you want to go to the next survey page?', $this->plugin_name),
        ) );

    }
    
    public function codemirror_enqueue_scripts($hook) {
        if(strpos($hook, $this->plugin_name) !== false){
            if(function_exists('wp_enqueue_code_editor')){
                $cm_settings['codeEditor'] = wp_enqueue_code_editor(array(
                    'type' => 'text/css',
                    'codemirror' => array(
                        'inputStyle' => 'contenteditable',
                        'theme' => 'cobalt',
                    )
                ));

                wp_enqueue_script('wp-theme-plugin-editor');
                wp_localize_script('wp-theme-plugin-editor', 'cm_settings', $cm_settings);

                wp_enqueue_style('wp-codemirror');
            }
        }
    }

    public function aysSurveyMakerVersionCompare($version1, $operator, $version2) {
   
        $_fv = intval ( trim ( str_replace ( '.', '', $version1 ) ) );
        $_sv = intval ( trim ( str_replace ( '.', '', $version2 ) ) );
       
        if (strlen ( $_fv ) > strlen ( $_sv )) {
            $_sv = str_pad ( $_sv, strlen ( $_fv ), 0 );
        }
       
        if (strlen ( $_fv ) < strlen ( $_sv )) {
            $_fv = str_pad ( $_fv, strlen ( $_sv ), 0 );
        }
       
        return version_compare ( ( string ) $_fv, ( string ) $_sv, $operator );
    }

    /**
     * Register the administration menu for this plugin into the WordPress Dashboard menu.
     *
     * @since    1.0.0
     */
    public function add_plugin_admin_menu(){

        /*
         * Add a settings page for this plugin to the Settings menu.
         *
         * NOTE:  Alternative menu locations are available via WordPress administration menu functions.
         *
         *        Administration Menus: http://codex.wordpress.org/Administration_Menus
         *
         */
        global $wpdb;
        $sql = "SELECT COUNT(*) FROM " . esc_sql( $wpdb->prefix . SURVEY_MAKER_DB_PREFIX ) . "submissions WHERE `read` = 0 OR `read` = 2 ";
        $unread_results_count = intval( $wpdb->get_var( $sql ) );
        $menu_item = ($unread_results_count == 0) ? 'Survey Maker' : 'Survey Maker' . '<span class="ays-survey-menu-badge ays-survey-results-bage">' . $unread_results_count . '</span>';
        
        $this->capability = $this->survey_maker_capabilities();
        $capability = $this->capability;
                
        $hook_survey_maker =add_menu_page(
            'Survey Maker', 
            $menu_item,
            $this->capability,
            $this->plugin_name,
            array($this, 'display_plugin_surveys_page'), 
            SURVEY_MAKER_ADMIN_URL . '/images/icons/survey_logo.png',
            '6.21'
        );
        add_action( "load-$hook_survey_maker", array( $this, 'add_tabs' ));
    }

    public function add_plugin_surveys_submenu(){
        $hook_survey_maker = add_submenu_page(
            $this->plugin_name,
            __('Surveys', $this->plugin_name),
            __('Surveys', $this->plugin_name),
            $this->capability,
            $this->plugin_name,
            array($this, 'display_plugin_surveys_page')
        );

        add_action("load-$hook_survey_maker", array($this, 'screen_option_surveys'));
        add_action("load-$hook_survey_maker", array($this, 'add_tabs'));
    }

    public function add_plugin_export_import_submenu(){
        $hook_exp_imp = add_submenu_page(
            $this->plugin_name,
            __('Export / Import', $this->plugin_name),
            __('Export / Import', $this->plugin_name),
            $this->capability,
            $this->plugin_name . '-export-import',
            array($this, 'display_plugin_export_import_page')
        );

        // add_action("load-$hook_exp_imp", array($this, 'screen_option_questions'));        
        add_action("load-$hook_exp_imp", array($this, 'add_tabs'));
    }

    public function add_plugin_survey_categories_submenu(){
        $hook_survey_categories = add_submenu_page(
            $this->plugin_name,
            __('Survey Categories', $this->plugin_name),
            __('Survey Categories', $this->plugin_name),
            $this->capability,
            $this->plugin_name . '-survey-categories',
            array($this, 'display_plugin_survey_categories_page')
        );

        add_action("load-$hook_survey_categories", array($this, 'screen_option_survey_categories'));
        add_action("load-$hook_survey_categories", array($this, 'add_tabs'));
    }

    public function add_plugin_submissions_submenu(){
        global $wpdb;
        
        $sql = "SELECT COUNT(*) FROM " . esc_sql( $wpdb->prefix . SURVEY_MAKER_DB_PREFIX ) . "submissions WHERE `read` = 0 OR `read` = 2 ";
        $unread_results_count = intval( $wpdb->get_var( $sql ) );

        $results_text = __('Submissions', $this->plugin_name);
        $menu_item = ( $unread_results_count == 0 ) ? $results_text : $results_text . '<span class="ays-survey-menu-badge ays-survey-results-bage">' . $unread_results_count . '</span>';

        $hook_submissions = add_submenu_page(
            $this->plugin_name,
            $results_text,
            $menu_item,
            $this->capability,
            $this->plugin_name . '-submissions',
            array($this, 'display_plugin_submissions_page')
        );

        add_action("load-$hook_submissions", array($this, 'screen_option_submissions'));
        add_action("load-$hook_submissions", array($this, 'add_tabs'));
        
        $hook_each_submission = add_submenu_page(
            'each_submission_slug',
            __('Each', $this->plugin_name),
            null,
            $this->capability,
            $this->plugin_name . '-each-submission',
            array($this, 'display_plugin_each_submission_page')
        );

        add_action("load-$hook_each_submission", array($this, 'screen_option_each_survey_submission'));
        add_action("load-$hook_each_submission", array($this, 'add_tabs'));

        add_filter('parent_file', array($this,'survey_maker_select_submenu'));
    }

    public function add_plugin_dashboard_submenu(){
        $hook_quizes = add_submenu_page(
            $this->plugin_name,
            __('How to use', $this->plugin_name),
            __('How to use', $this->plugin_name),
            $this->capability,
            $this->plugin_name . '-dashboard',
            array($this, 'display_plugin_setup_page')
        );
        add_action("load-$hook_quizes", array($this, 'add_tabs'));
    }

    public function add_plugin_general_settings_submenu(){
        $hook_settings = add_submenu_page( $this->plugin_name,
            __('General Settings', $this->plugin_name),
            __('General Settings', $this->plugin_name),
            'manage_options',
            $this->plugin_name . '-settings',
            array($this, 'display_plugin_settings_page') 
        );
        add_action("load-$hook_settings", array($this, 'screen_option_settings'));
        add_action("load-$hook_settings", array($this, 'add_tabs'));
    }

    public function add_plugin_featured_plugins_submenu(){
        $hook_featured_plugins = add_submenu_page( $this->plugin_name,
            __('Our products', $this->plugin_name),
            __('Our products', $this->plugin_name),
            $this->capability,
            $this->plugin_name . '-our-products',
            array($this, 'display_plugin_featured_plugins_page') 
        );
        add_action("load-$hook_featured_plugins", array($this, 'add_tabs'));
    }

    public function add_plugin_survey_features_plugins_submenu(){
        $hook_pro_features = add_submenu_page( $this->plugin_name,
            __('PRO Features', $this->plugin_name),
            __('PRO Features', $this->plugin_name),
            $this->capability,
            $this->plugin_name . '-survey-features',
            array($this, 'display_plugin_features_page') 
        );
        add_action("load-$hook_pro_features", array($this, 'add_tabs'));
    }

    public function add_plugin_popup_survey(){
        $hook_popup = add_submenu_page( $this->plugin_name,
            __('Popup survey', $this->plugin_name),
            __('Popup survey', $this->plugin_name),
            $this->capability,
            $this->plugin_name . '-popup-survey',
            array($this, 'display_plugin_popup_page') 
        );
        add_action("load-$hook_popup", array($this, 'add_tabs'));
    }

    public function survey_maker_select_submenu($file) {
        global $plugin_page;
        if ($this->plugin_name."-each-submission" == $plugin_page) {
            $plugin_page = $this->plugin_name."-submissions";
        }
        return $file;
    }
    
    protected function survey_maker_capabilities(){
        global $wpdb;
        return 'manage_options';

        $sql = "SELECT meta_value FROM {$wpdb->prefix}aysquiz_settings WHERE `meta_key` = 'user_roles'";
        $result = $wpdb->get_var($sql);
        
        $capability = 'manage_options';
        if($result !== null){
            $ays_user_roles = json_decode($result, true);
            if(is_user_logged_in()){
                $current_user = wp_get_current_user();
                $current_user_roles = $current_user->roles;
                $ishmar = 0;
                foreach($current_user_roles as $r){
                    if(in_array($r, $ays_user_roles)){
                        $ishmar++;
                    }
                }
                if($ishmar > 0){
                    $capability = "read";
                }
            }
        }
        return $capability;
    }


    /**
     * Add settings action link to the plugins page.
     *
     * @since    1.0.0
     */
    public function add_action_links($links){
        /*
        *  Documentation : https://codex.wordpress.org/Plugin_API/Filter_Reference/plugin_action_links_(plugin_file_name)
        */
        $settings_link = array(
            '<a href="' . admin_url('admin.php?page=' . $this->plugin_name) . '">' . __('Settings', $this->plugin_name) . '</a>',
            '<a href="https://ays-demo.com/wordpress-survey-plugin-free-demo/" target="_blank">' . __('Demo', $this->plugin_name) . '</a>',
            '<a href="https://ays-pro.com/wordpress/survey-maker" target="_blank" style="color:red;font-weight:bold;">' . __('Buy Now', $this->plugin_name) . '</a>',
        );
        return array_merge($settings_link, $links);

    }

    
    public function add_survey_row_meta( $links, $file ) {
        if ( SURVEY_MAKER_BASENAME == $file ) {
            $row_meta = array(
                'ays-survey-support'    => '<a href="' . esc_url( 'https://wordpress.org/support/plugin/survey-maker/' ) . '" target="_blank">' . esc_html__( 'Free Support', $this->plugin_name ) . '</a>'
                );

            return array_merge( $links, $row_meta );
        }
        return $links;
    }

    /**
     * Render the settings page for this plugin.
     *
     * @since    1.0.0
     */
    public function display_plugin_setup_page(){
        include_once('partials/survey-maker-admin-display.php');
    }

    public function display_plugin_surveys_page(){
        $action = (isset($_GET['action'])) ? sanitize_text_field($_GET['action']) : '';
        switch ($action) {
            case 'add':
                include_once('partials/surveys/actions/survey-maker-surveys-actions.php');
                break;
            case 'edit':
                include_once('partials/surveys/actions/survey-maker-surveys-actions.php');
                break;
            default:
                include_once('partials/surveys/survey-maker-surveys-display.php');
        }
    }

    public function display_plugin_survey_categories_page(){
        $action = (isset($_GET['action'])) ? sanitize_text_field($_GET['action']) : '';

        switch ($action) {
            case 'add':
                include_once('partials/surveys/actions/survey-maker-survey-categories-actions.php');
                break;
            case 'edit':
                include_once('partials/surveys/actions/survey-maker-survey-categories-actions.php');
                break;
            default:
                include_once('partials/surveys/survey-maker-survey-categories-display.php');
        }
    }

    public function display_plugin_submissions_page(){

        include_once('partials/submissions/survey-maker-submissions-display.php');
    }
    
    public function display_plugin_each_submission_page(){
        include_once 'partials/submissions/survey-maker-each-submission-display.php';
    }
    
    public function display_plugin_settings_page(){        
        include_once('partials/settings/survey-maker-settings.php');
    }

    public function display_plugin_export_import_page(){        
        include_once('partials/export-import/survey-maker-export-import-display.php');
    }

    public function display_plugin_featured_plugins_page(){
        include_once('partials/features/survey-maker-plugin-featured-display.php');
    }
    
    public function display_plugin_features_page(){
        include_once('partials/features/survey-maker-features-display.php');
    }

    public function display_plugin_popup_page(){
        include_once('partials/popup/survey-maker-popups-display.php');
    }


    public static function set_screen($status, $option, $value){
        return $value;
    }

    public function screen_option_surveys(){
        $option = 'per_page';
        $args = array(
            'label' => __('Surveys', $this->plugin_name),
            'default' => 20,
            'option' => 'surveys_per_page'
        );

        if( ! ( isset( $_GET['action'] ) && ( $_GET['action'] == 'add' || $_GET['action'] == 'edit' ) ) ){
            add_screen_option($option, $args);
        }

        $this->surveys_obj = new Surveys_List_Table($this->plugin_name);
        $this->settings_obj = new Survey_Maker_Settings_Actions($this->plugin_name);
    }

    public function screen_option_survey_categories(){
        $option = 'per_page';
        $args = array(
            'label' => __('Survey Categories', $this->plugin_name),
            'default' => 20,
            'option' => 'survey_categories_per_page'
        );
        
        if( ! ( isset( $_GET['action'] ) && ( $_GET['action'] == 'add' || $_GET['action'] == 'edit' ) ) ){
            add_screen_option($option, $args);
        }

        $this->surveys_categories_obj = new Survey_Categories_List_Table($this->plugin_name);
        $this->settings_obj = new Survey_Maker_Settings_Actions($this->plugin_name);
    }

    public function screen_option_questions(){
        $option = 'per_page';
        $args = array(
            'label' => __('Questions', $this->plugin_name),
            'default' => 20,
            'option' => 'survey_questions_per_page'
        );

        add_screen_option($option, $args);
        $this->questions_obj = new Survey_Questions_List_Table($this->plugin_name);
        $this->settings_obj = new Survey_Maker_Settings_Actions($this->plugin_name);
    }

    public function screen_option_questions_categories(){
        $option = 'per_page';
        $args = array(
            'label' => __('Question Categories', $this->plugin_name),
            'default' => 20,
            'option' => 'survey_question_categories_per_page'
        );

        add_screen_option($option, $args);
        $this->question_categories_obj = new Survey_Question_Categories_List_Table($this->plugin_name);
    }

    public function screen_option_submissions(){
        $option = 'per_page';
        $args = array(
            'label' => __('Submissions', $this->plugin_name),
            'default' => 20,
            'option' => 'survey_submissions_results_per_page'
        );

        add_screen_option($option, $args);
        $this->submissions_obj = new Submissions_List_Table( $this->plugin_name );
    }

    public function screen_option_each_survey_submission() {
        $option = 'per_page';
        $args = array(
            'label' => __('Results', $this->plugin_name),
            'default' => 50,
            'option' => 'survey_each_submission_results_per_page',
        );

        add_screen_option($option, $args);
        $this->each_submission_obj = new Survey_Each_Submission_List_Table($this->plugin_name);
    }
    
    public function screen_option_settings(){
        $this->settings_obj = new Survey_Maker_Settings_Actions($this->plugin_name);
    }

    public function deactivate_plugin_option(){
        $request_value = $_REQUEST['upgrade_plugin'];
        $upgrade_option = get_option( 'ays_survey_maker_upgrade_plugin', '' );
        if($upgrade_option === ''){
            add_option( 'ays_survey_maker_upgrade_plugin', $request_value );
        }else{
            update_option( 'ays_survey_maker_upgrade_plugin', $request_value );
        }
        ob_end_clean();
        $ob_get_clean = ob_get_clean();
        echo json_encode( array( 'option' => get_option( 'ays_survey_maker_upgrade_plugin', '' ) ) );
        wp_die();
    }

    public function survey_maker_admin_footer($a){
        if(isset($_REQUEST['page'])){
            if(false !== strpos( sanitize_text_field( $_REQUEST['page'] ), $this->plugin_name)){
                ?>
                <p style="font-size:13px;text-align:center;font-style:italic;">
                    <span style="margin-left:0px;margin-right:10px;" class="ays_heart_beat"><i class="ays_fa ays_fa_heart_o animated"></i></span>
                    <span><?php echo __( "If you love our plugin, please do big favor and rate us on", $this->plugin_name); ?></span> 
                    <a target="_blank" href='https://wordpress.org/support/plugin/survey-maker/reviews/?rate=5#new-post'>WordPress.org</a>
                    <span class="ays_heart_beat"><i class="ays_fa ays_fa_heart_o animated"></i></span>
                </p>
            <?php
            }
        }
    }

    public static function ays_restriction_string($type, $x, $length){
        $output = "";
        switch($type){
            case "char":                
                if(strlen($x)<=$length){
                    $output = $x;
                } else {
                    $output = substr($x,0,$length) . '...';
                }
                break;
            case "word":
                $res = explode(" ", $x);
                if(count($res)<=$length){
                    $output = implode(" ",$res);
                } else {
                    $res = array_slice($res,0,$length);
                    $output = implode(" ",$res) . '...';
                }
            break;
        }
        return $output;
    }    
    
    public static function validateDate($date, $format = 'Y-m-d H:i:s'){
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    public static function get_max_id( $table ) {
        global $wpdb;
        $db_table = $wpdb->prefix . SURVEY_MAKER_DB_PREFIX . $table;

        $sql = "SELECT MAX(id) FROM {$db_table}";

        $result = intval( $wpdb->get_var( $sql ) );

        return $result;
    }

    public function get_all_surveys(){
        global $wpdb;
        $surveys_table = $wpdb->prefix . "ayssurvey_surveys";
        $surveys = $wpdb->get_results("SELECT * FROM {$surveys_table}");
        return $surveys;
    }

    //--------------------- Xcho Survey export ------------------------------

    public function ays_surveys_export_json() {
        global $wpdb;

        $surveys_ids = isset($_REQUEST['surveys_ids']) ? array_map( 'sanitize_text_field', $_REQUEST['surveys_ids'] ) : array();
        $surveys_table = $wpdb->prefix . SURVEY_MAKER_DB_PREFIX . 'surveys';
        $survey_category_table = $wpdb->prefix. SURVEY_MAKER_DB_PREFIX . "survey_categories";
        $questions_table = $wpdb->prefix . SURVEY_MAKER_DB_PREFIX. 'questions';
        $questions_category_table = $wpdb->prefix . SURVEY_MAKER_DB_PREFIX . 'question_categories';
        $answers_table = $wpdb->prefix . SURVEY_MAKER_DB_PREFIX . 'answers';
        $sections_table = $wpdb->prefix . SURVEY_MAKER_DB_PREFIX . 'sections';
        if(empty($surveys_ids)){
            $where = '';
        }else{
            $where = " WHERE id IN (". implode(',', $surveys_ids) .") ";
        }
        $sql_survey_categories = "SELECT * FROM ".$survey_category_table;
        $survey_categories = $wpdb->get_results($sql_survey_categories, 'ARRAY_A');
        $survey_all_categories = array();
        foreach ($survey_categories as $survey_categories_key) {
            $survey_all_categories[$survey_categories_key['id']] = $survey_categories_key['title'];
        }
        $sql_surveys = "SELECT * FROM ".$surveys_table.$where;
        $surveys = $wpdb->get_results($sql_surveys, 'ARRAY_A');
        $data = array();
        $data['ays_survey_key'] = 1;
        $data['surveys'] = array();
        foreach ($surveys as $survey_key => &$survey) {
            $questions_id = trim($survey['question_ids'], ',');
            $survey_cat_ids = explode(',' , $surveys[$survey_key]['category_ids']);
            foreach ($survey_cat_ids as $survey_cat_key) {
                $surveys[$survey_key]['survey_categories'][$survey_cat_key] = $survey_all_categories[$survey_cat_key];
            }
            unset($survey['id']);
            unset($survey['category_ids']);
            if(empty($questions_id)){
                $survey["questions"] = array();
            }else{
                $sql_sections = "SELECT id,title,ordering FROM ".$sections_table." WHERE id IN (". esc_sql( $survey['section_ids'] ) .")";
                $sections = $wpdb->get_results($sql_sections, 'ARRAY_A');
                $sql_question_cat = "SELECT * FROM ".$questions_category_table;
                $questions_categories = $wpdb->get_results($sql_question_cat, 'ARRAY_A');
                $categories = array();
                foreach ($questions_categories as $question_key) {
                    $categories[$question_key['id']] = $question_key['title'];
                }
                $sql_questions = "SELECT * FROM ".$questions_table." WHERE id IN (". esc_sql( $questions_id ) .")" ;
                $all_questions = $wpdb->get_results($sql_questions, 'ARRAY_A');
                $cat_ids = '';
                foreach ($all_questions as $key => &$question) {
                    $all_questions[$key]['answers'] = $this->get_question_answers($question['id']);
                    $cat_ids = explode(',' , $all_questions[$key]['category_ids']);
                    foreach ($cat_ids as $cat_key) {
                        $all_questions[$key]['question_categories'][$cat_key] = $categories[$cat_key];
                    }
                }
            }
            $survey['sections'] = $sections;
            $survey['questions'] = $all_questions;
        }        
            $data['surveys'] = $surveys;

        $response = array(
            'status' => true,
            'data'   => $data,
            'title'  => 'surveys-export',
        );
        echo json_encode($response);
        wp_die();
    }   

    //------------------ Xcho Survey Import ---------------------

    public function ays_survey_import( $import_file ) {
        global $wpdb;
        $name_arr = explode('.', $import_file['name']);
        $type     = end($name_arr);

        $json = file_get_contents($import_file['tmp_name']);
        $json = json_decode($json, true);
        $json_key = isset($json['ays_survey_key']) ? $json['ays_survey_key'] : false;

        if($json_key) {
            $surveys_table             = $wpdb->prefix . SURVEY_MAKER_DB_PREFIX . "surveys";
            $questions_table           = $wpdb->prefix . SURVEY_MAKER_DB_PREFIX . "questions";
            $answers_table             = $wpdb->prefix . SURVEY_MAKER_DB_PREFIX . "answers";
            $survey_category_table     = $wpdb->prefix . SURVEY_MAKER_DB_PREFIX . "survey_categories";
            $questions_category_table  = $wpdb->prefix . SURVEY_MAKER_DB_PREFIX . "question_categories";
            $sections_table            = $wpdb->prefix . SURVEY_MAKER_DB_PREFIX . "sections";

            $user_id = get_current_user_id();
            $user    = get_userdata($user_id);
            $author  = array(
                'id'   => $user->ID,
                'name' => $user->data->display_name
            );
            
            $survey_all_unique_questions_id = array();

            // Question categories
            $categories_r = $wpdb->get_results("SELECT id, title FROM ".$questions_category_table, 'ARRAY_A');
            $categories = array();
            foreach($categories_r as $cat){
                $categories[$cat['id']] = strtolower($cat['title']);
            }

            // Survey categories
            $scategories_r = $wpdb->get_results("SELECT id, title FROM ".$survey_category_table, 'ARRAY_A');
            $scategories = array();
                       
            foreach($scategories_r as $cat){
                $scategories[$cat['id']] = strtolower($cat['title']);
            }
            $surveys = $json['surveys'];

            $imported_surveys = 0;
            $failed_surveys = 0;
            $imported_qusts = 0;
            $failed_qusts = 0;
            $imported_answers = 0;
            $failed_answers = 0;

            // Import surveys
            foreach ($surveys as $survey_val => $survey) {
                $scategory_ids           = array();
                $survey_question_id      = array();
                $for_question_import     = array();
                $section_all_ids         = array();
                $survey_category_all_ids = array();
                $survey_section_ids      = array();
                $survey_categrories      = $survey['survey_categories'];
                $questions               = $survey['questions'];
                $sections                = $survey['sections'];

                // Import survey categories
                foreach ($survey_categrories as $category_key => $category_val) {
                    $survey_category = 1;
                    $survey_category_file = 'Uncategorized';
                    if(isset($category_val)){
                        $survey_category_file = strval($category_val);
                    }

                    if($this->string_starts_with_number($survey_category_file)){
                        $survey_category = 1;
                    }elseif(in_array(strtolower($survey_category_file), $scategories)){
                        $category_id = array_search(strtolower($survey_category_file), $scategories);
                        if($category_id !== false){
                            $survey_category = intval($category_id);
                        }else{
                            $survey_category = 1;
                        }
                    }else{
                        $wpdb->insert(
                            $survey_category_table,
                            array( 'title'  =>  $survey_category_file),
                            array( '%s' )
                        );
                        $survey_category = $wpdb->insert_id;
                        $scategories[strval($survey_category)] = strtolower($survey_category_file);
                    }
                    $survey_category_all_ids[] = $survey_category;
                    
                    $category_id = $survey_category;
                }

                // Import sections
                foreach ($sections as $section_key => $section_value) {
                    $section_ordering = $this->get_max_id('sections');
                    $section_ordering++;
                    $wpdb->insert(
                        $sections_table,
                        array( 'title'     =>  $section_value['title'],
                               'ordering'  =>  $section_ordering
                        ),
                        array( '%s' ,
                               '%d' 
                        )
                    );
                    $survey_new_id = $wpdb->insert_id;
                    $section_all_ids[$section_value['id']] = $survey_new_id;
                    $survey_section_ids[] = $section_all_ids[$section_value['id']];
                }

                // Import questions
                foreach ($questions as $question_key => $question_value) {
                    $question_category_ids = array();
                    $question_section_ids  = array();
                    $question_categories   = $question_value['question_categories'];
                    $question_old_id       = $question_value['id'];
                    if(! array_key_exists($question_old_id."", $survey_all_unique_questions_id)){
                        $options = array(
                            'author' => $author,
                        );                        
                        $question_option = json_decode($question_value['options'], true);
                        $options['use_html'] = isset($question_option['use_html']) ? $question_option['use_html'] : "off";                        
                        $question_value['answers'] = isset($question_value['answers']) ? $question_value['answers'] : array();
                        
                        // Import Question categories
                        foreach ($question_categories as $question_categories_key => $question_categories_value) {
                            $question_category = 1;
                            $question_category_file = 'Uncategorized';
                            if(isset($question_categories_value)){
                                $question_category_file = strval($question_categories_value);                                
                            }
                            if($this->string_starts_with_number($question_category_file)){
                                $question_category = 1;
                            }elseif(in_array(strtolower($question_category_file), $categories)){
                                $question_category_id = array_search(strtolower($question_category_file), $categories);
                                if($question_category_id !== false){
                                    $question_category = intval($question_category_id);
                                }else{
                                    $question_category = 1;
                                }
                            }else{
                                $wpdb->insert(
                                    $questions_category_table,
                                    array( 'title'  =>  $question_category_file),
                                    array( '%s' )
                                );
                                $question_category = $wpdb->insert_id;
                                $categories[strval($question_category)] = strtolower($question_category_file);
                            }                            
                            $question_category_id = $question_category;
                            $question_category_ids[] = $question_category_id;                            
                        }
                        if(isset($section_all_ids[$question_value['section_id']])){
                            $question_section_id = $section_all_ids[$question_value['section_id']];                           
                        }                        
                        $question_content = htmlspecialchars_decode(isset($question_value['question']) && $question_value['question'] != '' ? $question_value['question'] : '', ENT_HTML5);
                        $question_image   = (isset($question_value['image']) && $question_value['image'] != '') ? $question_value['image'] : '';
                        $type             = (isset($question_value['type']) && $question_value['type'] != '') ? $question_value['type'] : 'radio';
                        $published        = (isset($question_value['status']) && $question_value['status'] != '') ? $question_value['status'] : 'published';
                        $trash_status     = (isset($question_value['trash_status']) && $question_value['trash_status'] != '') ? intval($question_value['trash_status']) : '';
                        $user_variant     = htmlspecialchars_decode(isset($question_value['user_variant']) && $question_value['user_variant'] != '' ? $question_value['user_variant'] : '', ENT_HTML5);
                        $user_explanation = (isset($question_value['user_explanation']) && $question_value['user_explanation'] != '') ? $question_value['user_explanation'] : 'off';
                        $weight           = (isset($question_value['weight']) && $question_value['weight'] != '') ? floatval($question_value['weight']) : 1;
                        $create_date      = current_time( 'mysql' );
                        $modified_date    = (isset($question_value['date_modified']) && $question_value['user_explanation'] != '') ? $question_value['date_modified'] : '';
                        $answers_get      = $question_value['answers'];
                        
                        // Collect answers
                        $answers = array();
                        foreach($answers_get as $answer_key => $answer){
                            $answer_content = (isset($answer['answer']) && $answer['answer'] != '') ? htmlspecialchars_decode($answer['answer'], ENT_HTML5) : '';
                            $image = (isset($answer['image']) && $answer['image'] != '') ? $answer['image'] : '';
                            $ordering = $answer_key + 1;
                            $placeholder = (isset($answer['placeholder']) && $answer['placeholder'] != '') ? htmlspecialchars_decode($answer['placeholder'], ENT_HTML5) : '';

                            $answers[] = array(
                                'answer'        => $answer_content,
                                'image'         => $image,
                                'ordering'      => $ordering,
                                'placeholder'   => $placeholder,
                            );
                        }
                        // Collect questions
                        $for_question_import[] = array(
                            'id'                        => $question_old_id,
                            'author_id'                 => $author['id'],
                            'section_id'                => $question_section_id,
                            'category_ids'              => implode(',' ,$question_category_ids),
                            'question'                  => $question_content,
                            'type'                      => $type,
                            'status'                    => $published,
                            'trash_status'              => $trash_status,
                            'date_created'              => $create_date,
                            'date_modified'             => $modified_date,                            
                            'user_variant'              => $user_variant,
                            'user_explanation'          => $user_explanation,                            
                            'image'                     => $question_image,
                            'options'                   => $options,
                            'answers'                   => $answers,
                        );
                    }else{
                        $survey_question_id[] = $survey_all_unique_questions_id[$question_value["id"]];
                    }
                }

                $imported = 0;
                $failed = 0;

                // Import collected questions and answers
                foreach($for_question_import as $key => $question) {                   
                    $qusetion_ordering = $this->get_max_id('questions');
                    $qusetion_ordering++;
                    $import_array = array(
                        'author_id'                 => $question['author_id'],
                        'section_id'                => $question['section_id'],
                        'category_ids'              => $question['category_ids'],
                        'question'                  => $question['question'],
                        'type'                      => $question['type'],
                        'status'                    => $question['status'],
                        'trash_status'              => $question['trash_status'],
                        'date_created'              => $question['date_created'],
                        'date_modified'             => $question['date_modified'],
                        'user_variant'              => $question['user_variant'],
                        'user_explanation'          => $question['user_explanation'],
                        'image'                     => $question['image'],
                        'options'                   => json_encode($question['options']),
                        'ordering'                  => $qusetion_ordering
                    );
                    $import_array_vals = array(
                        '%d', //author_id
                        '%d', //section_id
                        '%s', //category_ids
                        '%s', //question
                        '%s', //type
                        '%d', //published
                        '%s', //trash_status
                        '%s', //date_created
                        '%s', //date_modified
                        '%s', //user_variant
                        '%s', //user_explanation
                        '%s', //image                        
                        '%s', //options
                        '%d', //orderind
                    );

                    $quest_res = $wpdb->insert(
                        $questions_table,                       
                        $import_array,
                        $import_array_vals
                    );
                   
                    $question_id = $wpdb->insert_id;
                    $survey_question_id[] = $question_id;
                    $survey_all_unique_questions_id[$question["id"]] = $question_id;

                    $ordering = 1;
                    $answer_res_success = 0;
                    $answer_res_fail = 0;
                        foreach ( $question['answers'] as $answer ) {
                            $result = $wpdb->insert(
                                $answers_table,
                                array(
                                    'question_id'   => $question_id,
                                    'answer'        => $answer['answer'],
                                    'image'         => $answer['image'],
                                    'ordering'      => $answer['ordering'],                                
                                    'placeholder'   => $answer['placeholder'],
                                ),
                                array(
                                    '%d', // question_id
                                    '%s', // answer
                                    '%s', // image
                                    '%d', // ordering                                
                                    '%s'  // placeholder
                                )
                            );

                            if($result === false){
                                $answer_res_fail++;
                            }
                            if($result >= 0){
                                $answer_res_success++;
                            }
                        }

                    $imported_answers += $answer_res_success;
                    $failed_answers += $answer_res_fail;

                    if($quest_res === false){
                        $failed++;
                    }
                    if($quest_res >= 0){
                        $imported++;
                    }else{
                        $failed++;
                    }
                }

                // Import surveys
                $imported_qusts += $imported;
                $failed_qusts += $failed;
                $survey_ordering = $this->get_max_id('surveys');
                $survey_ordering++;

                $survey_res = $wpdb->insert(
                    $surveys_table,
                    array(
                        'author_id'         => $author['id'],
                        'title'             => htmlspecialchars_decode($survey['title'], ENT_HTML5),
                        'description'       => htmlspecialchars_decode($survey['description'], ENT_HTML5),
                        'category_ids'      => implode(',' , $survey_category_all_ids),
                        'question_ids'      => implode(',' , $survey_question_id),
                        'section_ids'       => implode(',' ,  $survey_section_ids),
                        'sections_count'    => $survey['sections_count'],
                        'questions_count'   => $survey['questions_count'],
                        'date_created'      => $survey['date_created'],
                        'date_modified'     => $survey['date_modified'],
                        'image'             => $survey['image'],
                        'status'            => $survey['status'],
                        'trash_status'      => $survey['trash_status'],
                        'ordering'          => $survey_ordering,
                        'post_id'           => $survey['post_id'],
                        'options'           => $survey['options']                        
                    ),
                    array(
                        '%d', // author id
                        '%s', // title
                        '%s', // description
                        '%s', // category_ids
                        '%s', // question_ids
                        '%s', // section_ids
                        '%d', // sections_count
                        '%d', // questions_count
                        '%s', // date_created
                        '%s', // date_modified
                        '%s', // image
                        '%s', // status
                        '%s', // trash_status
                        '%s', // ordering
                        '%d',  // post_id
                        '%s'  // options
                    )
                );
                if($survey_res === false){
                    $failed_surveys++;
                }
                if($survey_res >= 0){
                    $imported_surveys++;
                }else{
                    $failed_surveys++;
                }
            }
                
            $stats = array(
                'surveys_successed'   => $imported_surveys,
                'surveys_failed'      => $failed_surveys,
                'questions_successed' => $imported_qusts,
                'questions_failed'    => $failed_qusts,
                'answers_successed'   => $imported_answers,
                'answers_failed'      => $failed_answers
            );
            return $stats;            
        }
        return null;
    } 

    public static function string_starts_with_number($string){
        $match = preg_match('/^\d/', $string);
        if($match === 1){
            return true;
        }else{
            return false;
        }
    }

    public function get_question_answers( $question_id ) {
        global $wpdb;

        $sql = "SELECT * FROM {$wpdb->prefix}ayssurvey_answers WHERE question_id=" . absint( $question_id );

        $results = $wpdb->get_results( $sql, 'ARRAY_A' );
        foreach ($results as $key => &$result) {
            unset($result['id']);
            unset($result['question_id']);
        }

        return $results;
    }

    // Send test Mail
    public function ays_survey_send_testing_mail(){
        if(isset($_REQUEST['ays_survey_test_email']) && filter_var($_REQUEST['ays_survey_test_email'], FILTER_VALIDATE_EMAIL)){
            $nsite_url_base = get_site_url();
            $nsite_url_replaced = str_replace( array( 'http://', 'https://' ), '', $nsite_url_base );
            $nsite_url = trim( $nsite_url_replaced, '/' );
            $nno_reply = "noreply@".$nsite_url;

            if(isset($_REQUEST['ays_email_configuration_from_name']) && $_REQUEST['ays_email_configuration_from_name'] != "") {
                $uname = stripslashes($_REQUEST['ays_email_configuration_from_name']);
            } else {
                $uname = 'Survey Maker';
            }

            if(isset($_REQUEST['ays_survey_email_configuration_from_name']) && $_REQUEST['ays_survey_email_configuration_from_name'] != "") {
                $nfrom = "From: " . $uname . " <".stripslashes($_REQUEST['ays_survey_email_configuration_from_name']).">";
            }else{
                $nfrom = "From: " . $uname . " <survey_maker@".$nsite_url.">";
            }

            if(isset($_REQUEST['ays_survey_email_configuration_from_subject']) && $_REQUEST['ays_survey_email_configuration_from_subject'] != "") {
                $subject = stripslashes($_REQUEST['ays_survey_email_configuration_from_subject']);
            } else {
                $subject = stripslashes($_REQUEST['ays_title']);
            }

            $headers = $nfrom."\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
            $attachment = array();

            $message_content = (isset($_REQUEST['ays_survey_mail_message']) && !empty($_REQUEST['ays_survey_mail_message'])) ? stripslashes($_REQUEST['ays_survey_mail_message']) : __( "Message text", $this->plugin_name );
            
            $message = $message_content;
            $to = $_REQUEST['ays_survey_test_email'];

            $ays_send_test_mail = (wp_mail($to, $subject, $message, $headers, $attachment)) ? true : false;
            $response_text = __( "Test email delivered", $this->plugin_name );
            if($ays_send_test_mail === false){
                $response_text = __( "Test email not delivered", $this->plugin_name );
            }

            ob_end_clean();
            $ob_get_clean = ob_get_clean();
            echo json_encode(array(
                'status' => true,
                'mail' => $ays_send_test_mail,
                'message' => $response_text,
            ));
            wp_die();
        }else{
            ob_end_clean();
            $ob_get_clean = ob_get_clean();
            $response_text = __( "Test email not delivered", $this->plugin_name );
            echo json_encode(array(
                'status' => false,
                'message' => $response_text,
            ));
            wp_die();
        }
    }
    
    public function ays_survey_question_results( $survey_id, $submission_ids = null ){
        global $wpdb;
        // $survey_id = isset( $_GET['survey'] ) ? intval( $_GET['survey'] ) : null;

        if($survey_id === null){
            return array(
                'total_count' => 0,
                'questions' => array()
            );
        }

        $submitions_questiions_table = $wpdb->prefix . SURVEY_MAKER_DB_PREFIX . "submissions_questions";
        $answer_table = $wpdb->prefix . SURVEY_MAKER_DB_PREFIX . "answers";
        $question_table = $wpdb->prefix . SURVEY_MAKER_DB_PREFIX . "questions";
        $submitions_table = $wpdb->prefix . SURVEY_MAKER_DB_PREFIX . "submissions";
        $survey_section_table = $wpdb->prefix . SURVEY_MAKER_DB_PREFIX . "sections";
        $surveys_table = $wpdb->prefix . SURVEY_MAKER_DB_PREFIX . "surveys";

        $survey_options_sql = "SELECT options FROM {$surveys_table} WHERE id =". absint( $survey_id );
        $survey_options = $wpdb->get_var( $survey_options_sql );

        $survey_options = isset( $survey_options ) && $survey_options != '' ? json_decode( $survey_options, true ) : array();

        // Allow HTML in answers
        $survey_options[ 'survey_allow_html_in_answers' ] = isset($survey_options[ 'survey_allow_html_in_answers' ]) ? $survey_options[ 'survey_allow_html_in_answers' ] : 'off';
        $allow_html_in_answers = (isset($survey_options[ 'survey_allow_html_in_answers' ]) && $survey_options[ 'survey_allow_html_in_answers' ] == 'on') ? true : false;

        $question_ids = "SELECT question_ids FROM {$surveys_table} WHERE id =". absint( $survey_id );
        $question_ids_results = $wpdb->get_var( $question_ids );
        $ays_question_id = ($question_ids_results != '') ? $question_ids_results : null;

        if($ays_question_id == null){
            return array(
                'total_count' => 0,
                'questions' => array()
            );
        }

        $questions_ids_arr = explode(',',$ays_question_id);
        $answer_id = "SELECT a.id, a.answer, COUNT(s_q.answer_id) AS answer_count
                    FROM {$answer_table} AS a
                    LEFT JOIN {$submitions_questiions_table} AS s_q 
                    ON a.id = s_q.answer_id
                    WHERE s_q.survey_id=".absint( $survey_id ) ."
                    GROUP BY a.id";

        $answer_id_result = $wpdb->get_results($answer_id,'ARRAY_A');

        $for_checkbox = "SELECT a.id, a.answer, COUNT(s_q.answer_id) AS answer_count
                    FROM {$answer_table} AS a
                    LEFT JOIN {$submitions_questiions_table} AS s_q 
                    ON a.id = s_q.answer_id OR FIND_IN_SET( a.id, s_q.user_answer )
                    WHERE s_q.type = 'checkbox'
                    AND s_q.survey_id=".absint( $survey_id ) ."
                    GROUP BY a.id";

        $for_checkbox_result = $wpdb->get_results($for_checkbox,'ARRAY_A');

        $for_text_type = "SELECT a.id, a.answer, COUNT(s_q.id) AS answer_count
                    FROM {$answer_table} AS a
                    LEFT JOIN {$submitions_questiions_table} AS s_q 
                    ON a.id = s_q.answer_id OR FIND_IN_SET( a.id, s_q.user_answer )
                    WHERE s_q.type IN ('name', 'email', 'text', 'short_text', 'number')
                    AND s_q.survey_id=".absint( $survey_id ) ."
                    GROUP BY a.id";

        $for_text_type_result = $wpdb->get_results($for_checkbox,'ARRAY_A');
        $answer_count = array();
        $question_type = '';
        foreach ($answer_id_result as $key => $answer_count_by_id) {
            $ays_survey_answer_count = (isset($answer_count_by_id['answer_count']) && $answer_count_by_id['answer_count'] !="") ? absint(intval($answer_count_by_id['answer_count'])) : '';
            $answer_count[$answer_count_by_id['id']] = $ays_survey_answer_count;
        }

        foreach ($for_checkbox_result as $key => $answer_count_by_id) {
            $ays_survey_answer_count = (isset($answer_count_by_id['answer_count']) && $answer_count_by_id['answer_count'] !="") ? absint(intval($answer_count_by_id['answer_count'])) : '';
            $answer_count[$answer_count_by_id['id']] = $ays_survey_answer_count;
        }

        foreach ($for_text_type_result as $key => $answer_count_by_id) {
            $ays_survey_answer_count = (isset($answer_count_by_id['answer_count']) && $answer_count_by_id['answer_count'] !="") ? absint(intval($answer_count_by_id['answer_count'])) : '';
            $answer_count[$answer_count_by_id['id']] = $ays_survey_answer_count;
        }

        $question_by_ids = Survey_Maker_Data::get_question_by_ids( $questions_ids_arr );

        $select_answer_q_type = "SELECT type, user_answer, id, question_id
            FROM {$submitions_questiions_table}
            WHERE user_answer != '' 
                AND type != 'checkbox' 
                AND survey_id=". absint( $survey_id );

        $submission_answer_other = "SELECT question_id, user_variant
            FROM {$submitions_questiions_table}
            WHERE user_variant != ''
                AND survey_id=". absint( $survey_id );

        if( $submission_ids !== null ){
            if( is_array( $submission_ids ) ){
                $select_answer_q_type .= " AND submission_id IN (" . esc_sql( implode( ',', $submission_ids ) ) . ") ";
                $submission_answer_other .= " AND submission_id IN (" . esc_sql( implode( ',', $submission_ids ) ) . ") ";
            }
        }
            
        $result_answers_q_type = $wpdb->get_results($select_answer_q_type,'ARRAY_A');
        $result_answers_other = $wpdb->get_results($submission_answer_other,'ARRAY_A');
        $text_answer = array();
        foreach($result_answers_q_type as $key => $result_answer_q_type){
            $text_answer[$result_answer_q_type['type']][$result_answer_q_type['question_id']][] = $result_answer_q_type['user_answer'];
        }
        
        $other_answers = array();
        foreach($result_answers_other as $key => $result_answer_other){
            $other_answers[$result_answer_other['question_id']][] = $result_answer_other['user_variant'];
        }

        $text_types = array(
            'text',
            'short_text',
            'number',
            'name',
            'email',
        );

        //Question types different charts
        $ays_submissions_count  = array();
        $question_results = array();

        $total_count = 0;
        foreach ($question_by_ids as $key => $question) {
            $answers        = $question->answers;
            $question_id    = $question->id;
            $question_title = $question->question;
            $question_type  = $question->type;
            //questions
            $question_results[$question_id]['question_id'] = $question_id;
            $question_results[$question_id]['question'] = $question_title;
            $ays_answer = array();
            $question_answer_ids = array();
            //

            foreach ($answers as $key => $answer) {
                $answer_id    = $answer->id;
                $answer_title = $answer->answer;
                
                $ays_answer[$answer_id] = isset( $answer_count[$answer_id] ) ? $answer_count[$answer_id] : 0;
                $question_answer_ids[$answer_id] = $allow_html_in_answers ? sanitize_text_field( $answer_title ) : $answer_title;
            }
            
            //sum of submissions count per questions
            if($question_type == "checkbox"){
                $sub_checkbox_count = $this->ays_survey_get_submission_count($question->id, $question_type, $survey_id);
                $sum_of_count = $sub_checkbox_count;
            }else{
                $sum_of_count = array_sum( array_values( $ays_answer ) );
            }
            $question_results[$question_id]['otherAnswers'] = isset( $other_answers[$question->id] ) ? $other_answers[$question->id] : array();

            if( in_array( $question->type, $text_types ) ){
                $question_results[$question_id]['answers'] = isset( $text_answer[$question->type] ) ? $text_answer[$question->type] : '';
                $question_results[$question_id]['answerTitles'] = isset( $text_answer[$question->type] ) ? $text_answer[$question->type] : '';
                $question_results[$question_id]['sum_of_answers_count'] = isset( $text_answer[$question->type][$question->id] ) ? count( $text_answer[$question->type][$question->id] ) : 0;
                $question_results[$question_id]['sum_of_same_answers']  = isset( $text_answer[$question->type][$question->id] ) ? array_count_values( $text_answer[$question->type][$question->id] ) : 0;
            }else{
                $question_results[$question_id]['answers'] = $ays_answer;
                $question_results[$question_id]['answerTitles'] = $question_answer_ids;
                $question_results[$question_id]['sum_of_answers_count'] = $sum_of_count;
                if( $sum_of_count == 0 ){
                    $question_results[$question_id]['answers'] = array();
                }
            }

            // Answers for charts
            if( !empty( $question_results[$question_id]['otherAnswers'] ) ){
                $question_results[$question_id]['answers'][0] = count( $question_results[$question_id]['otherAnswers'] );
                $question_results[$question_id]['answerTitles'][0] = __( '"Other" answer(s)', $this->plugin_name );
                $question_results[$question_id]['same_other_count'] = array_count_values( $question_results[$question_id]['otherAnswers'] );

                if($question_type == "radio" || $question_type == "yesorno"){
                    $question_results[$question_id]['sum_of_answers_count'] += count( $question_results[$question_id]['otherAnswers'] );
                }

            }
            //

            $total_count += intval( $question_results[$question_id]['sum_of_answers_count'] );

            $question_results[$question_id]['question_type'] = $question->type;
        }

        return array(
            'total_count' => $total_count,
            'questions' => $question_results
        );
    }
    
    public function ays_survey_get_last_submission_id( $survey_id ){
        global $wpdb;

        if($survey_id === null){
            return array();
        }

        $submitions_table = $wpdb->prefix . SURVEY_MAKER_DB_PREFIX . "submissions";

        //submission of each result
        $submission = "SELECT * FROM {$submitions_table} WHERE survey_id=". absint( $survey_id ) ." ORDER BY id DESC LIMIT 1 ";
        $last_submission = $wpdb->get_row( $submission, 'ARRAY_A' );
        
        if( $last_submission == null ){
            return array();
        }
        return $last_submission;
    }

    public function ays_survey_individual_results_for_one_submission( $submission, $survey ){
        global $wpdb;
        $survey_id = isset( $survey['id'] ) ? absint( intval( $survey['id'] ) ) : null;

        if( is_null( $survey_id ) || empty( $submission )){
            return array(
                'sections' => array()
            );
        }

        $submitions_questiions_table = $wpdb->prefix . SURVEY_MAKER_DB_PREFIX . "submissions_questions";
        $submitions_table = $wpdb->prefix . SURVEY_MAKER_DB_PREFIX . "submissions";

        $ays_individual_questions_for_one_submission = array();
        $question_answer_id = array();
        $submission_id = isset( $submission['id'] ) && $submission['id'] != '' ? $submission['id'] : null;

        if( is_null( $submission_id ) ){
            return array(
                'sections' => array()
            );
        }

        $checkbox_ids = '';
        
        $individual_questions = "SELECT * FROM {$submitions_questiions_table} WHERE submission_id=" . absint( $submission_id );
        $individual_questions_results = $wpdb->get_results($individual_questions,'ARRAY_A');

        // Get user info
        $which_needed = "id,user_ip,user_id,user_name,user_email,submission_date";
        $individual_users = "SELECT ".$which_needed." FROM ".$submitions_table." WHERE id=" . absint( $submission_id );
        $individual_users_results = $wpdb->get_row($individual_users,'ARRAY_A');
        $user_id = isset($individual_users_results['user_id']) && $individual_users_results['user_id'] != "" ? $individual_users_results['user_id'] : 0;
        $user_real_name = __("Guest" , $this->plugin_name); 
        $user_real_email = ""; 
        if($user_id > 0){
            $user_data = get_userdata($user_id);
            $user_real_name = $user_data->data->display_name;
            $user_real_email = $user_data->data->user_email;
        }
        if(!isset($individual_users_results['user_name']) || (isset($individual_users_results['user_name']) && $individual_users_results['user_name'] == "")){
            $individual_users_results['user_name'] = $user_real_name;
        }

        if(!isset($individual_users_results['user_email']) || (isset($individual_users_results['user_email']) && $individual_users_results['user_email'] == "")){
            $individual_users_results['user_email'] = $user_real_email;
        }
        $individual_users_results['user_name'] =  stripslashes(nl2br( htmlentities($individual_users_results['user_name'])));
        // Survey questions IDs
        $question_ids = isset( $survey['question_ids'] ) && $survey['question_ids'] != '' ? $survey['question_ids'] : '';

        // Section Ids
        $sections_ids = (isset( $survey['section_ids' ] ) && $survey['section_ids'] != '') ? $survey['section_ids'] : '';

        $sections = Survey_Maker_Data::get_suervey_sections_with_questions( $sections_ids, $question_ids );

        $text_types = array(
            'text',
            'short_text',
            'number',
            'name',
            'email',
        );

        foreach ($individual_questions_results as $key => $individual_questions_result) {
            if($individual_questions_result['type'] == 'checkbox'){
                $checkbox_ids = $individual_questions_result['user_answer'] != '' ? explode(',', $individual_questions_result['user_answer']) : array();
                $question_answer_id[ $individual_questions_result['question_id'] ]['answer'] = $checkbox_ids;
                $question_answer_id[ $individual_questions_result['question_id'] ]['otherAnswer'] = isset($individual_questions_result['user_variant']) && $individual_questions_result['user_variant'] != '' ? $individual_questions_result['user_variant'] : '';
            }elseif( in_array( $individual_questions_result['type'], $text_types ) ){
                $question_answer_id[ $individual_questions_result['question_id'] ] = stripslashes(nl2br( htmlentities($individual_questions_result['user_answer'])));
            }elseif($individual_questions_result['type'] == 'radio'){
                $other_answer = isset($individual_questions_result['user_variant']) && $individual_questions_result['user_variant'] != '' ? stripslashes(nl2br( htmlentities($individual_questions_result['user_variant']))) : '';
                $question_answer_id[ $individual_questions_result['question_id'] ]['otherAnswer'] = $other_answer;
                $question_answer_id[ $individual_questions_result['question_id'] ]['answer'] = $individual_questions_result['answer_id'];
            }else{
                $question_answer_id[ $individual_questions_result['question_id'] ] = $individual_questions_result['answer_id'];
            }
        }

        $ays_individual_questions_for_one_submission['submission_id'] = $submission['id'];
        $ays_individual_questions_for_one_submission['questions'] = $question_answer_id;
        $ays_individual_questions_for_one_submission['sections'] = $sections;
        $ays_individual_questions_for_one_submission['user_info'] = $individual_users_results;
        return $ays_individual_questions_for_one_submission;
    }
    
    public function get_submission_count_and_ids(){
        global $wpdb;
        $survey_id = isset($_GET['survey']) ? intval($_GET['survey']) : null;

        if($survey_id === null){
            return false;
        }
        $submitions_table = $wpdb->prefix . SURVEY_MAKER_DB_PREFIX . "submissions";
       
        //submission of each result
        $submission_ids = "SELECT id,
                            (SELECT COUNT(id) FROM {$submitions_table} i 
                            WHERE i.survey_id=j.survey_id) AS count_submission 
                            FROM {$submitions_table} j 
                            WHERE survey_id=". absint( $survey_id ) ."
                            ORDER BY id";
        $submission_ids_result = $wpdb->get_results($submission_ids,'ARRAY_A');
        $submission_count = '';
        $submissions_id_arr = array();
        foreach ($submission_ids_result as $key => $submission_id_result) {
            $submission_id_count = $submission_id_result['count_submission'];
            $submission_count = intval($submission_id_count);
            $submissions_id_arr[] = $submission_id_result['id'];
        }
        $submissions_id_str = implode(',', $submissions_id_arr );
        
        $submission_count_and_ids = array(
            'submission_count' => $submission_count,
            'submission_ids' => $submissions_id_str
        );

        return $submission_count_and_ids;
    }

    public function ays_survey_submission_report(){
        global $wpdb;
        if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'ays_survey_submission_report') {

            $survey_id = (isset($_REQUEST['surveyId']) && $_REQUEST['surveyId'] != "") ? intval($_REQUEST['surveyId']) : null;
            if($survey_id === null){
                return false;
            }
            
            $sql = "SELECT * FROM " . $wpdb->prefix . SURVEY_MAKER_DB_PREFIX . "surveys WHERE id =" . absint( $survey_id );
            $survey = $wpdb->get_row( $sql, 'ARRAY_A' );

            $submission_id = (isset($_REQUEST['submissionId']) && $_REQUEST['submissionId'] != '') ? absint( sanitize_text_field( $_REQUEST['submissionId'] ) ) : null;
            if($submission_id == null){
                return false;
            }
            $submission = array(
                'id' => $submission_id
            );

            $results = $this->ays_survey_individual_results_for_one_submission( $submission, $survey );
            $individual_user_name   = isset($results['user_info']['user_name']) && isset($results['user_info']['user_name']) ? $results['user_info']['user_name'] : "";
            $individual_user_email  = isset($results['user_info']['user_email']) && isset($results['user_info']['user_email'])  ? esc_attr($results['user_info']['user_email']) : "";
            $individual_user_ip     = isset($results['user_info']['user_ip']) && isset($results['user_info']['user_ip'])  ? esc_attr($results['user_info']['user_ip']) : "";
            $individual_user_date   = isset($results['user_info']['submission_date']) && isset($results['user_info']['submission_date'])  ? esc_attr($results['user_info']['submission_date']) : "";
            $individual_user_sub_id = isset($results['user_info']['id']) && isset($results['user_info']['id'])  ? esc_attr($results['user_info']['id']) : "";
            $survey_data_clipboard = array(
                "user_name"   => $individual_user_name,
                "user_email"  => $individual_user_email,
                "user_ip"     => $individual_user_ip,
                "user_date"   => $individual_user_date,
                "user_sub_id" => $individual_user_sub_id,
            );
            ob_end_clean();
            $ob_get_clean = ob_get_clean();
            $response = array(
                'status' => true,
                'questions' => $results['questions'],
                'user_info' => $results['user_info'],
                'user_info_for_copy' => Survey_Maker_Data::ays_survey_copy_text_formater($survey_data_clipboard)
            );
            echo json_encode($response);
            wp_die();
        }
        ob_end_clean();
        $ob_get_clean = ob_get_clean();
        $response = array(
            'status' => false
        );
        echo json_encode($response);
        wp_die();
    }

    // Survey Maker Elementor widget init
    public function survey_maker_el_widgets_registered() {
        // We check if the Elementor plugin has been installed / activated.
        if ( defined( 'ELEMENTOR_PATH' ) && class_exists( 'Elementor\Widget_Base' ) ) {
            // get our own widgets up and running:
            // copied from widgets-manager.php
            if ( class_exists( 'Elementor\Plugin' ) ) {
                if ( is_callable( 'Elementor\Plugin', 'instance' ) ) {
                    $elementor = Elementor\Plugin::instance();
                    if ( isset( $elementor->widgets_manager ) ) {
                        if ( method_exists( $elementor->widgets_manager, 'register_widget_type' ) ) {
                            wp_enqueue_style($this->plugin_name . '-admin', plugin_dir_url(__FILE__) . 'css/admin.css', array(), $this->version, 'all');
                            wp_enqueue_style( SURVEY_MAKER_NAME . "-dropdown", SURVEY_MAKER_PUBLIC_URL . '/css/dropdown.min.css', array(), SURVEY_MAKER_VERSION, 'all' );
                            $widget_file   = 'plugins/elementor/survey-maker-elementor.php';
                            $template_file = locate_template( $widget_file );
                            if ( !$template_file || !is_readable( $template_file ) ) {
                                $template_file = SURVEY_MAKER_DIR . 'pb_templates/survey-maker-elementor.php';
                            }
                            if ( $template_file && is_readable( $template_file ) ) {
                                require_once $template_file;
                                Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Elementor\Widget_Survey_Maker_Elementor() );
                            }
                        }
                    }
                }
            }
        }
    }

    // Get Submissions count ( Checkbox )
    public function ays_survey_get_submission_count($id, $type, $survey_id){
        global $wpdb;
        $submitions_table   = $wpdb->prefix . SURVEY_MAKER_DB_PREFIX . "submissions";
        $submitions_q_table = $wpdb->prefix . SURVEY_MAKER_DB_PREFIX . "submissions_questions";
        $results = array();
        if($type == 'checkbox'){
            $sql = "SELECT COUNT(submission_id) AS sub_count
                    FROM {$submitions_q_table}
                    WHERE question_id = ". absint( $id )."
                    AND user_answer != ''
                    AND type='". esc_sql( $type ) ."'
                    AND survey_id=". absint( $survey_id );
            $results = $wpdb->get_row($sql,'ARRAY_A');
        }
        $submission_count = isset($results['sub_count']) && $results['sub_count'] != "" && $results['sub_count'] > 0 ? $results['sub_count'] : 0;
        return $submission_count;
    }

    public function ays_survey_sale_baner(){
        
        if(isset($_POST['ays_survey_maker_winter_sale_btn'])){
            update_option('ays_survey_maker_sale_notification', 1);
            update_option('ays_survey_maker_sale_date', current_time( 'mysql' ));
        }

        if(isset($_POST['ays_survey_maker_winter_sale_btn_for_two_months'])){
            update_option('ays_survey_maker_sale_dismiss_for_two_month', 1);
            update_option('ays_survey_maker_sale_date', current_time( 'mysql' ));
        }
    
        $ays_survey_maker_sale_date = get_option('ays_survey_maker_sale_date');
        $ays_survey_maker_sale_two_months = get_option('ays_survey_maker_sale_dismiss_for_two_month');

        $val = 60*60*24*5;
        if($ays_survey_maker_sale_two_months == 1){
            $val = 60*60*24*61;
        }

        $current_date = current_time( 'mysql' );
        $date_diff = strtotime($current_date) - intval(strtotime($ays_survey_maker_sale_date)) ;
        
        $days_diff = $date_diff / $val;
    
        if(intval($days_diff) > 0 ){
            update_option('ays_survey_maker_sale_notification', 0);
            update_option('ays_survey_maker_sale_dismiss_for_two_month', 0);
        }
    
        $ays_survey_maker_flag = intval(get_option('ays_survey_maker_sale_notification'));
        $ays_survey_maker_flag += intval(get_option('ays_survey_maker_sale_dismiss_for_two_month'));
        if( $ays_survey_maker_flag == 0 ){
            if (isset($_GET['page']) && strpos($_GET['page'], SURVEY_MAKER_NAME) !== false) {
                $this->ays_survey_maker_sale_message($ays_survey_maker_flag);
            }
        }
    }

    // public function ays_survey_maker_sale_message($flag){
    //     if($flag == 0){
    //         $content = array();

    //         $content[] = '<div id="ays-survey-dicount-month-main" class="notice notice-success is-dismissible ays_survey_dicount_info">';
    //             $content[] = '<div id="ays-survey-dicount-month" class="ays_survey_dicount_month">';
    //                 $content[] = '<a href="https://bit.ly/3wpBqNV" target="_blank" class="ays-survey-sale-banner-link" style="display:none;"><img src="' . SURVEY_MAKER_ADMIN_URL . '/images/mega_bundle_logo_box.png"></a>';

    //                 $content[] = '<div class="ays-survey-dicount-wrap-box">';

    //                 $content[] = '<strong style="font-weight: bold;">';
    //                     $content[] = __( "Limited Time <span class='ays-survey-dicount-wrap-color'>60%</span> SALE on <br><span><a href='https://bit.ly/3Hyr8QQ' target='_blank' class='ays-survey-dicount-wrap-color ays-survey-dicount-wrap-text-decoration' style='display:block;'>Black Friday Great Bundle</a></span> (Quiz + Survey + survey + Copy + Popup)!", SURVEY_MAKER_NAME );
    //                 $content[] = '</strong>';

    //                     $content[] = '<br>';


    //                     $content[] = '<strong>';
    //                             $content[] = __( "Hurry up! Ends on November 26. <a href='https://bit.ly/3Hyr8QQ' target='_blank'>Check it out!</a>", SURVEY_MAKER_NAME );
    //                     $content[] = '</strong>';
                        

                            
    //                 $content[] = '</div>';

    //                 $content[] = '<div class="ays-survey-dicount-wrap-box">';

    //                     $content[] = '<div id="ays-survey-maker-countdown-main-container">';
    //                         $content[] = '<div class="ays-survey-maker-countdown-container">';

    //                             $content[] = '<div id="ays-survey-countdown">';
    //                                 $content[] = '<ul>';
    //                                     $content[] = '<li><span id="ays-survey-countdown-days"></span>days</li>';
    //                                     $content[] = '<li><span id="ays-survey-countdown-hours"></span>Hours</li>';
    //                                     $content[] = '<li><span id="ays-survey-countdown-minutes"></span>Minutes</li>';
    //                                     $content[] = '<li><span id="ays-survey-countdown-seconds"></span>Seconds</li>';
    //                                 $content[] = '</ul>';
    //                             $content[] = '</div>';

    //                             $content[] = '<div id="ays-survey-countdown-content" class="emoji">';
    //                                 $content[] = '<span>🚀</span>';
    //                                 $content[] = '<span>⌛</span>';
    //                                 $content[] = '<span>🔥</span>';
    //                                 $content[] = '<span>💣</span>';
    //                             $content[] = '</div>';

    //                         $content[] = '</div>';

    //                         $content[] = '<form action="" method="POST">';
    //                             $content[] = '<div id="ays-survey-dismiss-buttons-content">';
    //                                 $content[] = '<button class="btn btn-link ays-button" name="ays_survey_maker_sale_btn" style="height: 32px; padding-left: 0">Dismiss ad</button>';
    //                                 $content[] = '<button class="btn btn-link ays-button" name="ays_survey_maker_sale_btn_for_two_months" style="height: 32px; padding-left: 0">Dismiss ad for 2 months</button>';
    //                             $content[] = '</div>';
    //                         $content[] = '</form>';
    //                     $content[] = '</div>';
                            
    //                 $content[] = '</div>';

    //                 $content[] = '<div class="ays-survey-dicount-wrap-box ays-buy-now-button-box">';
    //                     $content[] = '<a href="https://bit.ly/3Hyr8QQ" class="button button-primary ays-buy-now-button" id="ays-button-top-buy-now" target="_blank" style="" >' . __( 'Buy Now !', SURVEY_MAKER_NAME ) . '</a>';
    //                 $content[] = '</div>';

    //                 $content[] = '<div class="ays-survey-dicount-wrap-box ays-survey-dicount-wrap-opacity-box">';
    //                     $content[] = '<a href="https://bit.ly/3Hyr8QQ" class="ays-buy-now-opacity-button" target="_blank">' . __( 'link', SURVEY_MAKER_NAME ) . '</a>';
    //                 $content[] = '</div>';

    //             $content[] = '</div>';
    //         $content[] = '</div>';

    //         $content = implode( '', $content );
    //         echo $content;
    //     }
    // }
    
    // Winter bundle
    public function ays_survey_maker_sale_message($flag){
		$content = array();
		$content[] = '<div id="ays-survey-winter-dicount-main">';
			$content[] = '<div id="ays-survey-dicount-month-main" class="notice notice-success is-dismissible ays_survey_dicount_info">';
				$content[] = '<div id="ays-survey-dicount-month" class="ays_survey_dicount_month">';
					$content[] = '<a href="https://ays-pro.com/mega-bundle" target="_blank" class="ays-survey-sale-banner-link"><img src="' . SURVEY_MAKER_ADMIN_URL . '/images/mega_bundle_logo_box.png"></a>';

					$content[] = '<div class="ays-survey-dicount-wrap-box">';

						$content[] = '<strong>';
							$content[] = __( "The BIGGEST <span class='ays-survey-dicount-wrap-color' style='color:#001E64;'>50%</span> SALE on <br><span><a href='https://ays-pro.com/mega-bundle' target='_blank' class='ays-survey-dicount-wrap-color ays-survey-dicount-wrap-text-decoration' style='display:block;color:#001E64'>Mega Bundle</a></span> (Quiz+Survey+Poll)!", SURVEY_MAKER_ADMIN_URL );
						$content[] = '</strong>';

						$content[] = '<br>';

						$content[] = '<strong>';
								$content[] = __( "Hurry up! Ending on. <a href='https://ays-pro.com/mega-bundle' target='_blank' style='color:#001E64;'>Check it out!</a>", SURVEY_MAKER_ADMIN_URL );
						$content[] = '</strong>';
							
					$content[] = '</div>';

					$content[] = '<div class="ays-survey-dicount-wrap-box">';

						$content[] = '<div id="ays-survey-maker-countdown-main-container">';
							$content[] = '<div class="ays-survey-maker-countdown-container">';

								$content[] = '<div id="ays-survey-countdown">';
									$content[] = '<ul>';
										$content[] = '<li><span id="ays-survey-countdown-days"></span>days</li>';
										$content[] = '<li><span id="ays-survey-countdown-hours"></span>Hours</li>';
										$content[] = '<li><span id="ays-survey-countdown-minutes"></span>Minutes</li>';
										$content[] = '<li><span id="ays-survey-countdown-seconds"></span>Seconds</li>';
									$content[] = '</ul>';
								$content[] = '</div>';

								$content[] = '<div id="ays-survey-countdown-content" class="emoji">';
									$content[] = '<span>🚀</span>';
									$content[] = '<span>⌛</span>';
									$content[] = '<span>🔥</span>';
									$content[] = '<span>💣</span>';
								$content[] = '</div>';

							$content[] = '</div>';

							$content[] = '<form action="" method="POST">';
								$content[] = '<button class="btn btn-link ays-button" name="ays_survey_maker_winter_sale_btn" style="height: 32px;color:#0041B8" value="winter_bundle">Dismiss ad</button>';
								$content[] = '<button class="btn btn-link ays-button" name="ays_survey_maker_winter_sale_btn_for_two_months" style="height: 32px; padding-left: 0;color:#0041B8" value="winter_bundle">Dismiss ad for 2 months</button>';
							$content[] = '</form>';

						$content[] = '</div>';
							
					$content[] = '</div>';

					$content[] = '<a href="https://ays-pro.com/mega-bundle" class="button button-primary ays-button" id="ays-survey-button-top-buy-now" target="_blank">' . __( 'Buy Now !', SURVEY_MAKER_ADMIN_URL ) . '</a>';
				$content[] = '</div>';
			$content[] = '</div>';
		// $content[] = '</div>';
		$content = implode( '', $content );
		echo $content;
    }

    public function ays_live_preivew_content(){

        $content = isset($_REQUEST['content']) && $_REQUEST['content'] != '' ? wp_kses_post( $_REQUEST['content'] ) : null;
        if($content === null){
            ob_end_clean();
            $ob_get_clean = ob_get_clean();
            echo json_encode(array(
                'status' => false,
            ));
        }
        // $content = Survey_Maker_Data::ays_autoembed( $content );
        $content = stripslashes( wpautop( $content ) );
        ob_end_clean();
        $ob_get_clean = ob_get_clean();
        echo json_encode(array(
            'status' => true,
            'content' => $content,
        ));
        wp_die();
    }
    
	public function add_tabs() {
		$screen = get_current_screen();
	
		if ( ! $screen) {
			return;
		}
        
        $title   = __( 'General Information:', $this->plugin_name);
        $content_text = 'Get real-time feedback with the Survey Maker plugin. You are free to generate unlimited online surveys with unlimited questions and sections. Easily create your customer satisfaction surveys, employee engagement forms, market researches, event planning questionnaires with this plugin.
                        <br><br>Increase users’ track to your WordPress website with the Survey Maker features. Build smarter surveys with LogicJump, advance your questionnaires with Conditional Results, earn money with Paid Surveys, generate leads super easily, get valuable feedback.';

        $sidebar_content = '<p><strong>' . __( 'For more information:', $this->plugin_name) . '</strong></p>' .
                            '<p>
                                <a href="https://www.youtube.com/watch?v=Q1qi649acb0" target="_blank">' . __( 'Youtube video tutorials' , $this->plugin_name ) . '</a>
                            </p>' .
                            '<p>
                                <a href="https://ays-pro.com/wordpress-survey-maker-user-manual" target="_blank">' . __( 'Documentation', $this->plugin_name ) . '</a>
                            </p>' .
                            '<p>
                                <a href="https://ays-pro.com/wordpress/survey-maker" target="_blank">' . __( 'Survey Maker plugin pro version', $this->plugin_name ) . '</a>
                            </p>' .
                            '<p>
                                <a href="https://ays-demo.com/wordpress-survey-plugin-pro-demo/" target="_blank">' . __( 'Survey Maker plugin demo', $this->plugin_name ) . '</a>
                            </p>';

        
        $content =  '<h2>' . __( 'Survey Maker Information', $this->plugin_name) . '</h2>'
                   .'<p>' .sprintf(__( '%s',  $this->plugin_name ), $content_text).'</p>';

        $help_tab_content = array(
            'id'      => 'survey_maker_help_tab',
            'title'   => $title,
            'content' => $content
        );
        
		$screen->add_help_tab($help_tab_content);

		$screen->set_help_sidebar($sidebar_content);
	}

    public function get_next_or_prev_survey_by_id( $id, $type = "next" ) {
        global $wpdb;

        $surveys_table = esc_sql( $wpdb->prefix . "ayssurvey_surveys" );

        $where = array();
        $where_condition = "";

        $id     = (isset( $id ) && $id != "" && absint($id) != 0) ? absint( sanitize_text_field( $id ) ) : null;
        $type   = (isset( $type ) && $type != "") ? sanitize_text_field( $type ) : "next";

        if ( is_null( $id ) || $id == 0 ) {
            return null;
        }

        switch ( $type ) {
            case 'next':
            default:
                $where[] = ' `id` > ' . $id;
                break;
        }

        if( ! empty($where) ){
            $where_condition = " WHERE " . implode( " AND ", $where );
        }

        $sql = "SELECT `id` FROM {$surveys_table} ". $where_condition ." LIMIT 1;";
        $results = $wpdb->get_row( $sql, 'ARRAY_A' );

        return $results;

    }

}
