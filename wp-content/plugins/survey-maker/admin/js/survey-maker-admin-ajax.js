(function( $ ) {
    'use strict';
    $.fn.serializeFormJSON = function () {
        let o = {},
            a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    
    $.fn.aysModal = function(action){
        let $this = $(this);
        switch(action){
            case 'hide':
                $(this).find('.ays-modal-content').css('animation-name', 'zoomOut');
                setTimeout(function(){
                    $(document.body).removeClass('modal-open');
                    $(document).find('.ays-modal-backdrop').remove();
                    $this.hide();
                }, 250);
            break;
            case 'show': 
            default:
                $this.show();
                $(this).find('.ays-modal-content').css('animation-name', 'zoomIn');
                $(document).find('.modal-backdrop').remove();
                $(document.body).append('<div class="ays-modal-backdrop"></div>');
                $(document.body).addClass('modal-open');
            break;
        }
    };

    $(document).find('.ays_survey_test_mail_btn').on('click', function(e) {
        var form = $(document).find('#ays-quiz-category-form');
        var del_message = $(document).find('#ays_test_delivered_message');
        var loader = '<img src="'+ del_message.data('src') +'">';
        del_message.html(loader);
        del_message.show();
        var data = form.serializeFormJSON();
        var action = 'ays_survey_send_testing_mail';
        data.action = action;
        $.ajax({
            url: ajaxurl,
            method: 'post',
            dataType: 'json',
            data: data,
            success: function(response) {
                if (response.status) {
                    if(response.mail){
                        del_message.css("color", "green");
                    }else{
                        del_message.css("color", "red");
                    }
                    del_message.html(response.message);
                }else{
                    del_message.html(response.message);
                    del_message.css("color", "red");
                }
                setTimeout(function(){
                    del_message.fadeOut(500);
                }, 1500);
            }
        });
    });

    $(document).find('.ays_export_surveys').on('click', function(){
            var form = $(this).parents('form');
            var data = form.serializeFormJSON();
            data.action = 'ays_surveys_export_json';
            $.ajax({
                url: ajaxurl,
                dataType: 'json',
                data: data,
                method: 'post',
                success: function(response){
                    if (response.status) {
                        var text = JSON.stringify(response.data);
                        var data = new Blob([text], {type: "application/json"});
                        var fileUrl = window.URL.createObjectURL(data);
                        $('#downloadFile').attr({
                            'href': fileUrl,
                            'download': response.title+".json",
                        })[0].click();
                        window.URL.revokeObjectURL(fileUrl);
                    }
                }
            });
        });

    $(document).on('click', '.ays-survey-apply-question-changes', function(e){
        var sectionCont = $(document).find('.ays-survey-sections-conteiner');
        var editorPopup = $(document).find('#ays-edit-question-content');
        var questionId = $(this).attr('data-question-id');
        var questionName = $(this).attr('data-question-name');
        var sectionId = $(this).attr('data-section-id');
        var sectionName = $(this).attr('data-section-name');
        var question = sectionCont.find('.ays-survey-section-box[data-id="'+sectionId+'"][data-name="'+sectionName+'"] .ays-survey-question-answer-conteiner[data-id="'+questionId+'"][data-name="'+questionName+'"]');

        var editor = window.tinyMCE.get('ays_survey_question_editor');
        var questionContent = '';

        question.find('.ays-survey-open-question-editor-flag').val('on');

        editorPopup.find('.ays-survey-preloader').css('display', 'flex');

        if ( editorPopup.find("#wp-ays_survey_question_editor-wrap").hasClass("tmce-active")){
            questionContent = editor.getContent();
        }else{
            questionContent = editorPopup.find('#ays_survey_question_editor').val();
        }
        var action = 'ays_live_preivew_content';
        var data = {};
        data.action = action;
        data.content = questionContent;
        $.ajax({
            url: ajaxurl,
            method: 'post',
            dataType: 'json',
            data: data,
            success: function(response) {
                if (response.status) {
                    editorPopup.find('.ays-survey-preloader').css('display', 'none');
                    question.find('textarea.ays-survey-question-input-textarea').val( questionContent );
                    question.find('.ays-survey-question-preview-box').html( response.content );

                    question.find('.ays-survey-question-input-box').addClass('display_none');
                    question.find('.ays-survey-question-preview-box').removeClass('display_none');

                    editorPopup.find('.ays-survey-apply-question-changes').attr( 'data-question-id', '' );
                    editorPopup.find('.ays-survey-apply-question-changes').attr( 'data-question-name', '' );
                    editorPopup.find('.ays-survey-back-to-textarea').attr( 'data-question-id', '' );
                    editorPopup.find('.ays-survey-back-to-textarea').attr( 'data-question-name', '' );
                    var SurveyTinyMCE = window.tinyMCE.get('ays_survey_question_editor');
                    if(SurveyTinyMCE != null){
                        SurveyTinyMCE.setContent( '' );
                    }
                    else{
                        $(document).find('#ays_survey_question_editor').val(" ");
                    }
                    
                    editorPopup.aysModal('hide');
                }
            }
        });
    });


    // ===============================================================
    // ======================      Xcho      =========================
    // ===============================================================
    
    // Slack integration
    $(document).on('click', '#slackOAuthGetToken', function () {
        var clientId = $("#ays_slack_client").val(),
            clientSecret = $("#ays_slack_secret").val(),
            clientCode = $(this).attr('data-code'),
            successText = $(this).attr('data-success');
        if (clientId == '' || clientSecret == "" || clientCode == "") {
            return false;
        }
        $('#ays_submit').prop('disabled', true);
        $.post({
            url: "https://slack.com/api/oauth.access",
            data: {
                client_id: clientId,
                client_secret: clientSecret,
                code: clientCode
            },
            success: function (res) {
                $('#slackOAuthGetToken')
                    .text(successText)
                    .toggleClass('btn-secondary btn-success pointer-events-none');
                $('#ays_slack_token').val(res.access_token);
                $('#ays_submit').prop('disabled', false);
            }
        });
    });


    $("#testZapier").on('click', function () {
        var AysSurvey = {};
        var $this = $(this);
        $this.prop('disabled', true);
        $("#testZapierFields").find("input").each(function () {
            if ($(this).prop('checked')) {
                switch ($(this).val()) {
                    case "ays_user_name":
                        AysSurvey[$(this).data('name')] = "John Smith";
                        break;
                    case "ays_user_email":
                        AysSurvey[$(this).data('name')] = "john_smith@example.com";
                        break;
                    case "ays_user_phone":
                        AysSurvey[$(this).data('name')] = "+123123123";
                        break;
                    default:
                        AysSurvey[$(this).data('name')] = "Test data";
                        break;
                }
            }
        });
        $.post({
            url: $this.attr('data-url'),
            dataType: 'json',
            data: {
                "AysSurvey": JSON.stringify(AysSurvey)
            },
            success: function (response) {
                $this.prop('disabled', false);
                if (response.status) {
                    $this.removeClass('btn-outline-secondary').addClass('btn-success').text('Successfully sent')
                } else {
                    $this.removeClass('btn-outline-secondary').addClass('btn-danger').text('Failed')
                }
            },
            error: function () {
                $this.prop('disabled', false).removeClass('btn-outline-secondary').addClass('btn-danger').text('Failed')
            }
        });
    });
    
    
})( jQuery );
