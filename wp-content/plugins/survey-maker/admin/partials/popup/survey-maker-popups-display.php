<div class="wrap">
    <h1 class="wp-heading-inline display_none">
        <?php
        echo __(esc_html(get_admin_page_title()),$this->plugin_name);
        ?>
    </h1>
    <div class="form-group row" style="margin:0px;">
        <div class="col-sm-12 only_pro">
            <div class="pro_features">
                <div>
                    <p>
                        <?php echo __("This feature is available only in ", $this->plugin_name); ?>
                        <a href="https://ays-pro.com/wordpress/survey-maker/" target="_blank" title="PRO feature"><?php echo __("PRO version!!!", $this->plugin_name); ?></a>
                    </p>
                </div>
            </div>
            <div>
                <img src="<?php echo SURVEY_MAKER_ADMIN_URL.'/images/screenshots/survey-popup.png'?>" alt="Popup" style="width:100%;" >
            </div>
        </div>
    </div>
</div>
