<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://ays-pro.com/
 * @since      1.0.0
 *
 * @package    Survey_Maker
 * @subpackage Survey_Maker/admin/partials
 */

$survey_page_url = sprintf('?page=%s', 'survey-maker');
$add_new_url = sprintf('?page=%s&action=%s', 'survey-maker', 'add');

?>
<div class="wrap">
    <!-- <div class="ays-survey-maker-wrapper" style="position:relative;">
        <h1 class="ays_heart_beat"><?php echo __(esc_html(get_admin_page_title()),$this->plugin_name); ?> <i class="ays_fa ays_fa_heart_o animated"></i></h1>
    </div> -->
    <div class="ays-survey-heart-beat-main-heading ays-survey-heart-beat-main-heading-container">
        <h1 class="ays-survey-maker-wrapper ays_heart_beat">
            <?php echo __(esc_html(get_admin_page_title()),$this->plugin_name); ?> <i class="ays_fa ays_fa_heart_o animated"></i>
        </h1>
    </div>
    <div class="ays-survey-faq-main">
        <h2>
            <?php echo __("How to create a simple survey in 3 steps with the help of the", $this->plugin_name ) .
            ' <strong>'. __("Survey Maker", $this->plugin_name ) .'</strong> '.
            __("plugin.", $this->plugin_name ); ?>
            
        </h2>
        <fieldset>
            <div class="ays-survey-ol-container">
                <ol>
                    <li>
                        <?php echo __( "Go to the", $this->plugin_name ) . ' <a href="'. $survey_page_url .'" target="_blank">'. __( "Surveys" , $this->plugin_name ) .'</a> ' .  __( "page and build your first survey by clicking on the", $this->plugin_name ) . ' <a href="'. $add_new_url .'" target="_blank">'. __( "Add New" , $this->plugin_name ) .'</a> ' .  __( "button", $this->plugin_name ); ?>,
                    </li>
                    <li>
                        <?php echo __( "Fill out the information by adding a title, creating questions and so on.", $this->plugin_name ); ?>
                    </li>
                    <li>
                        <?php echo __( "Copy the", $this->plugin_name ) . ' <strong>'. __( "shortcode" , $this->plugin_name ) .'</strong> ' .  __( "of the survey and paste it into any post․", $this->plugin_name ); ?> 
                    </li>
                </ol>
            </div>
            <div class="ays-survey-p-container">
                <p><?php echo __("Congrats! You have already created your first survey." , $this->plugin_name); ?></p>
            </div>
        </fieldset>
    </div>
    <br>

    <div class="ays-survey-community-wrap">
        <div class="ays-survey-community-title">
            <h4><?php echo __( "Community", $this->plugin_name ); ?></h4>
        </div>
        <div class="ays-survey-community-youtube-video">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/5II14kyZ4ps" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="ays-survey-community-container">
            <div class="ays-survey-community-item">
                <a href="https://www.youtube.com/channel/UC-1vioc90xaKjE7stq30wmA" target="_blank" class="ays-survey-community-item-cover" style="color: #ff0000;background-color: rgba(253, 38, 38, 0.1);">
                    <i class="ays-survey-community-item-img ays_fa ays_fa_youtube_play"></i>
                </a>
                <h3 class="ays-survey-community-item-title"><?php echo __( "YouTube community", $this->plugin_name ); ?></h3>
                <p class="ays-survey-community-item-desc"></p>
                <div class="ays-survey-community-item-footer">
                    <a href="https://www.youtube.com/channel/UC-1vioc90xaKjE7stq30wmA" target="_blank" class="button"><?php echo __( "Subscribe", $this->plugin_name ); ?></a>
                </div>
            </div>
            <div class="ays-survey-community-item">
                <a href="https://wordpress.org/support/plugin/survey-maker/" target="_blank" class="ays-survey-community-item-cover" style="color: #0073aa;background-color: rgb(220, 220, 220);">
                    <i class="ays-survey-community-item-img ays_fa ays_fa_wordpress"></i>
                </a>
                <h3 class="ays-survey-community-item-title"><?php echo __( "Free support", $this->plugin_name ); ?></h3>
                <p class="ays-survey-community-item-desc"></p>
                <div class="ays-survey-community-item-footer">
                    <a href="https://wordpress.org/support/plugin/survey-maker/" target="_blank" class="button"><?php echo __( "Join", $this->plugin_name ); ?></a>
                </div>
            </div>
            <div class="ays-survey-community-item">
                <a href="https://ays-pro.com/contact" target="_blank" class="ays-survey-community-item-cover" style="color: #ff0000;background-color: rgba(0, 115, 170, 0.22);">
                    <img class="ays-survey-community-item-img" src="<?php echo SURVEY_MAKER_ADMIN_URL; ?>/images/logo_final.png">
                </a>
                <h3 class="ays-survey-community-item-title"><?php echo __( "Premium support", $this->plugin_name ); ?></h3>
                <p class="ays-survey-community-item-desc"></p>
                <div class="ays-survey-community-item-footer">
                    <a href="https://ays-pro.com/contact" target="_blank" class="button"><?php echo __( "Contact", $this->plugin_name ); ?></a>
                </div>
            </div>
        </div>
    </div>

    <div class="ays-survey-faq-main">
        <div class="ays-survey-asked-questions">
            <h4><?php echo __("FAQs" , $this->plugin_name); ?></h4>
            <div class="ays-survey-asked-question">
                <div class="ays-survey-asked-question__header">
                    <div class="ays-survey-asked-question__title">
                        <h4><strong><?php echo __( "Can I create a multi-step survey with the Free version?", $this->plugin_name ); ?></strong></h4>
                    </div>
                    <div class="ays-survey-asked-question__arrow"><i class="fa fa-chevron-down"></i></div>
                </div>
                <div class="ays-survey-asked-question__body">
                    <p>
                        <?php 
                            echo '<strong>'. __( "You can!" , $this->plugin_name ) .'</strong> ' .
                                __( "With the help of the plugin, you can create unlimited online surveys with multi sections. Quick, easy, and reliable for creating surveys interesting and engaging to take.", $this->plugin_name ) .
                                ' <em>'. __( "A drastic increase in conversion rate is guaranteed." , $this->plugin_name ) .'</em>';
                        ?>
                    </p>
                </div>
            </div>
            <div class="ays-survey-asked-question">
                <div class="ays-survey-asked-question__header">
                    <div class="ays-survey-asked-question__title">
                        <h4><strong><?php echo __("How do I make one question per page in Survey Maker?" , $this->plugin_name); ?></strong></h4>
                    </div>
                    <div class="ays-survey-asked-question__arrow"><i class="fa fa-chevron-down"></i></div>
                </div>
                <div class="ays-survey-asked-question__body">
                    <p>
                        <?php 
                            echo sprintf( 
                                __( "To achieve that, you need to divide your survey into separate sections and write one question per section. If you want to drive more traffic and use it as a %slead generator%s, then the plugin is %sperfect for your needs.%s It %sdoesn’t require any coding to build your surveys,%s yet it is %seasy to customize%s.", $this->plugin_name ),
                                '<strong>',
                                '</strong>',
                                '<strong>',
                                '</strong>',
                                '<strong>',
                                '</strong>',
                                '<strong>',
                                '</strong>'
                            );
                        ?>
                    </p>
                </div>
            </div>
            <div class="ays-survey-asked-question">
                <div class="ays-survey-asked-question__header">
                    <div class="ays-survey-asked-question__title">
                        <h4><strong><?php echo __( "Will I lose the data after upgrading to the Pro version?", $this->plugin_name ); ?></strong></h4>
                    </div>
                    <div class="ays-survey-asked-question__arrow"><i class="fa fa-chevron-down"></i></div>
                </div>
                <div class="ays-survey-asked-question__body">
                    <p>
                        <?php 
                            echo '<strong>'. __( "Nope, you will not!" , $this->plugin_name ) .'</strong> ' .
                                __( "All the data(surveys, settings, and submissions) of the plugin will remain unchanged even after switching to the Pro version. You don’t need to redo what you have already created with the free version.", $this->plugin_name ) .
                                ' <em>'. __( "For further detailed instructions, please check out the upgrade guide of the plugin." , $this->plugin_name ) .'</em>';
                        ?>
                    </p>
                </div>
            </div>
            <div class="ays-survey-asked-question">
                <div class="ays-survey-asked-question__header">
                    <div class="ays-survey-asked-question__title">
                        <h4><strong><?php echo __( "Where can I find the summary report of my submissions?", $this->plugin_name ); ?></strong></h4>
                    </div>
                    <div class="ays-survey-asked-question__arrow"><i class="fa fa-chevron-down"></i></div>
                </div>
                <div class="ays-survey-asked-question__body">
                    <p>
                        <?php 
                            echo sprintf( 
                                __( "To do that, please go to the %sSubmissions%s page of the plugin. There you will find plenty of analysis tools. Want to %sanalyze data from a survey%s? The plugin is %sjust what you needed%s. Collect user feedback and interpret them via reports, charts, and statistics.", $this->plugin_name ),
                                '<strong>',
                                '</strong>',
                                '<strong>',
                                '</strong>',
                                '<strong>',
                                '</strong>'
                            );                            
                        ?>
                    </p>
                </div>
            </div>
            <div class="ays-survey-asked-question">
                <div class="ays-survey-asked-question__header">
                    <div class="ays-survey-asked-question__title">
                        <h4><strong><?php echo __( "How to transfer all surveys from one site to another?", $this->plugin_name ); ?></strong></h4>
                    </div>
                    <div class="ays-survey-asked-question__arrow"><i class="fa fa-chevron-down"></i></div>
                </div>
                <div class="ays-survey-asked-question__body">
                    <p>
                        <?php 
                            echo sprintf( 
                                __( "Looking for an %samazing and flexible plugin%s? You are in a right place! We have done our best to make the time of our customers more manageable. You can import and export your already created surveys from one website to another via JSON format. %sGreat plugin, right?%s", $this->plugin_name ),
                                '<strong>',
                                '</strong>',
                                '<em>',
                                '</em>'
                            );                            
                        ?>
                    </p>
                </div>
            </div>
            <div class="ays-survey-asked-question">
                <div class="ays-survey-asked-question__header">
                    <div class="ays-survey-asked-question__title">
                        <h4><strong><?php echo __( "Can I get support for the Survey Maker Free version?" , $this->plugin_name ); ?></strong></h4>
                    </div>
                    <div class="ays-survey-asked-question__arrow"><i class="fa fa-chevron-down"></i></div>
                </div>
                <div class="ays-survey-asked-question__body">
                    <p>
                        <?php
                            echo __( "We happily support our community. Our Support Care specialists are always ready to help you to use the product more efficiently." , $this->plugin_name );
                            echo " ";
                            echo sprintf( 
                                __( "Though the plugin is %seasy to set up and use%s and has a %ssimple interface%s, please feel free to reach out to us anytime when you have any concerns.", $this->plugin_name ),
                                '<strong>',
                                '</strong>',
                                '<strong>',
                                '</strong>'
                            );
                            echo " ";
                            echo __( "The team may respond to your questions within 24 hours." , $this->plugin_name );
                            echo " ";
                            echo sprintf( 
                                __( "For questions concerning the Free plugin please post it on the %sWordPress Support Forum%s, otherwise(pre-sale question/pro support), please contact us via %sthis form%s. %sExcellent service is guaranteed!%s", $this->plugin_name ),
                                '<a href="https://wordpress.org/support/plugin/survey-maker/" target="_blank">',
                                '</a>',
                                '<a href="https://ays-pro.com/contact" target="_blank">',
                                '</a>',
                                '<em>',
                                '</em>'
                            );
                        ?>
                    </p>
                </div>
            </div>
            <div class="ays-survey-asked-question">
                <div class="ays-survey-asked-question__header">
                    <div class="ays-survey-asked-question__title">
                        <h4><strong><?php echo __( "How to add unlimited number of questions?" , $this->plugin_name ); ?></strong></h4>
                    </div>
                    <div class="ays-survey-asked-question__arrow"><i class="fa fa-chevron-down"></i></div>
                </div>
                <div class="ays-survey-asked-question__body">
                    <p>
                        <?php
                            echo sprintf( 
                                __( "You must know that our plugin has no limits
                                Survey Maker offers %sunlimited%s number of sections, questions and answers. However, if you came across to such problem, it is connected with your server. To solve that problem you must access to your cPanel, find your PHP values and enhance the number, and in case you are unable to do that yourself then contact your Hosting provider, so that they do the mentioned for you.", $this->plugin_name ),
                                '<strong>',
                                '</strong>'
                            );   
                            echo "<br>";
                            echo "<br>";
                            echo "max_input_vars 10000";
                            echo "<br>";
                            echo "max_execution_time 600";
                            echo "<br>";
                            echo "max_input_time 600";
                            echo "<br>";
                            echo "post_max_size 256M";
                        ?>
                    </p>
                </div>
            </div>
        </div>
        <p class="ays-survey-faq-footer">
            <?php echo __( "For more advanced needs, please take a look at our" , $this->plugin_name ); ?> 
            <a href="https://ays-pro.com/wordpress-survey-maker-user-manual" target="_blank"><?php echo __( "Survey Maker plugin User Manual." , $this->plugin_name ); ?></a>
            <br>
            <?php echo __( "If none of these guides help you, ask your question by contacting our" , $this->plugin_name ); ?>
            <a href="https://ays-pro.com/contact" target="_blank"><?php echo __( "support specialists." , $this->plugin_name ); ?></a> 
            <?php echo __( "and get a reply within a day." , $this->plugin_name ); ?>
        </p>
    </div>
</div>
<script>
    var acc = document.getElementsByClassName("ays-survey-asked-question__header");
    var i;
    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function() {
        
        var panel = this.nextElementSibling;
        
        
        if (panel.style.maxHeight) {
          panel.style.maxHeight = null;
          this.children[1].children[0].style.transform="rotate(0deg)";
        } else {
          panel.style.maxHeight = panel.scrollHeight + "px";
          this.children[1].children[0].style.transform="rotate(180deg)";
        } 
      });
    }
</script>
