<div id="tab3" class="ays-survey-tab-content <?php echo ($ays_tab == 'tab3') ? 'ays-survey-tab-content-active' : ''; ?>">
    <p class="ays-subtitle"><?php echo __('Survey Settings',$this->plugin_name)?></p>
    <hr/>
    <div class="form-group row">
        <div class="col-sm-4">
            <label for="ays-category">
                <?php echo __('Survey categories', $this->plugin_name); ?>
                <a class="ays_help" data-toggle="tooltip" data-html="true" title="<?php
                    echo htmlspecialchars( sprintf(
                        __('Choose the category/categories your survey belongs to. To create a category, go to the %sCategories%s page.',$this->plugin_name),
                        '<strong>',
                        '</strong>'
                    ) );
                ?>">
                    <i class="ays_fa ays_fa_info_circle"></i>
                </a>
            </label>
        </div>
        <div class="col-sm-8">
            <select id="ays-category" name="<?php echo $html_name_prefix; ?>category_ids[]" multiple>
                <option></option>
                <?php
                foreach ($categories as $key => $category) {
                    $selected = in_array( $category['id'], $category_ids ) ? "selected" : "";
                    if( empty( $category_ids ) ){
                        if ( intval( $category['id'] ) == 1 ) {
                            $selected = 'selected';
                        }
                    }
                    echo '<option value="' . $category["id"] . '" ' . $selected . '>' . stripslashes( $category['title'] ) . '</option>';
                }
                ?>
            </select>
        </div>
    </div> <!-- Survey Category -->
    <hr/>
    <div class="form-group row">
        <div class="col-sm-4">
            <label for="ays-status">
                <?php echo __('Survey status', $this->plugin_name); ?>
                <a class="ays_help" data-toggle="tooltip" data-html="true" title="<?php
                    echo htmlspecialchars( sprintf(
                        __("Decide whether your survey is active or not. If you choose %sDraft%s, the survey won't be shown anywhere on your website (you don't need to remove shortcodes).", $this->plugin_name),
                        '<strong>',
                        '</strong>'
                    ) );
                ?>">
                    <i class="ays_fa ays_fa_info_circle"></i>
                </a>
            </label>
        </div>
        <div class="col-sm-8">
            <select id="ays-status" name="<?php echo $html_name_prefix; ?>status">
                <option></option>
                <option <?php selected( $status, 'published' ); ?> value="published"><?php echo __( "Published", $this->plugin_name ); ?></option>
                <option <?php selected( $status, 'draft' ); ?> value="draft"><?php echo __( "Draft", $this->plugin_name ); ?></option>
            </select>
        </div>
    </div> <!-- Survey Status -->
    <hr/>
    <div class="form-group row">
        <div class="col-sm-4">
            <label for="ays_survey_show_title">
                <?php echo __('Show survey title',$this->plugin_name); ?>
                <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Show the name of the survey on the front-end.',$this->plugin_name); ?>">
                    <i class="ays_fa ays_fa_info_circle"></i>
                </a>
            </label>
        </div>
        <div class="col-sm-8">
                <input type="checkbox" id="ays_survey_show_title" name="ays_survey_show_title" value="on" <?php echo $survey_show_title ? 'checked' : ''; ?>/>
        </div>
    </div> <!-- Show survey title -->
    <hr/>
    <div class="form-group row">
        <div class="col-sm-4">
            <label for="ays_survey_show_section_header">
                <?php echo __('Show section header info',$this->plugin_name); ?>
                <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Tick the checkbox if you want to show the title and description of the section on the front-end.',$this->plugin_name); ?>">
                    <i class="ays_fa ays_fa_info_circle"></i>
                </a>
            </label>
        </div>
        <div class="col-sm-8">
                <input type="checkbox" id="ays_survey_show_section_header" name="ays_survey_show_section_header" value="on" <?php echo $survey_show_section_header ? 'checked' : ''; ?>/>
        </div>
    </div> <!-- Show survey title -->
    <hr/>
    <div class="form-group row">
        <div class="col-sm-4">
            <label for="ays_survey_enable_randomize_answers">
                <?php echo __('Enable randomize answers',$this->plugin_name)?>
                <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Show the answers in a random sequence every time the survey takers participate in a survey.',$this->plugin_name); ?>">
                    <i class="ays_fa ays_fa_info_circle"></i>
                </a>
            </label>
        </div>
        <div class="col-sm-8">
            <input type="checkbox" class="ays-enable-timerl" id="ays_survey_enable_randomize_answers" name="ays_survey_enable_randomize_answers" value="on" <?php echo ($survey_enable_randomize_answers) ? 'checked' : ''; ?>/>
        </div>
    </div> <!-- Enable randomize answers -->
    <hr/>
    <div class="form-group row">
        <div class="col-sm-4">
            <label for="ays_survey_enable_randomize_questions">
                <?php echo __('Enable randomize questions',$this->plugin_name)?>
                <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Show the questions in a random sequence every time the survey takers participate in a survey.',$this->plugin_name); ?>">
                    <i class="ays_fa ays_fa_info_circle"></i>
                </a>
            </label>
        </div>
        <div class="col-sm-8">
            <input type="checkbox" class="ays-enable-timerl" id="ays_survey_enable_randomize_questions" name="ays_survey_enable_randomize_questions" value="on" <?php echo ($survey_enable_randomize_questions) ? 'checked' : ''; ?>/>
        </div>
    </div> <!-- Enable randomize questions -->
    <hr/>
    <div class="form-group row">
        <div class="col-sm-4">
            <label for="ays_survey_enable_clear_answer">
                <?php echo __('Enable clear answer button',$this->plugin_name)?>
                <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Allow the survey taker to clear the chosen answer.',$this->plugin_name); ?>">
                    <i class="ays_fa ays_fa_info_circle"></i>
                </a>
            </label>
        </div>
        <div class="col-sm-8">
            <input type="checkbox" class="ays-enable-timer1" id="ays_survey_enable_clear_answer" name="ays_survey_enable_clear_answer" value="on" <?php echo ($survey_enable_clear_answer) ? 'checked' : '' ?>/>
        </div>
    </div> <!-- Enable clear answer button -->
    <hr/>
    <div class="form-group row">
        <div class="col-sm-4">
            <label for="ays_survey_enable_previous_button">
                <?php echo __('Enable previous button', $this->plugin_name); ?>
                <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Add a previous button that will let the users go back to the previous sections.',$this->plugin_name); ?>">
                    <i class="ays_fa ays_fa_info_circle"></i>
                </a>
            </label>
        </div>
        <div class="col-sm-8">
            <input type="checkbox" class="ays-enable-timer1" id="ays_survey_enable_previous_button" name="ays_survey_enable_previous_button" value="on" <?php echo ($survey_enable_previous_button) ? 'checked' : '' ?>/>
        </div>
    </div> <!-- Enable clear answer button -->
    <hr/>
    <div class="form-group row">
        <div class="col-sm-4">
            <label for="ays_survey_allow_html_in_answers">
                <?php echo __('Allow HTML in answers',$this->plugin_name)?>
                <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Allow implementing HTML coding in answer boxes. This works only for Radio and Checkbox (Multiple) questions.', $this->plugin_name); ?>">
                    <i class="ays_fa ays_fa_info_circle"></i>
                </a>
            </label>
        </div>
        <div class="col-sm-8">
            <input type="checkbox" class="ays-enable-timer1" id="ays_survey_allow_html_in_answers" name="ays_survey_allow_html_in_answers" value="on" <?php echo ($survey_allow_html_in_answers) ? 'checked' : '' ?>/>
        </div>
    </div> <!-- Allow HTML in answers -->
    <hr/>
    <div class="form-group row">
        <div class="col-sm-4">
            <label for="ays_survey_allow_html_in_section_description">
                <?php echo __('Allow HTML in section description',$this->plugin_name)?>
                <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Allow implementing HTML coding in section description boxes.', $this->plugin_name); ?>">
                    <i class="ays_fa ays_fa_info_circle"></i>
                </a>
            </label>
        </div>
        <div class="col-sm-8">
            <input type="checkbox" class="ays-enable-timer1" id="ays_survey_allow_html_in_section_description" name="ays_survey_allow_html_in_section_description" value="on" <?php echo ($survey_allow_html_in_section_description) ? 'checked' : '' ?>/>
        </div>
    </div> <!-- Allow HTML in section description -->
    <hr/>
    <div class="form-group row">
        <div class="col-sm-4">
            <label for="ays_survey_enable_leave_page">
                <?php echo __('Enable confirmation box for leaving the page',$this->plugin_name)?>
                <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Show a popup box whenever the survey taker wants to refresh or leave the page while taking the survey.',$this->plugin_name); ?>">
                    <i class="ays_fa ays_fa_info_circle"></i>
                </a>
            </label>
        </div>
        <div class="col-sm-8">
            <input type="checkbox" class="ays-enable-timer1" id="ays_survey_enable_leave_page" name="ays_survey_enable_leave_page" value="on" <?php echo ($survey_enable_leave_page) ? 'checked' : ''; ?>/>
        </div>
    </div> <!-- Enable confirmation box for leaving the page -->
    <hr/>
    <div class="form-group row">
        <div class="col-sm-4">
            <label for="ays_survey_enable_info_autofill">
                <?php echo __('Autofill logged-in user data',$this->plugin_name); ?>
                <a class="ays_help" data-toggle="tooltip" title="<?php echo __("After enabling this option, logged in user's name and email will be autofilled in Name and Email fields.",$this->plugin_name); ?>">
                    <i class="ays_fa ays_fa_info_circle"></i>
                </a>
            </label>
        </div>
        <div class="col-sm-8">
            <input type="checkbox" class="ays-enable-timer1" id="ays_survey_enable_info_autofill" name="ays_survey_enable_info_autofill" <?php echo $survey_info_autofill; ?>/>
        </div>
    </div><!-- Autofill logged-in user data -->
    <hr/>
    <div class="form-group row">
        <div class="col-sm-4">
            <label>
                <?php echo __('Answers numbering',$this->plugin_name); ?>
                <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Assign numbering to each answer in ascending sequential order. Choose your preferred type from the list.',$this->plugin_name); ?>">
                    <i class="ays_fa ays_fa_info_circle"></i>
                </a>
            </label>
        </div>
        <div class="col-sm-8">
            <select class="ays-text-input ays-text-input-short" name="ays_survey_show_answers_numbering">
                <option <?php echo $survey_auto_numbering == "none" ? "selected" : ""; ?> value="none"><?php echo __( "None", $this->plugin_name); ?></option>
                <option <?php echo $survey_auto_numbering == "1."   ? "selected" : ""; ?>   value="1."><?php echo __( "1.", $this->plugin_name); ?></option>
                <option <?php echo $survey_auto_numbering == "1)"   ? "selected" : ""; ?>   value="1)"><?php echo __( "1)", $this->plugin_name); ?></option>
                <option <?php echo $survey_auto_numbering == "A."   ? "selected" : ""; ?>   value="A."><?php echo __( "A.", $this->plugin_name); ?></option>
                <option <?php echo $survey_auto_numbering == "A)"   ? "selected" : ""; ?>   value="A)"><?php echo __( "A)", $this->plugin_name); ?></option>
                <option <?php echo $survey_auto_numbering == "a."   ? "selected" : ""; ?>   value="a."><?php echo __( "a.", $this->plugin_name); ?></option>
                <option <?php echo $survey_auto_numbering == "a)"   ? "selected" : ""; ?>   value="a)"><?php echo __( "a)", $this->plugin_name); ?></option>
            </select>

        </div>
    </div> <!-- Show answers numbering -->
    <hr>
    <div class="form-group row">
        <div class="col-sm-4">
            <label for="ays_survey_enable_full_screen_mode">
                <?php echo __('Enable full-screen mode',$this->plugin_name)?>
                <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Allow the survey to enter full-screen mode by pressing the icon located in the top-right corner of the survey container.',$this->plugin_name)?>">
                    <i class="ays_fa ays_fa_info_circle"></i>
                </a>
            </label>
        </div>
        <div class="col-sm-8">
            <input type="checkbox"
                   class="ays-enable-timer1"
                   id="ays_survey_enable_full_screen_mode"
                   name="ays_survey_enable_full_screen_mode"
                   value="on"
                   <?php echo $survey_full_screen;?>/>
        </div>
    </div> <!-- Open Full Screen Mode -->
    <hr>
    <div class="form-group row">
        <div class="col-sm-4">
            <label for="ays_survey_enable_progres_bar">
                <?php echo __('Enable live progress bar',$this->plugin_name)?>
                <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Show the current state of the user passing the survey. It will be shown at the bottom of the survey container.',$this->plugin_name)?>">
                    <i class="ays_fa ays_fa_info_circle"></i>
                </a>
            </label>
        </div>
        <div class="col-sm-8">
            <input type="checkbox"
                   class="ays-enable-timer1"
                   id="ays_survey_enable_progres_bar"
                   name="ays_survey_enable_progres_bar"
                   value="on"
                   <?php echo $survey_enable_progress_bar;?>/>
        </div>
    </div> <!-- Live progres bar -->
    <hr/>
    <div class="form-group row">
        <div class="col-sm-4">
            <label>
                <?php echo __('Questions numbering',$this->plugin_name); ?>
                <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Assign numbering to each question in ascending sequential order. Choose your preferred type from the list.',$this->plugin_name); ?>">
                    <i class="ays_fa ays_fa_info_circle"></i>
                </a>
            </label>
        </div>
        <div class="col-sm-8">
            <select class="ays-text-input ays-text-input-short" name="ays_survey_show_question_numbering">
                <option <?php echo $survey_auto_numbering_questions == "none" ? "selected" : ""; ?> value="none"><?php echo __( "None", $this->plugin_name); ?></option>
                <option <?php echo $survey_auto_numbering_questions == "1."   ? "selected" : ""; ?>   value="1."><?php echo __( "1.", $this->plugin_name); ?></option>
                <option <?php echo $survey_auto_numbering_questions == "1)"   ? "selected" : ""; ?>   value="1)"><?php echo __( "1)", $this->plugin_name); ?></option>
                <option <?php echo $survey_auto_numbering_questions == "A."   ? "selected" : ""; ?>   value="A."><?php echo __( "A.", $this->plugin_name); ?></option>
                <option <?php echo $survey_auto_numbering_questions == "A)"   ? "selected" : ""; ?>   value="A)"><?php echo __( "A)", $this->plugin_name); ?></option>
                <option <?php echo $survey_auto_numbering_questions == "a."   ? "selected" : ""; ?>   value="a."><?php echo __( "a.", $this->plugin_name); ?></option>
                <option <?php echo $survey_auto_numbering_questions == "a)"   ? "selected" : ""; ?>   value="a)"><?php echo __( "a)", $this->plugin_name); ?></option>
            </select>

        </div>
    </div> <!-- Show question numbering -->
    <hr/>
    <div class="form-group row">
        <div class="col-sm-4">
            <label>
                <?php echo __('Change current survey creation date',$this->plugin_name); ?>
                <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Change the survey creation date to your preferred date.',$this->plugin_name); ?>">
                    <i class="ays_fa ays_fa_info_circle"></i>
                </a>
            </label>
        </div>
        <div class="col-sm-8">
            <div class="input-group mb-3">
                <input type="text" class="ays-text-input ays-text-input-short ays-survey-date-create" id="ays_survey_change_creation_date" name="ays_survey_change_creation_date" value="<?php echo $date_created; ?>" placeholder="<?php echo current_time( 'mysql' ); ?>">
                <div class="input-group-append">
                    <label for="ays_survey_change_creation_date" class="input-group-text">
                        <span><i class="ays_fa ays_fa_calendar"></i></span>
                    </label>
                </div>
            </div>
        </div>
    </div> <!-- Change current survey creation date -->
    <hr/>
    <div class="form-group row">
        <div class="col-sm-4">
            <label for="ays_survey_show_questions_count">
                <?php echo __('Show questions count',$this->plugin_name); ?>
                <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Tick this option to show every sections questions count',$this->plugin_name); ?>">
                    <i class="ays_fa ays_fa_info_circle"></i>
                </a>
            </label>
        </div>
        <div class="col-sm-8">
            <input type="checkbox"
                class="ays-enable-timer1"
                id="ays_survey_show_questions_count"
                name="ays_survey_show_questions_count"
                value="on"
                <?php echo $survey_show_sections_questions_count;?>/>
        </div>
    </div> <!-- Show sections questions count -->
    <hr/>
    <div class="form-group row" style="margin:0px;">
        <div class="col-sm-12 only_pro">
            <div class="pro_features">
                <div>
                    <p>
                        <?php echo __("This feature is available only in ", $this->plugin_name); ?>
                        <a href="https://ays-pro.com/wordpress/survey-maker/" target="_blank" title="PRO feature"><?php echo __("PRO version!!!", $this->plugin_name); ?></a>
                    </p>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-4">
                    <label for="ays_survey_enable_copy_protection">
                        <?php echo __('Enable copy protection',$this->plugin_name)?>
                        <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Disable copy functionality in survey page (CTRL+C) and Right-click',$this->plugin_name)?>">
                            <i class="ays_fa ays_fa_info_circle"></i>
                        </a>
                    </label>
                </div>
                <div class="col-sm-8">
                    <input type="checkbox" class="ays-enable-timer1" id="ays_survey_enable_copy_protection"/>
                </div>
            </div>
        </div>
    </div> <!-- Enable copy protection -->
    <hr>
    <div class="form-group row" style="margin:0px;">
        <div class="col-sm-12 only_pro">
            <div class="pro_features">
                <div>
                    <p>
                        <?php echo __("This feature is available only in ", $this->plugin_name); ?>
                        <a href="https://ays-pro.com/wordpress/survey-maker/" target="_blank" title="PRO feature"><?php echo __("PRO version!!!", $this->plugin_name); ?></a>
                    </p>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-4">
                    <label for="ays_survey_enable_start_page">
                        <?php echo __('Enable start page',$this->plugin_name); ?>
                        <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Tick the checkbox if you want to add a start page to your survey. After enabling this option, a new tab will appear next to the Settings tab, where you can configure Start Page settings.',$this->plugin_name); ?>">
                            <i class="ays_fa ays_fa_info_circle"></i>
                        </a>
                    </label>
                </div>
                <div class="col-sm-8">
                    <input type="checkbox" value="on" />
                </div>
            </div> 
        </div>
    </div> <!-- Enable start page -->
    <hr/>
    <div class="form-group row" style="margin:0px;">
        <div class="col-sm-12 only_pro">
            <div class="pro_features">
                <div>
                    <p>
                        <?php echo __("This feature is available only in ", $this->plugin_name); ?>
                        <a href="https://ays-pro.com/wordpress/survey-maker/" target="_blank" title="PRO feature"><?php echo __("PRO version!!!", $this->plugin_name); ?></a>
                    </p>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-4">
                    <label for="ays_survey_allow_collecting_logged_in_users_data">
                        <?php echo __('Allow collecting information of logged in users',$this->plugin_name); ?>
                        <a class="ays_help" data-toggle="tooltip" title="<?php echo __("Allow collecting information from logged-in users. Email and name of users will be stored in the database. Email options will be work for these users.",$this->plugin_name); ?>">
                            <i class="ays_fa ays_fa_info_circle"></i>
                        </a>
                    </label>
                </div>
                <div class="col-sm-8">
                    <input type="checkbox" value="on" />
                </div>
            </div> 
        </div>
    </div> <!-- Allow collecting information of logged in users -->
    <hr/>
    <div class="form-group row" style="margin:0px;">
        <div class="col-sm-12 only_pro">
            <div class="pro_features">
                <div>
                    <p>
                        <?php echo __("This feature is available only in ", $this->plugin_name); ?>
                        <a href="https://ays-pro.com/wordpress/survey-maker/" target="_blank" title="PRO feature"><?php echo __("PRO version!!!", $this->plugin_name); ?></a>
                    </p>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-4">
                    <label for="ays_survey_enable_expand_collapse_question">
                        <?php echo __('Expand/collapse questions',$this->plugin_name)?>
                        <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Expand/collapse questions on the front page.',$this->plugin_name)?>">
                            <i class="ays_fa ays_fa_info_circle"></i>
                        </a>
                    </label>
                </div>
                <div class="col-sm-8">
                    <input type="checkbox" class="ays-enable-timer1" value="on" />
                </div>
            </div> 
        </div>
    </div><!-- Expand/collapse questions -->
    <hr/>
    <div class="form-group row" style="margin:0px;">
        <div class="col-sm-12 only_pro">
            <div class="pro_features">
                <div>
                    <p>
                        <?php echo __("This feature is available only in ", $this->plugin_name); ?>
                        <a href="https://ays-pro.com/wordpress/survey-maker/" target="_blank" title="PRO feature"><?php echo __("PRO version!!!", $this->plugin_name); ?></a>
                    </p>
                </div>
            </div>
            <div class="form-group row ays_toggle_parent">
                <div class="col-sm-4">
                    <label for="ays_survey_enable_chat_mode">
                        <?php echo __('Enable chat mode',$this->plugin_name)?>
                        <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Make your survey look like an online chat conversation. It will print each pre-set question instantly after the user answers the previous one.',$this->plugin_name)?>">
                            <i class="ays_fa ays_fa_info_circle"></i>
                        </a>
                    </label>
                </div>
                <div class="col-sm-1">
                    <input type="checkbox" class="ays_toggle_checkbox" value="on" />
                </div>
            </div> 
        </div>
    </div><!-- Enable chat mode -->
    <hr/>
    <div class="form-group row" style="margin:0px;">
        <div class="col-sm-12 only_pro">
            <div class="pro_features">
                <div>
                    <p>
                        <?php echo __("This feature is available only in ", $this->plugin_name); ?>
                        <a href="https://ays-pro.com/wordpress/survey-maker/" target="_blank" title="PRO feature"><?php echo __("PRO version!!!", $this->plugin_name); ?></a>
                    </p>
                    <p style="position: absolute;top: 0;">
                        <?php echo __("This feature is available only in ", $this->plugin_name); ?>
                        <a href="https://ays-pro.com/wordpress/survey-maker/" target="_blank" title="PRO feature"><?php echo __("PRO version!!!", $this->plugin_name); ?></a>
                    </p>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-4">
                    <label for="ays_survey_enable_schedule">
                        <?php echo __('Schedule the Survey', $this->plugin_name); ?>
                        <a class="ays_help" data-toggle="tooltip"
                           title="<?php echo __('.', $this->plugin_name); ?>">
                            <i class="ays_fa ays_fa_info_circle"></i>
                        </a>
                    </label>
                    <p class="ays_survey_small_hint_text_for_message_variables">
                        <span><?php echo __( "To change your GMT " , $this->plugin_name ); ?></span>
                        <a href="<?php echo $wp_general_settings_url; ?>" target="_blank"><?php echo __( "click here" , $this->plugin_name ); ?></a>
                    </p>
                </div>
                <div class="col-sm-1">
                    <input id="ays_survey_enable_schedule" type="checkbox" class="active_date_check">
                </div>
                <div class="col-sm-7 ays_toggle_target ays_divider_left active_date">
                    <div class="form-group row">
                        <div class="col-sm-4">
                            <label class="form-check-label" for="ays_survey_schedule_active"> <?php echo __('Start date:', $this->plugin_name); ?> </label>
                        </div>
                        <div class="col-sm-8">
                            <div class="input-group mb-3">
                                <input type="text" class="ays-text-input ays-text-input-short" id="ays_survey_schedule_active" placeholder="<?php echo current_time( 'mysql' ); ?>">
                                <div class="input-group-append">
                                    <label for="ays_survey_schedule_active" class="input-group-text">
                                        <span><i class="ays_fa ays_fa_calendar"></i></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-4">
                            <label class="form-check-label" for="ays_survey_schedule_deactive"> <?php echo __('End date:', $this->plugin_name); ?> </label>
                        </div>
                        <div class="col-sm-8">
                            <div class="input-group mb-3">
                                <input type="text" class="ays-text-input ays-text-input-short" id="ays_survey_schedule_deactive" placeholder="<?php echo current_time( 'mysql' ); ?>">
                                <div class="input-group-append">
                                    <label for="ays_survey_schedule_deactive" class="input-group-text">
                                        <span><i class="ays_fa ays_fa_calendar"></i></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-4">
                            <label class="form-check-label" for="ays_survey_schedule_pre_start_message"><?php echo __("Pre-start message:", $this->plugin_name) ?></label>
                        </div>
                        <div class="col-sm-8">
                            <div class="editor">
                                <?php
                                $content   = __( 'The survey will be available soon!' , $this->plugin_name );
                                $editor_id = 'ays_survey_schedule_pre_start_message';
                                $settings  = array(
                                    'editor_height'  => '100',
                                    'editor_class'   => 'ays-textarea',
                                    'media_elements' => false
                                );
                                wp_editor($content, $editor_id, $settings);
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-4">
                            <label class="form-check-label" for="ays_survey_schedule_expiration_message"><?php echo __("Expiration message:", $this->plugin_name) ?></label>
                        </div>
                        <div class="col-sm-8">
                            <div class="editor">
                                <?php
                                $content   = __( 'This survey has expired!' , $this->plugin_name );
                                $editor_id = 'ays_survey_schedule_expiration_message';
                                $settings  = array(
                                    'editor_height'  => '100',
                                    'editor_class'   => 'ays-textarea',
                                    'media_elements' => false
                                );
                                wp_editor($content, $editor_id, $settings);
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div> <!-- Schedule the Survey ( PRO ) -->
</div>
