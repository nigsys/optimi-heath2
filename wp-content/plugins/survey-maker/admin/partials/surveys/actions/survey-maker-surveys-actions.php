<?php
    require_once( SURVEY_MAKER_ADMIN_PATH . "/partials/surveys/actions/survey-maker-surveys-actions-options.php" );
?>

<div class="wrap">
    <div class="container-fluid">
        <form method="post" id="ays-survey-form">
            <input type="hidden" name="ays_survey_tab" value="<?php echo $ays_tab; ?>">
            <h1 class="wp-heading-inline">
                <?php
                    echo $heading;
                    $other_attributes = array('id' => 'ays-button-save-top');
                    submit_button(__('Save and close', $this->plugin_name), 'primary ays-button ays-survey-loader-banner', 'ays_submit_top', false, $other_attributes);
                    // $other_attributes = array('id' => 'ays-button-save-new-top');
                    // submit_button(__('Save and new', $this->plugin_name), 'primary ays-button', 'ays_save_new_top', false, $other_attributes);
                    $other_attributes = array('id' => 'ays-button-apply-top');
                    submit_button(__('Save', $this->plugin_name), 'ays-button ays-survey-loader-banner', 'ays_apply_top', false, $other_attributes);
                    echo $loader_iamge;
                ?>
            </h1>
            <div>
                <?php if($id !== null): ?>
                <div class="row">
                    <div class="col-sm-3">
                        <label> <?php echo __( "Shortcode text for editor", $this->plugin_name ); ?> </label>
                    </div>
                    <div class="col-sm-9">
                        <p style="font-size:14px; font-style:italic;">
                            <?php echo __("To insert the Survey into a page, post or text widget, copy shortcode", $this->plugin_name); ?>
                            <strong class="ays-survey-shortcode-box" onClick="selectElementContents(this)" style="font-size:16px; font-style:normal;" class="ays_help" data-toggle="tooltip" title="<?php echo __('Click for copy',$this->plugin_name);?>" ><?php echo "[ays_survey id='".$id."']"; ?></strong>
                            <?php echo " " . __( "and paste it at the desired place in the editor.", $this->plugin_name); ?>
                        </p>
                    </div>
                </div>
                <?php endif;?>
            </div>
            <hr/>
            <div class="form-group row">
                <div class="col-sm-2">
                    <label for='ays-survey-title'>
                        <?php echo __('Title', $this->plugin_name); ?>
                        <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Give a title to your survey.',$this->plugin_name); ?>">
                            <i class="ays_fa ays_fa_info_circle"></i>
                        </a>
                    </label>
                </div>
                <div class="col-sm-10">
                    <input type="text" class="ays-text-input" id='ays-survey-title' name='<?php echo $html_name_prefix; ?>title' value="<?php echo $title; ?>"/>
                </div>
            </div> <!-- Survey Title -->
            <hr/>
            <div class="ays-top-menu-wrapper">
                <div class="ays_menu_left" data-scroll="0"><i class="ays_fa ays_fa_angle_left"></i></div>
                <div class="ays-top-menu">
                    <div class="nav-tab-wrapper ays-top-tab-wrapper">
                        <a href="#tab1" data-tab="tab1" class="nav-tab <?php echo ($ays_tab == 'tab1') ? 'nav-tab-active' : ''; ?>">
                            <?php echo __("General", $this->plugin_name);?>
                        </a>
                        <a href="#tab2" data-tab="tab2" class="nav-tab <?php echo ($ays_tab == 'tab2') ? 'nav-tab-active' : ''; ?>">
                            <?php echo __("Styles", $this->plugin_name);?>
                        </a>
                        <a href="#tab6" data-tab="tab6" class="nav-tab <?php echo ($ays_tab == 'tab6') ? 'nav-tab-active' : ''; ?>">
                            <?php echo __("Start page", $this->plugin_name);?>
                        </a>
                        <a href="#tab3" data-tab="tab3" class="nav-tab <?php echo ($ays_tab == 'tab3') ? 'nav-tab-active' : ''; ?>">
                            <?php echo __("Settings", $this->plugin_name);?>
                        </a>
                        <a href="#tab4" data-tab="tab4" class="nav-tab <?php echo ($ays_tab == 'tab4') ? 'nav-tab-active' : ''; ?>">
                            <?php echo __("Results Settings", $this->plugin_name);?>
                        </a>
                        <a href="#tab9" data-tab="tab9" class="nav-tab <?php echo ($ays_tab == 'tab9') ? 'nav-tab-active' : ''; ?>">
                            <?php echo __("Conditional Result", $this->plugin_name);?>
                        </a>
                        <a href="#tab5" data-tab="tab5" class="nav-tab <?php echo ($ays_tab == 'tab5') ? 'nav-tab-active' : ''; ?>">
                            <?php echo __("Limitation Users", $this->plugin_name);?>
                        </a>
                        <a href="#tab7" data-tab="tab7" class="nav-tab <?php echo ($ays_tab == 'tab7') ? 'nav-tab-active' : ''; ?>">
                            <?php echo __("E-Mail", $this->plugin_name);?>
                        </a>
                        <a href="#tab8" data-tab="tab8" class="nav-tab <?php echo ($ays_tab == 'tab8') ? 'nav-tab-active' : ''; ?>">
                            <?php echo __("Integrations", $this->plugin_name);?>
                        </a>
                    </div>  
                </div>
                <div class="ays_menu_right" data-scroll="-1"><i class="ays_fa ays_fa_angle_right"></i></div>
            </div>
            
            <?php
                for($tab_ind = 1; $tab_ind <= 9; $tab_ind++){
                    require_once( SURVEY_MAKER_ADMIN_PATH . "/partials/surveys/actions/partials/survey-maker-surveys-actions-tab".$tab_ind.".php" );
                }
            ?>

            <input type="hidden" name="<?php echo $html_name_prefix; ?>author_id" value="<?php echo $author_id; ?>">
            <input type="hidden" name="<?php echo $html_name_prefix; ?>post_id" value="<?php echo $post_id; ?>">
            <input type="hidden" name="<?php echo $html_name_prefix; ?>date_created" value="<?php echo $date_created; ?>">
            <input type="hidden" name="<?php echo $html_name_prefix; ?>date_modified" value="<?php echo $date_modified; ?>">
            <input type="hidden" name="<?php echo $html_name_prefix; ?>default_question_type" value="<?php echo $survey_default_type; ?>">
            <input type="hidden" name="<?php echo $html_name_prefix; ?>default_answers_count" value="<?php echo $survey_answer_default_count; ?>">
            <hr>
            <!-- <div class="form-group row ays-survey-general-bundle-container">
                <div class="col-sm-12 ays-survey-general-bundle-box">
                    <div class="ays-survey-general-bundle-row">
                        <div class="ays-survey-general-bundle-text">
                            <?php //echo __( "You have", $this->plugin_name ); ?>
                            <span><?php //echo __( "20% off Christmas discount", $this->plugin_name ); ?></span>
                            <?php //echo __( "on Survey Maker plugin! ", $this->plugin_name ); ?>
                        </div>
                        <p><?php // echo __( "Increase your website traffic and warm up for winter.", $this->plugin_name ); ?></p>
                        <div class="ays-survey-general-bundle-sale-text ays-survey-general-bundle-color">
                            <div><a href="https://ays-pro.com/wordpress/survey-maker" class="ays-survey-general-bundle-link-color" target="_blank"><?php //echo __( "Discount 20% OFF", $this->plugin_name ); ?></a></div>
                        </div>
                    </div>
                    <div class="ays-survey-general-bundle-row">
                        <a href="https://ays-pro.com/wordpress/survey-maker" class="ays-survey-general-bundle-button" target="_blank">Get Now!</a>
                    </div>
                </div>
            </div> -->
            <?php
                // wp_nonce_field('survey_action', 'survey_action');
                // $other_attributes = array();
                // $buttons_html = '';
                // $buttons_html .= '<div class="ays_save_buttons_content">';
                //     $buttons_html .= '<div class="ays_save_buttons_box">';
                //     echo $buttons_html;
                //         $other_attributes = array('id' => 'ays-button-save');
                //         submit_button(__('Save and close', $this->plugin_name), 'primary ays-button ays-survey-loader-banner', 'ays_submit', false, $other_attributes);
                //         // $other_attributes = array('id' => 'ays-button-save-new');
                //         // submit_button(__('Save and new', $this->plugin_name), 'primary ays-button', 'ays_save_new', false, $other_attributes);
                //         $other_attributes = array('id' => 'ays-button-apply');
                //         submit_button(__('Save', $this->plugin_name), 'ays-button ays-survey-loader-banner', 'ays_apply', false, $other_attributes);
                //         echo $loader_iamge;
                //     $buttons_html = '</div>';
                //     // $buttons_html .= '<div class="ays_save_default_button_box">';
                //     echo $buttons_html;
                //         // $buttons_html = '<a class="ays_help" data-toggle="tooltip" title=".">
                //         //     <i class="ays_fa ays_fa_info_circle"></i>
                //         // </a>';
                //         // echo $buttons_html;
                //         // $other_attributes = array( 'data-message' => __( 'Are you sure that you want to save these parameters as default?', $this->plugin_name ) );
                //         // submit_button(__('Save as default', $this->plugin_name), 'primary ays_default_btn', 'ays_default', false, $other_attributes);
                //     // $buttons_html = '</div>';
                // $buttons_html = "</div>";
                // echo $buttons_html;
            ?>
            <div class="form-group row ays-surveys-button-box">
                <div class="ays-question-button-first-row" style="padding: 0;">
                <?php
                    wp_nonce_field('survey_action', 'survey_action');
                    $other_attributes = array();
                    $buttons_html = '';
                    $buttons_html .= '<div class="ays_save_buttons_content">';
                        $buttons_html .= '<div class="ays_save_buttons_box">';
                        echo $buttons_html;
                            $other_attributes = array('id' => 'ays-button-save');
                            submit_button(__('Save and close', $this->plugin_name), 'primary ays-button ays-survey-loader-banner', 'ays_submit', false, $other_attributes);
                            // $other_attributes = array('id' => 'ays-button-save-new');
                            // submit_button(__('Save and new', $this->plugin_name), 'primary ays-button', 'ays_save_new', false, $other_attributes);
                            $other_attributes = array('id' => 'ays-button-apply');
                            submit_button(__('Save', $this->plugin_name), 'ays-button ays-survey-loader-banner', 'ays_apply', false, $other_attributes);
                            echo $loader_iamge;
                        $buttons_html = '</div>';
                        // $buttons_html .= '<div class="ays_save_default_button_box">';
                        echo $buttons_html;
                            // $buttons_html = '<a class="ays_help" data-toggle="tooltip" title=".">
                            //     <i class="ays_fa ays_fa_info_circle"></i>
                            // </a>';
                            // echo $buttons_html;
                            // $other_attributes = array( 'data-message' => __( 'Are you sure that you want to save these parameters as default?', $this->plugin_name ) );
                            // submit_button(__('Save as default', $this->plugin_name), 'primary ays_default_btn', 'ays_default', false, $other_attributes);
                        // $buttons_html = '</div>';
                    $buttons_html = "</div>";
                    echo $buttons_html; 
                ?>
                </div>
                <div class="ays-surveys-button-second-row">
                <?php
                    if ( $next_survey_id != "" && !is_null( $next_survey_id ) ) {

                        $other_attributes = array(
                            'id' => 'ays-surveys-next-button',
                            'href' => sprintf( '?page=%s&action=%s&id=%d', esc_attr( $_REQUEST['page'] ), 'edit', absint( $next_survey_id ) )
                        );
                        submit_button(__('Next Suvrey', $this->plugin_name), 'button button-primary ays-button ays-survey-loader-banner', 'ays_survey_next_button', false, $other_attributes);
                    }
                ?>
                </div>
            </div>
        </form>

        <div class="ays-modal" id="ays-edit-question-content">
            <div class="ays-modal-content">
                <div class="ays-survey-preloader">
                    <img class="loader" src="<?php echo SURVEY_MAKER_ADMIN_URL; ?>/images/loaders/tail-spin-result.svg" alt="" width="100">
                </div>

                <!-- Modal Header -->
                <div class="ays-modal-header">
                    <span class="ays-close">&times;</span>
                    <h2>
                        <div class="ays-survey-icons" style="width:36px;height:36px;line-height: 0;vertical-align: bottom;">
                            <img src="<?php echo SURVEY_MAKER_ADMIN_URL; ?>/images/icons/edit-content.svg" style="vertical-align: initial;line-height: 0;margin: 0px;padding: 0;width: 36px;height: 36px;">
                        </div>
                        <span><?php echo __( 'Edit question', $this->plugin_name ); ?></span>
                    </h2>
                </div>

                <!-- Modal body -->
                <div class="ays-modal-body">
                    <form method="post" id="ays_export_filter">
                        <div style="padding: 15px 0;">
                        <?php
                            $content = '';
                            $editor_id = 'ays_survey_question_editor';
                            $settings = array('editor_height' => $survey_wp_editor_height, 'textarea_name' => 'ays_survey_question_editor', 'editor_class' => 'ays-textarea');
                            wp_editor($content, $editor_id, $settings);
                        ?>
                        </div>
                    </form>
                </div>

                <!-- Modal footer -->
                <div class="ays-modal-footer">
                    <button type="button" class="button button-primary ays-survey-back-to-textarea" data-question-id="" data-question-name="" style="margin-right: 10px;"><?php echo __( 'Back to classic texarea', $this->plugin_name ); ?></button>
                    <button type="button" class="button button-primary ays-survey-apply-question-changes" data-question-id="" data-question-name=""><?php echo __( 'Apply changes', $this->plugin_name ); ?></button>
                </div>
            </div>
        </div>
    </div>
</div>
