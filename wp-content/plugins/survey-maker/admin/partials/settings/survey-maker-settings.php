<?php
    $actions = $this->settings_obj;

    if( isset( $_REQUEST['ays_submit'] ) ){
        $actions->store_data();
    }
    if(isset($_GET['ays_survey_tab'])){
        $ays_survey_tab = sanitize_text_field( $_GET['ays_survey_tab'] );
    }else{
        $ays_survey_tab = 'tab1';
    }

    $loader_iamge = '<span class="display_none ays_survey_loader_box"><img src="'. SURVEY_MAKER_ADMIN_URL .'/images/loaders/loading.gif"></span>';
    
    $db_data = $actions->get_db_data();
    $options = ($actions->ays_get_setting('options') === false) ? array() : json_decode($actions->ays_get_setting('options'), true);

    global $wp_roles;
    $ays_users_roles = $wp_roles->role_names;
    $user_roles = $actions->ays_get_setting('user_roles');
    if( $user_roles === null || $user_roles === false ){
        $user_roles = array();
    }else{
        $user_roles = json_decode( $user_roles );
    }

    $survey_types = array(
        "radio"      => __("Radio", $this->plugin_name),
        "checkbox"   => __("Checkbox (Multi)", $this->plugin_name),
        "select"     => __("Dropdown", $this->plugin_name),
        "text"       => __("Paragraph", $this->plugin_name),
        "short_text" => __("Short Text", $this->plugin_name),
        "number"     => __("Number", $this->plugin_name),
        "yesorno"    => __("Yes or No", $this->plugin_name),
        "email"      => __("Email", $this->plugin_name),
        "name"       => __("Name", $this->plugin_name),
    );

    // Survey question default type
    $options['survey_default_type'] = !isset($options['survey_default_type']) ? 'radio' : $options['survey_default_type'];
    $survey_default_type = (isset($options['survey_default_type']) && $options['survey_default_type'] != '') ? esc_attr($options['survey_default_type']) : 'radio';
    $survey_answer_default_count = (isset($options['survey_answer_default_count']) && $options['survey_answer_default_count'] != '')? $options['survey_answer_default_count'] : 1;

    // Do not store IP addresses
    $options['survey_disable_user_ip'] = (isset($options['survey_disable_user_ip']) && $options['survey_disable_user_ip'] == 'on') ? $options['survey_disable_user_ip'] : 'off' ;
    $survey_disable_user_ip = (isset($options['survey_disable_user_ip']) && $options['survey_disable_user_ip'] == 'on') ? true : false;

    // Do not store user names
    $options['survey_disable_user_name'] = (isset($options['survey_disable_user_name']) && $options['survey_disable_user_name'] != '') ? $options['survey_disable_user_name'] : 'off' ;
    $survey_disable_user_name = (isset($options['survey_disable_user_name']) && $options['survey_disable_user_name'] == 'on') ? true : false;

    // Do not store user emails
    $options['survey_disable_user_email'] = (isset($options['survey_disable_user_email']) && $options['survey_disable_user_email'] != '') ? $options['survey_disable_user_email'] : 'off' ;
    $survey_disable_user_email = (isset($options['survey_disable_user_email']) && $options['survey_disable_user_email'] == 'on') ? true : false;

    $survey_submmission_title_length = (isset($options['survey_submissions_title_length']) && $options['survey_submissions_title_length'] != '') ? intval($options['survey_submissions_title_length']) : 5;
    $survey_title_length = (isset($options['survey_title_length']) && $options['survey_title_length'] != '') ? intval($options['survey_title_length']) : 5;

    // Animation Top
    $survey_animation_top = (isset($options['survey_animation_top']) && $options['survey_animation_top'] != '') ? absint(intval($options['survey_animation_top'])) : 200;
    $options['survey_enable_animation_top'] = isset($options['survey_enable_animation_top']) ? $options['survey_enable_animation_top'] : 'on';
    $survey_enable_animation_top = (isset($options['survey_enable_animation_top']) && $options['survey_enable_animation_top'] == "on") ? true : false;

    // Textarea height (public)
    $survey_textarea_height = (isset($options['survey_textarea_height']) && $options['survey_textarea_height'] != '' && $options['survey_textarea_height'] != 0) ? absint( sanitize_text_field($options['survey_textarea_height']) ) : 100;

    // WP Editor height
    $survey_wp_editor_height = (isset($options['survey_wp_editor_height']) && $options['survey_wp_editor_height'] != '' && $options['survey_wp_editor_height'] != 0) ? absint( esc_attr($options['survey_wp_editor_height']) ) : 100;

    //Summary Email
    $ays_survey_admin_email_sessions = array(
        'hourly'     => __('Hourly', $this->plugin_name),
        'daily'      => __('Daily', $this->plugin_name),
        'twicedaily' => __('Twicedaily', $this->plugin_name),
        'weekly'     => __('Weekly', $this->plugin_name),
    );

    $buttons_texts_res      = ($actions->ays_get_setting('buttons_texts') === false) ? json_encode(array()) : $actions->ays_get_setting('buttons_texts');
    $buttons_texts          = json_decode($buttons_texts_res, true);

    $survey_next_button     = (isset($buttons_texts['next_button']) && $buttons_texts['next_button'] != '') ? esc_attr($buttons_texts['next_button']) : 'Next';
    $survey_previous_button = (isset($buttons_texts['prev_button']) && $buttons_texts['prev_button'] != '') ? esc_attr($buttons_texts['prev_button']) : 'Prev';
    $survey_restart_button  = (isset($buttons_texts['restart_button']) && $buttons_texts['restart_button'] != '') ? esc_attr($buttons_texts['restart_button']) : 'Restart';
    $survey_clear_button    = (isset($buttons_texts['clear_button']) && $buttons_texts['clear_button'] != '') ? esc_attr($buttons_texts['clear_button']) : 'Clear selection';
    $survey_finish_button   = (isset($buttons_texts['finish_button']) && $buttons_texts['finish_button'] != '') ? esc_attr($buttons_texts['finish_button']) : 'Finish';
    $survey_exit_button     = (isset($buttons_texts['exit_button']) && $buttons_texts['exit_button'] != '') ? esc_attr($buttons_texts['exit_button']) : 'Exit';
    $survey_login_button    = (isset($buttons_texts['login_button']) && $buttons_texts['login_button'] != '') ? esc_attr($buttons_texts['login_button']) : 'Log In';

    $default_all_submissions_columns = array(
        'User Name',
        'Survey Name',
        'Submission Date',
    );

    $ays_survey_user_history_columns_order = array(
        'Survey Name',
        'Submission Date',
    );
?>
<div class="wrap" style="position:relative;">
    <div class="container-fluid">
        <form method="post" id="ays-survey-settings-form">
            <input type="hidden" name="ays_survey_tab" value="<?php echo $ays_survey_tab; ?>">
            <h1 class="wp-heading-inline">
            <?php
                echo __('General Settings',$this->plugin_name);
            ?>
            </h1>
            <?php
                if( isset( $_REQUEST['status'] ) ){
                    $actions->survey_settings_notices( sanitize_text_field( $_REQUEST['status'] ) );
                }
            ?>
            <hr/>
            <div class="ays-settings-wrapper">
                <div>
                    <div class="nav-tab-wrapper" style="position:sticky; top:35px;">
                        <a href="#tab1" data-tab="tab1" class="nav-tab <?php echo ($ays_survey_tab == 'tab1') ? 'nav-tab-active' : ''; ?>">
                            <?php echo __("General", $this->plugin_name);?>
                        </a>
                        <a href="#tab2" data-tab="tab2" class="nav-tab <?php echo ($ays_survey_tab == 'tab2') ? 'nav-tab-active' : ''; ?>">
                            <?php echo __("Integrations", $this->plugin_name);?>
                        </a>
                        <a href="#tab3" data-tab="tab3" class="nav-tab <?php echo ($ays_survey_tab == 'tab3') ? 'nav-tab-active' : ''; ?>">
                            <?php echo __("Message Variables", $this->plugin_name);?>
                        </a>
                        <a href="#tab4" data-tab="tab4" class="nav-tab <?php echo ($ays_survey_tab == 'tab4') ? 'nav-tab-active' : ''; ?>">
                            <?php echo __("Buttons Texts", $this->plugin_name);?>
                        </a>
                        <a href="#tab5" data-tab="tab5" class="nav-tab <?php echo ($ays_survey_tab == 'tab5') ? 'nav-tab-active' : ''; ?>">
                            <?php echo __("Shortcodes", $this->plugin_name);?>
                        </a>
                    </div>
                </div>
                <div class="ays-survey-tabs-wrapper">
                    <div id="tab1" class="ays-survey-tab-content <?php echo ($ays_survey_tab == 'tab1') ? 'ays-survey-tab-content-active' : ''; ?>">
                        <p class="ays-subtitle"><?php echo __('General Settings',$this->plugin_name)?></p>
                        <hr/>
                        <fieldset>
                            <legend>
                                <strong style="font-size:30px;"><i class="ays_fa ays_fa_question"></i></strong>
                                <h5><?php echo __('Default parameters for Survey',$this->plugin_name)?></h5>
                            </legend>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="ays_survey_default_type">
                                        <?php echo __( "Surveys default question type", $this->plugin_name ); ?>
                                        <a class="ays_help" data-toggle="tooltip" title="<?php echo __('You can choose default question type which will be selected in the Add new survey page.',$this->plugin_name)?>">
                                            <i class="ays_fa ays_fa_info_circle"></i>
                                        </a>
                                    </label>
                                </div>
                                <div class="col-sm-8">
                                    <select id="ays_survey_default_type" name="ays_survey_default_type">
                                        <option></option>
                                        <?php
                                            foreach($survey_types as $survey_type => $survey_label):
                                            $selected = $survey_default_type == $survey_type ? "selected" : "";
                                        ?>
                                        <option value="<?php echo $survey_type; ?>" <?php echo $selected; ?> ><?php echo $survey_label; ?></option>
                                        <?php
                                            endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="ays_survey_answer_default_count">
                                        <?php echo __( "Answer default count", $this->plugin_name ); ?>
                                        <a class="ays_help" data-toggle="tooltip" title="<?php echo __('You can write the default answer count which will be showing in the Add new question page (this will work only with radio, checkbox, and dropdown types).',$this->plugin_name)?>">
                                            <i class="ays_fa ays_fa_info_circle"></i>
                                        </a>
                                    </label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="number" name="ays_survey_answer_default_count" id="ays_survey_answer_default_count" class="ays-text-input" value="<?php echo $survey_answer_default_count; ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="ays_survey_textarea_height">
                                        <?php echo __( "Textarea height (public)", $this->plugin_name ); ?>
                                        <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Set the height of the textarea by entering a numeric value. It applies to Paragraph question type textarea.',$this->plugin_name); ?>">
                                            <i class="ays_fa ays_fa_info_circle"></i>
                                        </a>
                                    </label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="number" name="ays_survey_textarea_height" id="ays_survey_textarea_height" class="ays-text-input" value="<?php echo $survey_textarea_height; ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="ays_survey_wp_editor_height">
                                        <?php echo __( "WP Editor height", $this->plugin_name ); ?>
                                        <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Give the default value to the height of the WP Editor. It will apply to all WP Editors within the plugin on the dashboard.',$this->plugin_name); ?>">
                                            <i class="ays_fa ays_fa_info_circle"></i>
                                        </a>
                                    </label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="number" name="ays_survey_wp_editor_height" id="ays_survey_wp_editor_height" class="ays-text-input" value="<?php echo $survey_wp_editor_height; ?>">
                                </div>
                            </div>
                        </fieldset>
                        <hr/>
                        <fieldset>
                            <legend>
                                <strong style="font-size:30px;"><i class="fa fa-server"></i></strong>
                                <h5><?php echo __('Users IP addresses',$this->plugin_name)?></h5>
                            </legend>
                            <blockquote class="ays_warning">
                                <p style="margin:0;"><?php echo __( "If this option is enabled then the 'Limitation by IP' option will not work!", $this->plugin_name ); ?></p>
                            </blockquote>
                            <hr/>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="ays_survey_disable_user_ip">
                                        <?php echo __( "Do not store IP addresses", $this->plugin_name ); ?>
                                        <a class="ays_help" data-toggle="tooltip" title="<?php echo __('After enabling this option, IP address of the users will not be stored in database. Note: If this option is enabled, then the `Limits user by IP` option will not work.',$this->plugin_name)?>">
                                            <i class="ays_fa ays_fa_info_circle"></i>
                                        </a>
                                    </label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="checkbox" class="ays-checkbox-input" id="ays_survey_disable_user_ip" name="ays_survey_disable_user_ip" value="on" <?php echo $survey_disable_user_ip ? 'checked' : ''; ?> />
                                </div>
                            </div>
                        </fieldset>
                        <hr/>
                        <fieldset>
                            <legend>
                                <strong style="font-size:30px;"><i class="fa fa-user-secret"></i></strong>
                                <h5><?php echo __('Anonymity Survey',$this->plugin_name)?></h5>
                            </legend>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="ays_survey_disable_user_name">
                                        <?php echo __( "Do not store User Names", $this->plugin_name ); ?>
                                        <a class="ays_help" data-toggle="tooltip" title="<?php echo __('After enabling this option, User Names will not be stored in database.',$this->plugin_name); ?>">
                                            <i class="ays_fa ays_fa_info_circle"></i>
                                        </a>
                                    </label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="checkbox" class="ays-checkbox-input" id="ays_survey_disable_user_name" name="ays_survey_disable_user_name" value="on" <?php echo $survey_disable_user_name ? 'checked' : ''; ?> />
                                </div>
                            </div>
                            <hr/>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="ays_survey_disable_user_email">
                                        <?php echo __( "Do not store User Emails", $this->plugin_name ); ?>
                                        <a class="ays_help" data-toggle="tooltip" title="<?php echo __('After enabling this option, User Emails will not be stored in database.',$this->plugin_name); ?>">
                                            <i class="ays_fa ays_fa_info_circle"></i>
                                        </a>
                                    </label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="checkbox" class="ays-checkbox-input" id="ays_survey_disable_user_email" name="ays_survey_disable_user_email" value="on" <?php echo $survey_disable_user_email ? 'checked' : ''; ?> />
                                </div>
                            </div>
                        </fieldset> <!-- Anonymity Survey -->
                        <hr>
                        <fieldset>
                            <legend>
                                <strong style="font-size:30px;"><i class="fa fa-align-left"></i></strong>
                                <h5><?php echo __('Excerpt words count in list tables',$this->plugin_name)?></h5>
                            </legend>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="ays_survey_title_length">
                                        <?php echo __( "Survey list table", $this->plugin_name ); ?>
                                        <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Determine the length of the surveys to be shown in the Surveys List Table by putting your preferred count of words in the following field. (For example: if you put 10,  you will see the first 10 words of each survey in the Surveys page of your dashboard.',$this->plugin_name)?>">
                                            <i class="ays_fa ays_fa_info_circle"></i>
                                        </a>
                                    </label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="number" name="ays_survey_title_length" id="ays_survey_title_length" class="ays-text-input" value="<?php echo $survey_title_length; ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="ays_survey_submissions_title_length">
                                        <?php echo __( "Submissions list table", $this->plugin_name ); ?>
                                        <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Determine the length of the submissions to be shown in the Submissions List Table by putting your preferred count of words in the following field. (For example: if you put 10,you will see the first 10 words of each submissions in the Submissions page of your dashboard.',$this->plugin_name)?>">
                                            <i class="ays_fa ays_fa_info_circle"></i>
                                        </a>
                                    </label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="number" name="ays_survey_submissions_title_length" id="ays_survey_submissions_title_length" class="ays-text-input" value="<?php echo $survey_submmission_title_length; ?>">
                                </div>
                            </div>
                        </fieldset>
                        <hr>
                        <fieldset>
                            <legend>
                                <strong style="font-size:30px;"><i class="fa fa-code"></i></strong>
                                <h5><?php echo __('Animation Top',$this->plugin_name)?></h5>
                            </legend>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="ays_survey_enable_animation_top">
                                        <?php echo __( "Enable animation", $this->plugin_name ); ?>
                                        <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Enable animation of the scroll offset of the survey container. It works when the survey container is visible on the screen partly and the user starts the survey and moves from one question to another.',$this->plugin_name)?>">
                                            <i class="ays_fa ays_fa_info_circle"></i>
                                        </a>
                                    </label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="checkbox" name="ays_survey_enable_animation_top" id="ays_survey_enable_animation_top" value="on" <?php echo $survey_enable_animation_top ? 'checked' : ''; ?>>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="ays_survey_animation_top">
                                        <?php echo __( "Scroll offset(px)", $this->plugin_name ); ?>
                                        <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Define the scroll offset of the survey container after the animation starts. It works when the survey container is visible on the screen partly and the user starts the survey and moves from one question to another.',$this->plugin_name)?>">
                                            <i class="ays_fa ays_fa_info_circle"></i>
                                        </a>
                                    </label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="number" name="ays_survey_animation_top" id="ays_survey_animation_top" class="ays-text-input" value="<?php echo $survey_animation_top; ?>">
                                </div>
                            </div>
                        </fieldset>
                        <hr>
                        <fieldset>
                            <legend>
                                <strong style="font-size:30px;"><i class="ays_fa ays_fa_globe"></i></strong>
                                <h5><?php echo __('Who will have permission to Survey menu',$this->plugin_name)?></h5>
                            </legend>
                            <div class="form-group row" style="margin:0px;">
                                <div class="col-sm-12 only_pro">
                                    <div class="pro_features">
                                        <div>
                                            <p>
                                                <?php echo __("This feature is available only in ", $this->plugin_name); ?>
                                                <a href="https://ays-pro.com/wordpress/survey-maker/" target="_blank" title="PRO feature"><?php echo __("PRO version!!!", $this->plugin_name); ?></a>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <label for="ays_user_roles">
                                                <?php echo __( "Select user role", $this->plugin_name ); ?>
                                                <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Ability to manage Survey Maker plugin only for selected user roles.',$this->plugin_name)?>">
                                                    <i class="ays_fa ays_fa_info_circle"></i>
                                                </a>
                                            </label>
                                        </div>
                                        <div class="col-sm-8">
                                            <select name="ays_user_roles[]" id="ays_user_roles" multiple="multiple">
                                                <?php
                                                    foreach($ays_users_roles as $role => $role_name){
                                                        $selected = in_array($role, $user_roles) ? 'selected' : '';
                                                        echo "<option ".$selected." value='".$role."'>".$role_name."</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <blockquote>
                                        <?php echo __( "Ability to manage Survey Maker plugin only for selected user roles.", $this->plugin_name ); ?>
                                    </blockquote>
                                </div>
                            </div>
                        </fieldset>
                        <!-- summary email start -->
                        <hr/>
                        <fieldset class="ays_toggle_parent">
                            <legend>
                                <img src="<?php echo SURVEY_MAKER_ADMIN_URL; ?>/images/icons/send-mail-50x50.png" alt="">
                                <h5><?php echo __("Send automatic email reporting to admin(s)",$this->plugin_name)?></h5>
                            </legend>
                            <div class="form-group row" style="margin:0px;">
                                <div class="col-sm-12 only_pro">
                                    <div class="pro_features">
                                        <div>
                                            <p>
                                                <?php echo __("This feature is available only in ", $this->plugin_name); ?>
                                                <a href="https://ays-pro.com/wordpress/survey-maker/" target="_blank" title="PRO feature"><?php echo __("PRO version!!!", $this->plugin_name); ?></a>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <label for="ays_survey_email_to_admins">
                                                <?php echo __( "Send automatic email reporting per session", $this->plugin_name ); ?>
                                                <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Set up automatic email reporting to inform yourself or anyone else, about the submissions of surveys. It will indicate the times of submissions all surveys will have at that given moment,  will send a table which will include the names of all surveys and the number of submissions each one will have and will show the statistics of the growth/decline of the submissions in percentage',$this->plugin_name); ?>">
                                                    <i class="ays_fa ays_fa_info_circle"></i>
                                                </a>
                                            </label>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="ays-poll-email-to-admins">
                                                <input type="checkbox" class="ays_toggle_checkbox" id="ays_survey_email_to_admins" value="on">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group ays_toggle_target" style='display:block'>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label for="ays_survey_admin_email_sessions">
                                                    <?php echo __( "Session period", $this->plugin_name ); ?>
                                                    <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Specify the time for one session and the provided email(s) will receive an automatic email notification once during the period.',$this->plugin_name); ?>">
                                                        <i class="ays_fa ays_fa_info_circle"></i>
                                                    </a>
                                                </label>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="ays-poll-email-to-admins">
                                                    <select id="ays_survey_admin_email_session">
                                                        <?php
                                                            foreach($ays_survey_admin_email_sessions as $key => $admin_email_session):
                                                                $selected = '';
                                                        ?>
                                                            <option value="<?php echo $key;?>" <?php echo $selected;?>>
                                                                <?php echo $admin_email_session; ?>
                                                            </option>
                                                        <?php
                                                            endforeach;
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label for="ays_survey_admins_emails">
                                                    <?php echo __( "Email addresses", $this->plugin_name ); ?>
                                                    <a class="ays_help" data-toggle="tooltip" title="<?php echo __("Provide emails to which you need to send the survey's reports. Insert emails comma separated.",$this->plugin_name); ?>">
                                                        <i class="ays_fa ays_fa_info_circle"></i>
                                                    </a>
                                                </label>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="ays-poll-email-to-admins">
                                                    <input type="email" class="" id="ays_survey_admins_emails" value="" placeholder="Admins email" style="width:100%" multiple>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <!-- summary email end -->
                    </div>
                    <div id="tab2" class="ays-survey-tab-content <?php echo ($ays_survey_tab == 'tab2') ? 'ays-survey-tab-content-active' : ''; ?>">
                        <p class="ays-subtitle"><?php echo __('Integrations',$this->plugin_name)?></p>
                        <hr/>
                        <?php
                            do_action( 'ays_sm_settings_page_integrations' );
                        ?>                        
                    </div>
                    <div id="tab3" class="ays-survey-tab-content <?php echo ($ays_survey_tab == 'tab3') ? 'ays-survey-tab-content-active' : ''; ?>">
                        <p class="ays-subtitle">
                            <?php echo __('Message variables',$this->plugin_name)?>
                            <a class="ays_help" data-toggle="tooltip" data-html="true" title="<p><?php echo __( 'You can copy these variables and paste them in the following options from the survey settings', $this->plugin_name ); ?>:</p>
                                <ul class='ays_tooltop_ul'>
                                    <li><?php echo __( 'Thank you message', $this->plugin_name ); ?></li>
                                </ul>">
                                <i class="ays_fa ays_fa_info_circle"></i>
                            </a>
                        </p>
                        <blockquote>
                            <p><?php echo __( "You can copy these variables and paste them in the following options from the survey settings", $this->plugin_name ); ?>:</p>
                            <p style="text-indent:10px;margin:0;">- <?php echo __( "Thank you message", $this->plugin_name ); ?></p>
                            <div style="margin-top: 20px;background: #dae2e2;padding: 10px;">
                                <p style="margin:0;"><?php echo __( "Message variables for these features are available only in the Pro version.", $this->plugin_name ); ?></p>
                                <p style="text-indent:10px;margin:0;color:red;">- <?php echo __( "Send email to user", $this->plugin_name ); ?></p>
                                <p style="text-indent:10px;margin:0;color:red;">- <?php echo __( "Send email to admin", $this->plugin_name ); ?></p>
                                <p style="text-indent:10px;margin:0;color:red;">- <?php echo __( "Email configuration", $this->plugin_name ); ?></p>
                                <p style="text-indent:30px;margin:0;color:red;">* <?php echo __( "From Name", $this->plugin_name ); ?></p>
                                <p style="text-indent:30px;margin:0;color:red;">* <?php echo __( "Subject", $this->plugin_name ); ?></p>
                                <p style="text-indent:30px;margin:0;color:red;">* <?php echo __( "Reply To Name", $this->plugin_name ); ?></p>
                            </div>
                        </blockquote>
                        <hr>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <p class="vmessage">
                                    <strong>
                                        <input type="text" onClick="this.setSelectionRange(0, this.value.length)" readonly value="%%user_name%%"/>
                                    </strong>
                                    <span> - </span>
                                    <span style="font-size:18px;">
                                        <?php echo __( "The name the user entered into the survey form. It will work only if the name field exists in the form.", $this->plugin_name); ?>
                                    </span>
                                </p>
                                <p class="vmessage">
                                    <strong>
                                        <input type="text" onClick="this.setSelectionRange(0, this.value.length)" readonly value="%%user_email%%" />
                                    </strong>
                                    <span> - </span>
                                    <span style="font-size:18px;">
                                        <?php echo __( "The E-mail the user entered into the survey form. It will work only if the email field exists in the form.", $this->plugin_name); ?>
                                    </span>
                                </p>
                                <p class="vmessage">
                                    <strong>
                                        <input type="text" onClick="this.setSelectionRange(0, this.value.length)" readonly value="%%survey_title%%" />
                                    </strong>
                                    <span> - </span>
                                    <span style="font-size:18px;">
                                        <?php echo __( "The title of the survey", $this->plugin_name); ?>
                                    </span>
                                </p>
                                <p class="vmessage">
                                    <strong>
                                        <input type="text" onClick="this.setSelectionRange(0, this.value.length)" readonly value="%%questions_count%%" />
                                    </strong>
                                    <span> - </span>
                                    <span style="font-size:18px;">
                                        <?php echo __( "The number of the questions of the given survey.", $this->plugin_name); ?>
                                    </span>
                                </p>
                                <p class="vmessage">
                                    <strong>
                                        <input type="text" onClick="this.setSelectionRange(0, this.value.length)" readonly value="%%current_date%%" />
                                    </strong>
                                    <span> - </span>
                                    <span style="font-size:18px;">
                                        <?php echo __( "The date of the submission survey.", $this->plugin_name); ?>
                                    </span>
                                </p>
                                <p class="vmessage">
                                    <strong>
                                        <input type="text" onClick="this.setSelectionRange(0, this.value.length)" readonly value="%%unique_code%%" />
                                    </strong>
                                    <span> - </span>
                                    <span style="font-size:18px;">
                                        <?php echo __( "Use to identify the uniqueness of each attempt.", $this->plugin_name); ?>
                                    </span>
                                </p>
                                <p class="vmessage">
                                    <strong>
                                        <input type="text" onClick="this.setSelectionRange(0, this.value.length)" readonly value="%%sections_count%%" />
                                    </strong>
                                    <span> - </span>
                                    <span style="font-size:18px;">
                                        <?php echo __( "The number of the sections of the given survey.", $this->plugin_name); ?>
                                    </span>
                                </p>
                                <p class="vmessage">
                                    <strong>
                                        <input type="text" onClick="this.setSelectionRange(0, this.value.length)" readonly value="%%users_count%%" />
                                    </strong>
                                    <span> - </span>
                                    <span style="font-size:18px;">
                                        <?php echo __( "The number of the passed users count of the given survey.", $this->plugin_name); ?>
                                    </span>
                                </p>
                                <p class="vmessage">
                                    <strong>
                                        <input type="text" onClick="this.setSelectionRange(0, this.value.length)" readonly value="%%users_first_name%%" />
                                    </strong>
                                    <span> - </span>
                                    <span style="font-size:18px;">
                                        <?php echo __( "The user's first name that was filled in their WordPress site during registration.", $this->plugin_name); ?>
                                    </span>
                                </p>
                                <p class="vmessage">
                                    <strong>
                                        <input type="text" onClick="this.setSelectionRange(0, this.value.length)" readonly value="%%users_last_name%%" />
                                    </strong>
                                    <span> - </span>
                                    <span style="font-size:18px;">
                                        <?php echo __( "The user's last name that was filled in their WordPress site during registration.", $this->plugin_name); ?>
                                    </span>
                                </p>
                                <p class="vmessage">
                                    <strong>
                                        <input type="text" onClick="this.setSelectionRange(0, this.value.length)" readonly value="%%creation_date%%" />
                                    </strong>
                                    <span> - </span>
                                    <span style="font-size:18px;">
                                        <?php echo __( "The creation date of the survey.", $this->plugin_name); ?>
                                    </span>
                                </p>
                                <p class="vmessage">
                                    <strong>
                                        <input type="text" onClick="this.setSelectionRange(0, this.value.length)" readonly value="%%current_survey_author%%" />
                                    </strong>
                                    <span> - </span>
                                    <span style="font-size:18px;">
                                        <?php echo __( "It will show the author of the current survey.", $this->plugin_name); ?>
                                    </span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div id="tab4" class="ays-survey-tab-content <?php echo ($ays_survey_tab == 'tab4') ? 'ays-survey-tab-content-active' : ''; ?>">
                        <p class="ays-subtitle">
                            <?php echo __('Buttons texts',$this->plugin_name)?>
                            <a class="ays_help" data-toggle="tooltip" data-html="true" title="<p style='margin-bottom:3px;'><?php echo __( 'If you make a change here, these words will not be translated!', $this->plugin_name ); ?>">
                                <i class="ays_fa ays_fa_info_circle"></i>
                            </a>
                        </p>
                        <blockquote class="ays_warning">
                            <p style="margin:0;"><?php echo __( "If you make a change here, these words will not be translated!", $this->plugin_name ); ?></p>
                        </blockquote>
                        <hr>
                        <div class="form-group row">
                            <div class="col-sm-3">
                                <label for="ays_survey_next_button">
                                    <?php echo __( "Next button", $this->plugin_name ); ?>
                                </label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" id="ays_survey_next_button" name="ays_survey_next_button" class="ays-text-input ays-text-input-short"  value='<?php echo $survey_next_button; ?>'>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-3">
                                <label for="ays_survey_previous_button">
                                <?php echo __( "Previous button", $this->plugin_name ); ?>
                                </label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" id="ays_survey_previous_button" name="ays_survey_previous_button" class="ays-text-input ays-text-input-short"  value='<?php echo $survey_previous_button; ?>'>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-3">
                                <label for="ays_survey_clear_button">
                                    <?php echo __( "Clear selection button", $this->plugin_name ); ?>
                                </label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" id="ays_survey_clear_button" name="ays_survey_clear_button" class="ays-text-input ays-text-input-short"  value='<?php echo $survey_clear_button; ?>'>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-3">
                                <label for="ays_survey_finish_button">
                                    <?php echo __( "Finish button", $this->plugin_name ); ?>
                                </label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" id="ays_survey_finish_button" name="ays_survey_finish_button" class="ays-text-input ays-text-input-short"  value='<?php echo $survey_finish_button; ?>'>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-3">
                                <label for="ays_survey_restart_button">
                                <?php echo __( "Restart survey button", $this->plugin_name ); ?>
                                </label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" id="ays_survey_restart_button" name="ays_survey_restart_button" class="ays-text-input ays-text-input-short"  value='<?php echo $survey_restart_button; ?>'>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-3">
                                <label for="ays_survey_exit_button">
                                    <?php echo __( "Exit button", $this->plugin_name ); ?>
                                </label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" id="ays_survey_exit_button" name="ays_survey_exit_button" class="ays-text-input ays-text-input-short"  value='<?php echo $survey_exit_button; ?>'>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-3">
                                <label for="ays_survey_login_button">
                                    <?php echo __( "Log In button", $this->plugin_name ); ?>
                                </label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" id="ays_survey_login_button" name="ays_survey_login_button" class="ays-text-input ays-text-input-short"  value='<?php echo $survey_login_button; ?>'>
                            </div>
                        </div>
                        <div class="form-group row" style="margin:0px;">
                            <div class="col-sm-12 only_pro">
                                <div class="pro_features">
                                    <div>
                                        <p>
                                            <?php echo __("This feature is available only in ", $this->plugin_name); ?>
                                            <a href="https://ays-pro.com/wordpress/survey-maker/" target="_blank" title="PRO feature"><?php echo __("PRO version!!!", $this->plugin_name); ?></a>
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label for="ays_survey_check_button">
                                            <?php echo __( "Check button", $this->plugin_name ); ?>
                                        </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="ays-text-input ays-text-input-short"  value='<?php echo __("Check button", $this->plugin_name); ?>'>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row" style="margin:0px;">
                            <div class="col-sm-12 only_pro">
                                <div class="pro_features">
                                    <div>
                                        <p>
                                            <?php echo __("This feature is available only in ", $this->plugin_name); ?>
                                            <a href="https://ays-pro.com/wordpress/survey-maker/" target="_blank" title="PRO feature"><?php echo __("PRO version!!!", $this->plugin_name); ?></a>
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label for="ays_survey_start_button">
                                            <?php echo __( "Start button", $this->plugin_name ); ?>
                                        </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="ays-text-input ays-text-input-short"  value='<?php echo __("Start button", $this->plugin_name); ?>'>
                                    </div>
                                </div>          
                            </div>          
                        </div>          
                    </div>
                    <div id="tab5" class="ays-survey-tab-content <?php echo ($ays_survey_tab == 'tab5') ? 'ays-survey-tab-content-active' : ''; ?>">
                        <p class="ays-subtitle">
                            <?php echo __('Shortcodes',$this->plugin_name); ?>
                        </p>                        
                        <hr>
                        <fieldset>
                            <legend>
                                <strong style="font-size:30px;">[ ]</strong>
                                <h5><?php echo __('Display survey summary',$this->plugin_name); ?></h5>
                            </legend>
                            <div class="form-group row" style="padding:0px;margin:0;">
                                <div class="col-sm-12" style="padding:20px;">
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <label>
                                                <?php echo __( "Shortcode", $this->plugin_name ); ?>
                                                <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Copy the given shortcode and insert it into any post or page to show the charts of the submissions of the given survey. Please change \'Your_Survey_ID\' to the corresponding ID of your survey.', $this->plugin_name ); ?>">
                                                    <i class="ays_fa ays_fa_info_circle"></i>
                                                </a>
                                            </label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input type="text" id="ays_survey_submissions_summary" class="ays-text-input" onclick="this.setSelectionRange(0, this.value.length)" readonly="" value='[ays_survey_submissions_summary id="Your_Survey_ID"]'>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset> <!-- Display survey summary -->
                        <hr/>
                        <fieldset>
                            <legend>
                                <strong style="font-size:30px;">[ ]</strong>
                                <h5><?php echo __('Extra shortcodes',$this->plugin_name); ?></h5>
                            </legend>
                            <div class="form-group row" style="padding:0px;margin:0;">
                                <div class="col-sm-12" style="padding:20px;">
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <label for="ays_survey_user_first_name">
                                                <?php echo __( "Show User First Name", $this->plugin_name ); ?>
                                                <a class="ays_help" data-toggle="tooltip" title="<?php echo esc_attr( __("Shows the logged-in user's First Name. If the user is not logged-in, the shortcode will be empty.",$this->plugin_name) ); ?>">
                                                    <i class="ays_fa ays_fa_info_circle"></i>
                                                </a>
                                            </label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input type="text" id="ays_survey_user_first_name" class="ays-text-input" onclick="this.setSelectionRange(0, this.value.length)" readonly="" value='[ays_survey_user_first_name]'>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row" style="padding:0px;margin:0;">
                                <div class="col-sm-12" style="padding:20px;">
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <label for="ays_survey_user_last_name">
                                                <?php echo __( "Show User Last Name", $this->plugin_name ); ?>
                                                <a class="ays_help" data-toggle="tooltip" title="<?php echo esc_attr( __("Shows the logged-in user's last name. If the user is not logged-in, the shortcode will be empty.",$this->plugin_name) ); ?>">
                                                    <i class="ays_fa ays_fa_info_circle"></i>
                                                </a>
                                            </label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input type="text" id="ays_survey_user_last_name" class="ays-text-input" onclick="this.setSelectionRange(0, this.value.length)" readonly="" value='[ays_survey_user_last_name]'>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row" style="padding:0px;margin:0;">
                                <div class="col-sm-12" style="padding:20px;">
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <label for="ays_survey_user_nickname">
                                                <?php echo __( "Show User Nickname", $this->plugin_name ); ?>
                                                <a class="ays_help" data-toggle="tooltip" title="<?php echo esc_attr( __("Shows the logged-in user's nickname. If the user is not logged-in, the shortcode will be empty.",$this->plugin_name) ); ?>">
                                                    <i class="ays_fa ays_fa_info_circle"></i>
                                                </a>
                                            </label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input type="text" id="ays_survey_user_nickname" class="ays-text-input" onclick="this.setSelectionRange(0, this.value.length)" readonly="" value='[ays_survey_user_nick_name]'>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row" style="padding:0px;margin:0;">
                                <div class="col-sm-12" style="padding:20px;">
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <label for="ays_survey_user_display_name">
                                                <?php echo __( "Show User Display name", $this->plugin_name ); ?>
                                                <a class="ays_help" data-toggle="tooltip" title="<?php echo esc_attr( __("Shows the logged-in user's display name. If the user is not logged-in, the shortcode will be empty.",$this->plugin_name) ); ?>">
                                                    <i class="ays_fa ays_fa_info_circle"></i>
                                                </a>
                                            </label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input type="text" id="ays_survey_user_display_name" class="ays-text-input" onclick="this.setSelectionRange(0, this.value.length)" readonly="" value='[ays_survey_user_display_name]'>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row" style="padding:0px;margin:0;">
                                <div class="col-sm-12" style="padding:20px;">
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <label for="ays_survey_user_email">
                                                <?php echo __( "Show User Email", $this->plugin_name ); ?>
                                                <a class="ays_help" data-toggle="tooltip" title="<?php echo esc_attr( __("Shows the logged-in user's email. If the user is not logged-in, the shortcode will be empty.",$this->plugin_name) ); ?>">
                                                    <i class="ays_fa ays_fa_info_circle"></i>
                                                </a>
                                            </label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input type="text" id="ays_survey_user_email" class="ays-text-input" onclick="this.setSelectionRange(0, this.value.length)" readonly="" value='[ays_survey_user_email]'>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row" style="padding:0px;margin:0;">
                                <div class="col-sm-12" style="padding:20px;">
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <label for="ays_survey_passed_users_count">
                                                <?php echo __( "Passed users count", $this->plugin_name ); ?>
                                                <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Copy the following shortcode and paste it in posts. Insert the Survey ID to receive the number of participants of the survey.',$this->plugin_name); ?>">
                                                    <i class="ays_fa ays_fa_info_circle"></i>
                                                </a>
                                            </label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input type="text" id="ays_survey_passed_users_count" class="ays-text-input" onclick="this.setSelectionRange(0, this.value.length)" readonly="" value='[ays_survey_passed_users_count id="Your_survey_ID"]'>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row" style="padding:0px;margin:0;">
                                <div class="col-sm-12" style="padding:20px;">
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <label for="ays_survey_show_creation_date">
                                                <?php echo __( "Show survey creation date", $this->plugin_name ); ?>
                                                <a class="ays_help" data-toggle="tooltip" title="<?php echo esc_attr( __("You need to insert Your Survey ID in the shortcode. It will show the creation date of the particular survey. If there is no survey available/found with that particular Survey ID, the shortcode will be empty.",$this->plugin_name) ); ?>">
                                                    <i class="ays_fa ays_fa_info_circle"></i>
                                                </a>
                                            </label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input type="text" id="ays_survey_show_creation_date" class="ays-text-input" onclick="this.setSelectionRange(0, this.value.length)" readonly="" value='[ays_survey_creation_date id="Your_Survey_ID"]'>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row" style="padding:0px;margin:0;">
                                <div class="col-sm-12" style="padding:20px;">
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <label for="ays_survey_show_creation_date">
                                                <?php echo __( "Show survey sections count", $this->plugin_name ); ?>
                                                <a class="ays_help" data-toggle="tooltip" title="<?php echo esc_attr( __("You need to insert Your Survey ID in the shortcode. It will show the number of the sections of the given survey. If there is no survey available/found with that particular Survey ID, the shortcode will be empty.",$this->plugin_name) ); ?>">
                                                    <i class="ays_fa ays_fa_info_circle"></i>
                                                </a>
                                            </label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input type="text" id="ays_survey_show_sections_count" class="ays-text-input" onclick="this.setSelectionRange(0, this.value.length)" readonly="" value='[ays_survey_sections_count id="Your_Survey_ID"]'>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset> <!-- Extra shortcodes -->
                        <hr>
                        <fieldset>
                            <legend>
                                <strong style="font-size:30px;">[ ]</strong>
                                <h5><?php echo __('Most popular survey',$this->plugin_name); ?></h5>
                            </legend>
                            <div class="form-group row" style="padding:0px;margin:0;">
                                <div class="col-sm-12" style="padding:20px;">
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <label for="ays_survey_most_popular">
                                                <?php echo __( "Shortcode", $this->plugin_name ); ?>
                                                <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Designed to show the most popular survey that is passed most commonly by users.',$this->plugin_name); ?>">
                                                    <i class="ays_fa ays_fa_info_circle"></i>
                                                </a>
                                            </label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input type="text" id="ays_survey_most_popular" class="ays-text-input" onclick="this.setSelectionRange(0, this.value.length)" readonly="" value='[ays_survey_most_popular count="1"]'>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset> <!-- Most popular survey -->
                        <hr>
                        <fieldset>
                            <legend>
                                <strong style="font-size:30px;">[ ]</strong>
                                <h5><?php echo __('All submission settings',$this->plugin_name)?></h5>
                            </legend>
                            <div class="form-group row" style="margin:0px;">
                                <div class="col-sm-12 only_pro">
                                    <div class="pro_features">
                                        <div>
                                            <p>
                                                <?php echo __("This feature is available only in ", $this->plugin_name); ?>
                                                <a href="https://ays-pro.com/wordpress/survey-maker/" target="_blank" title="PRO feature"><?php echo __("PRO version!!!", $this->plugin_name); ?></a>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <label >
                                                <?php echo __( "Shortcode", $this->plugin_name ); ?>
                                                <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Copy the given shortcode and insert it into any post or page to show all the submissions of the surveys.',$this->plugin_name)?>">
                                                    <i class="ays_fa ays_fa_info_circle"></i>
                                                </a>
                                            </label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input type="text" class="ays-text-input" value='[ays_survey_all_submissions]'>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <label>
                                                <?php echo __( "Show to guests too", $this->plugin_name ); ?>
                                                <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Show the table to guests as well. By default, it is displayed only for logged-in users. So if the option is disabled, then only the logged-in users will be able to see the table. 
                                                Note: Despite the fact of showing the table to the guests, the table will contain only the info of the logged-in users.',$this->plugin_name)?>">
                                                    <i class="ays_fa ays_fa_info_circle"></i>
                                                </a>
                                            </label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input type="checkbox" class="ays-checkbox-input" />
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <label>
                                                <?php echo __( "Table columns", $this->plugin_name ); ?>
                                                <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Select and reorder the given columns which should be displayed on the front-end.',$this->plugin_name)?>">
                                                    <i class="ays_fa ays_fa_info_circle"></i>
                                                </a>
                                            </label>
                                            <div class="ays-show-user-page-table-wrap">
                                                <ul class="ays-all-submission-table ays-show-user-page-table">
                                                    <?php
                                                        foreach ($default_all_submissions_columns as $val) {
                                                            ?>
                                                            <li class="ays-user-page-option-row ui-state-default">
                                                                <input type="checkbox" checked>
                                                                <label>
                                                                    <?php echo $val; ?>
                                                                </label>
                                                            </li>
                                                        <?php
                                                        }
                                                    ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset> <!-- All Submission settings -->
                        <hr/>
                        <fieldset>
                            <legend>
                                <strong style="font-size:30px;">[ ]</strong>
                                <h5><?php echo __('User history settings',$this->plugin_name)?></h5>
                            </legend>
                            <div class="form-group row" style="margin:0px;">
                                <div class="col-sm-12 only_pro">
                                    <div class="pro_features">
                                        <div>
                                            <p>
                                                <?php echo __("This feature is available only in ", $this->plugin_name); ?>
                                                <a href="https://ays-pro.com/wordpress/survey-maker/" target="_blank" title="PRO feature"><?php echo __("PRO version!!!", $this->plugin_name); ?></a>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <label >
                                                <?php echo __( "Shortcode", $this->plugin_name ); ?>
                                                <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Copy the given shortcode and insert it into any post or page to show the current user’s submissions history. Each user will see individually presented content based on their taken surveys.',$this->plugin_name)?>">
                                                    <i class="ays_fa ays_fa_info_circle"></i>
                                                </a>
                                            </label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input type="text" class="ays-text-input" value='[ays_survey_user_history]'>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <label>
                                                <?php echo __( "User history submissions table columns", $this->plugin_name ); ?>
                                                <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Select and reorder the given columns which should be displayed on the front-end.',$this->plugin_name)?>">
                                                    <i class="ays_fa ays_fa_info_circle"></i>
                                                </a>
                                            </label>
                                            <div class="ays-show-user-page-table-wrap">
                                                <ul class="ays-show-user-page-table">
                                                    <?php
                                                        foreach ($ays_survey_user_history_columns_order as $val) {
                                                            ?>
                                                            <li class="ays-user-page-option-row ui-state-default">
                                                                <input type="checkbox" value="<?php echo $val; ?>" class="ays-checkbox-input" checked/>
                                                                <label>
                                                                    <?php echo $val; ?>
                                                                </label>
                                                            </li>
                                                            <?php
                                                        }
                                                    ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset> <!-- User history settings -->
                        <hr>
                        <fieldset>
                            <legend>
                                <strong style="font-size:30px;">[ ]</strong>
                                <h5><?php echo __('Display question summary',$this->plugin_name); ?></h5>
                            </legend>
                            <div class="form-group row" style="margin:0px;">
                                <div class="col-sm-12 only_pro">
                                    <div class="pro_features">
                                        <div>
                                            <p>
                                                <?php echo __("This feature is available only in ", $this->plugin_name); ?>
                                                <a href="https://ays-pro.com/wordpress/survey-maker/" target="_blank" title="PRO feature"><?php echo __("PRO version!!!", $this->plugin_name); ?></a>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <label for="">
                                                <?php echo __( "Shortcode", $this->plugin_name ); ?>
                                                <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Display each question summary individually on the front-end. To insert question ID, please click on the three dots located at the bottom right corner of the question(Surveys > the given survey > General tab).',$this->plugin_name); ?>">
                                                    <i class="ays_fa ays_fa_info_circle"></i>
                                                </a>
                                            </label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input type="text" id="" class="ays-text-input" onclick="this.setSelectionRange(0, this.value.length)" readonly="" value='[ays_survey_question_summary id="Your_question_ID"]'>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset> <!-- Display question summary -->
                    </div>
                </div>
            </div>
            <hr/>
            <div style="position:sticky;padding:15px 0px;bottom:0;">
                <?php
                    wp_nonce_field('settings_action', 'settings_action');
                    $other_attributes = array();
                    submit_button(__('Save changes', $this->plugin_name), 'primary ays-survey-loader-banner ays-survey-gen-settings-save', 'ays_submit', true, $other_attributes);
                    echo $loader_iamge;
                ?>
            </div>
        </form>
    </div>
</div>
