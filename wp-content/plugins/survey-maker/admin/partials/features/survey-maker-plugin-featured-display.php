<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="wrap">
    <div class="container-fluid">
        <div class="ays-survey-our-products-heading ays-survey-our-products-heading-container">
            <h1 class="text-center"><?php echo get_admin_page_title(); ?></h1>
        </div>
        <p class="text-center"><?php echo __('Please feel free to use our other awesome plugins!', $this->plugin_name); ?></p>
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <img class="card-img-top" src="<?php echo SURVEY_MAKER_ADMIN_URL . "/images/icons/quiz.png"; ?>" alt="Quiz Maker">
                    <div class="card-body">
                        <h5 class="card-title">Quiz Maker</h5>
                        <p class="card-text">With our Quiz Maker plugin it’s easy to make a quiz in a short time. You to add images to your quiz, order unlimited questions. Also you can style your quiz to satisfy your visitors. </p>
                        <a target="_blank" href="https://wordpress.org/plugins/quiz-maker/" class="btn btn-info">WP.org</a>
                        <a target="_blank" href="https://ays-pro.com/wordpress/quiz-maker" class="btn btn-primary">Buy Now</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <img class="card-img-top" src="<?php echo SURVEY_MAKER_ADMIN_URL . "/images/icons/poll.png"; ?>" alt="Poll Maker">
                    <div class="card-body">
                        <h5 class="card-title">Poll Maker</h5>
                        <p class="card-text">POLL MAKER plugin allows you make unlimited number of polls (choosing, rating, voting).</p>
                        <a target="_blank" href="https://wordpress.org/plugins/poll-maker/" class="btn btn-info">WP.org</a>
                        <a target="_blank" href="https://ays-pro.com/wordpress/poll-maker/" class="btn btn-primary">Buy Now</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <img class="card-img-top" src="<?php echo SURVEY_MAKER_ADMIN_URL . "/images/icons/popup.png"; ?>" alt="Popup Box">
                    <div class="card-body">
                        <h5 class="card-title">Popup Box</h5>
                        <p class="card-text">Popup everything you want!! Just put any shortcode and enjoy it.</p>
                        <a target="_blank" href="https://wordpress.org/plugins/ays-popup-box/" class="btn btn-info">WP.org</a>
                        <a target="_blank" href="https://ays-pro.com/wordpress/popup-box/" class="btn btn-primary">Buy Now</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <img class="card-img-top" src="<?php echo SURVEY_MAKER_ADMIN_URL . "/images/icons/sccp.png"; ?>" alt="Secure Copy Content Protection">
                    <div class="card-body">
                        <h5 class="card-title">Secure Copy Content Protection and Content Locking</h5>
                        <p class="card-text">Copy Protection plugin is activated it disables the right click, copy paste, content selection and copy shortcut keys.</p>
                        <a target="_blank" href="https://wordpress.org/plugins/secure-copy-content-protection/" class="btn btn-info">WP.org</a>
                        <a target="_blank" href="https://ays-pro.com/wordpress/secure-copy-content-protection/" class="btn btn-primary">Buy Now</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <img class="card-img-top" src="<?php echo SURVEY_MAKER_ADMIN_URL . "/images/icons/logo-gallery.jpg"; ?>" alt="Gallery - Photo Gallery">
                    <div class="card-body">
                        <h5 class="card-title">Gallery - Photo Gallery</h5>
                        <p class="card-text">Gallery - Photo Gallery is a cool responsive image gallery plugin with beautifully views.</p>
                        <a target="_blank" href="https://wordpress.org/plugins/gallery-photo-gallery/" class="btn btn-info">WP.org</a>
                        <a target="_blank" href="https://ays-pro.com/wordpress/photo-gallery/" class="btn btn-primary">Buy Now</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <img class="card-img-top" src="<?php echo SURVEY_MAKER_ADMIN_URL . "/images/icons/fbl_icon.jpg"; ?>" alt="Facebook Popup Likebox">
                    <div class="card-body">
                        <h5 class="card-title">Facebook Popup Likebox</h5>
                        <p class="card-text">With the help of this  amazing plugin you can promote your Facebook page and add number of Likes , which is very important today. When someone visits your website, plugin opens popup box with your facebook page likebox.</p>
                        <a target="_blank" href="https://wordpress.org/plugins/ays-facebook-popup-likebox/" class="btn btn-info">WP.org</a>
                        <a target="_blank" href="https://ays-pro.com/wordpress/facebook-popup-likebox/" class="btn btn-primary">Buy Now</a>
                    </div>
                </div>
            </div>
        </div>

        <p class="text-center coming-soon">And more and more is <span>Coming Soon</span></p>
    </div>
</div>
