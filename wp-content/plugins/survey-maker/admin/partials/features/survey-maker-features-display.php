<?php
/**
 * Created by PhpStorm.
 * User: biggie18
 * Date: 7/30/18
 * Time: 12:08 PM
 */
?>

<div class="wrap">
    <h1 class="wp-heading-inline">
        <?php echo __(esc_html(get_admin_page_title()), $this->plugin_name); ?>
    </h1>

    <div class="ays-survey-features-wrap">
        <div class="comparison">
            <table>
                <thead>
                    <tr>
                        <th class="tl tl2"></th>
                        <th class="product" style="background:#69C7F1; border-top-left-radius: 5px; border-left:0px;">
                            <span style="display: block"><?php echo __('Personal',$this->plugin_name)?></span>
                            <img src="<?php echo SURVEY_MAKER_ADMIN_URL . '/images/avatars/personal_avatar.png'; ?>" alt="Free" title="Free" width="100"/>
                        </th>
                        <th class="product" style="background:#69C7F1;">
                            <span style="display: block"><?php echo  __('Business',$this->plugin_name)?></span>
                            <img src="<?php echo SURVEY_MAKER_ADMIN_URL . '/images/avatars/business_avatar.png'; ?>" alt="Business" title="Business" width="100"/>
                        </th>
                        <th class="product" style="border-top-right-radius: 5px; border-right:0px; background:#69C7F1;">
                            <span style="display: block"><?php echo __('Developer',$this->plugin_name)?></span>
                            <img src="<?php echo SURVEY_MAKER_ADMIN_URL . '/images/avatars/pro_avatar.png'; ?>" alt="Developer" title="Developer" width="100"/>
                        </th>
                    </tr>
                    <tr>
                        <th></th>
                        <th class="price-info">
                            <div class="price-now">
                                <span><?php echo __('Free',$this->plugin_name)?></span>
                            </div>
                        </th>
                        <th class="price-info">
                            <!-- <div class="price-now"><span>$39</span></div> -->
                            <div class="price-now"><span style="text-decoration: line-through; color: red;">$49</span>
                            </div>
                            <div class="price-now"><span>$39</span>
                            </div>
                            <!-- <div class="price-now"><span style="color: red; font-size: 12px;">Until January 1</span>
                            </div> -->
                        </th>
                        <th class="price-info">
                            <!-- <div class="price-now"><span>$99</span></div> -->
                            <div class="price-now"><span span style="text-decoration: line-through; color: red;">$129</span>
                            </div>
                            <div class="price-now"><span>$99</span>
                            </div>
                            <!-- <div class="price-now"><span style="color: red; font-size: 12px;">Until January 1</span>

                            </div>  -->
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td></td>
                        <td colspan="4"><?php echo __('Support for',$this->plugin_name)?></td>
                    </tr>
                    <tr>
                        <td><?php echo __('Support for',$this->plugin_name)?></td>
                        <td><?php echo __('1 site',$this->plugin_name)?></td>
                        <td><?php echo __('5 site',$this->plugin_name)?></td>
                        <td><?php echo __('Unlimited sites',$this->plugin_name)?></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="3"><?php echo __('Upgrade for',$this->plugin_name)?></td>
                    </tr>
                    <tr class="compare-row">
                        <td><?php echo __('Upgrade for',$this->plugin_name)?></td>
                        <td><?php echo __('1 months',$this->plugin_name)?></td>
                        <td><?php echo __('12 months',$this->plugin_name)?></td>
                        <td><?php echo __('Lifetime',$this->plugin_name)?></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="4"><?php echo __('Support for',$this->plugin_name)?></td>
                    </tr>
                    <tr>
                        <td><?php echo __('Support for',$this->plugin_name)?></td>
                        <td><?php echo __('1 months',$this->plugin_name)?></td>
                        <td><?php echo __('12 months',$this->plugin_name)?></td>
                        <td><?php echo __('Lifetime',$this->plugin_name)?></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('Reports in dashboard',$this->plugin_name)?></td>
                    </tr>
                    <tr class="compare-row">
                        <td><?php echo __('Reports in dashboard',$this->plugin_name)?></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('Unlimited Surveys',$this->plugin_name)?></td>
                    </tr>
                    <tr>
                        <td><?php echo __('Unlimited Surveys',$this->plugin_name)?></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('Unlimited Questions',$this->plugin_name)?></td>
                    </tr>
                    <tr class="compare-row">
                        <td><?php echo __('Unlimited Questions',$this->plugin_name)?></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('Responsive design',$this->plugin_name)?></td>
                    </tr>
                    <tr>
                        <td><?php echo __('Responsive design',$this->plugin_name)?></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('Extra question types',$this->plugin_name)?></td>
                    </tr>
                    <tr class="compare-row">
                        <td><?php echo __('Extra question types',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('Send mail to user',$this->plugin_name)?></td>
                    </tr>
                    <tr>
                        <td><?php echo __('Send mail to user',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('Send mail to admin',$this->plugin_name)?></td>
                    </tr>
                    <tr class="compare-row">
                        <td><?php echo __('Send mail to admin',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('Email configuration',$this->plugin_name)?></td>
                    </tr>
                    <tr>
                        <td><?php echo __('Email configuration',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('Schedule survey',$this->plugin_name)?></td>
                    </tr>
                    <tr class="compare-row">
                        <td><?php echo __('Schedule survey',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('Mailchimp integration',$this->plugin_name)?></td>
                    </tr>
                    <tr>
                        <td><?php echo __('Mailchimp integration',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('Campaign Monitor integration',$this->plugin_name)?></td>
                    </tr>
                    <tr class="compare-row">
                        <td><?php echo __('Campaign Monitor integration',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('Slack integration',$this->plugin_name)?></td>
                    </tr>
                    <tr>
                        <td><?php echo __('Slack integration',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('ActiveCampaign integration',$this->plugin_name)?></td>
                    </tr>
                    <tr class="compare-row">
                        <td><?php echo __('ActiveCampaign integration',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('User history shortcode',$this->plugin_name)?></td>
                    </tr>
                    <tr>
                        <td><?php echo __('User history shortcode',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('Make questions required',$this->plugin_name)?></td>
                    </tr>
                    <tr class="compare-row">
                        <td><?php echo __('Make questions required',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>                    
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('Permissions by user role',$this->plugin_name)?></td>
                    </tr>
                    <tr >
                        <td><?php echo __('Permissions by user role',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('Limit attemps count',$this->plugin_name)?></td>
                    </tr>
                    <tr class="compare-row">
                        <td><?php echo __('Limit attemps count',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('Results with charts',$this->plugin_name)?></td>
                    </tr>
                    <tr>
                        <td><?php echo __('Results with charts',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('Export and import surveys',$this->plugin_name)?></td>
                    </tr>
                    <tr class="compare-row">
                        <td><?php echo __('Export and import surveys',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('Submissions summary export',$this->plugin_name)?></td>
                    </tr>
                    <tr>
                        <td><?php echo __('Submissions summary export',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('Individual submission export',$this->plugin_name)?></td>
                    </tr>
                    <tr class="compare-row">
                        <td><?php echo __('Individual submission export',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('PDF export',$this->plugin_name)?></td>
                    </tr>
                    <tr>
                        <td><?php echo __('PDF export',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('Password protected survey',$this->plugin_name)?></td>
                    </tr>
                    <tr class="compare-row">
                        <td><?php echo __('Password protected survey',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('Google sheet integration',$this->plugin_name)?></td>
                    </tr>
                    <tr>
                        <td><?php echo __('Google sheet integration',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('Zapier integration',$this->plugin_name)?></td>
                    </tr>
                    <tr class="compare-row">
                        <td><?php echo __('Zapier integration',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('SendGrid integration',$this->plugin_name)?></td>
                    </tr>
                    <tr>
                        <td><?php echo __('SendGrid integration',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('GamiPress integration',$this->plugin_name)?></td>
                    </tr>
                    <tr class="compare-row">
                        <td><?php echo __('GamiPress integration',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('MadMimi integration',$this->plugin_name)?></td>
                    </tr>
                    <tr >
                        <td><?php echo __('MadMimi integration',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('GetResponse integration',$this->plugin_name)?></td>
                    </tr>
                    <tr class="compare-row">
                        <td><?php echo __('GetResponse integration',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('ConvertKit integration',$this->plugin_name)?></td>
                    </tr>
                    <tr >
                        <td><?php echo __('ConvertKit integration',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('Access by user role',$this->plugin_name)?></td>
                    </tr>
                    <tr class="compare-row">
                        <td><?php echo __('Access by user role',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('Popup Survey',$this->plugin_name)?></td>
                    </tr>
                    <tr >
                        <td><?php echo __('Popup Survey',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('Conversational surveys',$this->plugin_name)?></td>
                    </tr>
                    <tr class="compare-row">
                        <td><?php echo __('Conversational surveys',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('Logic jump',$this->plugin_name)?></td>
                    </tr>
                    <tr >
                        <td><?php echo __('Logic jump',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('Conditional Results',$this->plugin_name)?></td>
                    </tr>
                    <tr class="compare-row">
                        <td><?php echo __('Conditional Results',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('Summary emails',$this->plugin_name)?></td>
                    </tr>
                    <tr>
                        <td><?php echo __('Summary emails',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('PayPal integration',$this->plugin_name)?></td>
                    </tr>
                    <tr class="compare-row">
                        <td><?php echo __('PayPal integration',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td colspan="4"><?php echo __('Stripe integration',$this->plugin_name)?></td>
                    </tr>
                    <tr class="">
                        <td><?php echo __('Stripe integration',$this->plugin_name)?></td>
                        <td><span>–</span></td>
                        <td><span>–</span></td>
                        <td><i class="ays_fa ays_fa_check"></i></td>
                    </tr>
                    <tr>
                        <td> </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><a href="https://wordpress.org/plugins/survey-maker/" target="_blank" class="price-buy"><?php echo __('Download',$this->plugin_name)?><span class="hide-mobile"></span></a></td>
                        <td><a href="https://ays-pro.com/wordpress/survey-maker/" target="_blank" class="price-buy"><?php echo __('Buy now',$this->plugin_name)?><span class="hide-mobile"></span></a></td>
                        <td><a href="https://ays-pro.com/wordpress/survey-maker/" target="_blank" class="price-buy"><?php echo __('Buy now',$this->plugin_name)?><span class="hide-mobile"></span></a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

