<?php
    /**
     * Enqueue front end and editor JavaScript
     */

    function ays_survey_gutenberg_scripts() {
        global $current_screen;

        if( ! $current_screen ){
            return null;
        }

        if( ! $current_screen->is_block_editor ){
            return null;
        }

        wp_enqueue_script( SURVEY_MAKER_NAME . "-plugin", SURVEY_MAKER_PUBLIC_URL . '/js/survey-maker-public-plugin.js', array('jquery'), SURVEY_MAKER_VERSION, true);
        wp_enqueue_script( SURVEY_MAKER_NAME, SURVEY_MAKER_PUBLIC_URL . '/js/survey-maker-public.js', array('jquery'), SURVEY_MAKER_VERSION, true);

        // Enqueue the bundled block JS file
        wp_enqueue_script(
            'survey-maker-block-js',
            SURVEY_MAKER_BASE_URL ."/survey/survey-maker-block.js",
            array( 'jquery', 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-components', 'wp-editor' ),
            SURVEY_MAKER_VERSION, true //( AYS_QUIZ_BASE_URL . 'quiz-maker-block.js' )
        );
        
        wp_enqueue_style( SURVEY_MAKER_NAME, SURVEY_MAKER_PUBLIC_URL . '/css/survey-maker-public.css', array(), SURVEY_MAKER_VERSION, 'all');
        
        // Enqueue the bundled block CSS file
        wp_enqueue_style(
            'survey-maker-block-css',
            SURVEY_MAKER_BASE_URL ."/survey/survey-maker-block.css",
            array(),
            SURVEY_MAKER_VERSION, 'all'
        );
    }

    function ays_survey_gutenberg_block_register() {
        
        global $wpdb;
        $block_name = 'survey';
        $block_namespace = 'survey-maker/' . $block_name;
        
        $current_user = get_current_user_id();
        $sql = "SELECT * FROM ". $wpdb->prefix . SURVEY_MAKER_DB_PREFIX . "surveys WHERE status != 'trashed'";
        if( ! current_user_can( 'manage_options' ) ){
            $sql .= " AND author_id = ". absint( $current_user ) ." ";
        }
        $results = $wpdb->get_results($sql, "ARRAY_A");
        
        register_block_type(
            $block_namespace, 
            array(
                'render_callback'   => 'survey_maker_render_callback',                
                'editor_script'     => 'survey-maker-block-js',  // The block script slug
                'style'             => 'survey-maker-block-css',
                'attributes'	    => array(
                    'idner' => $results,
                    'metaFieldValue' => array(
                        'type'  => 'integer', 
                    ),
                    'shortcode' => array(
                        'type'  => 'string',				
                    ),
                    'className' => array(
                        'type'  => 'string',
                    ),
                    'openPopupId' => array(
                        'type'  => 'string',
                    ),
                ),
            )
        );
    }    
    
    function survey_maker_render_callback( $attributes ) {
        global $current_screen;
        $is_front = true;

        if( ! empty( $current_screen ) ){
            if( isset( $current_screen->is_block_editor ) && $current_screen->is_block_editor === true ){
                $is_front = false;
            }
        }elseif ( wp_is_json_request() ) {
            $is_front = false;
        }

        $ays_html = "<div class='ays_survey_maker_block_select_survey'>
            <p>" . __('Please select survey') . "</p>
        </div>";

        if( ! empty( $attributes["shortcode"] ) ) {
            $ays_html = do_shortcode( $attributes["shortcode"] );
        }else{
            if( $is_front === true ){
                $ays_html = '';
            }
        }

        return $ays_html;
    }

if(function_exists("register_block_type")){
        // Hook scripts function into block editor hook
    add_action( 'enqueue_block_editor_assets', 'ays_survey_gutenberg_scripts' );
    add_action( 'init', 'ays_survey_gutenberg_block_register' );
}
